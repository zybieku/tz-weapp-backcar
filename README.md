# 介绍

这是粤港回头车

# 快速开始
```shell
# 安装pnpm (已安装可以忽略)
npm install -g pnpm
```

```shell
# 安装依赖库
pnpm install
```

```shell 
#司机端小程序
pnpm dev:dirver 启动开发
pnpm build:dirver 打包测试  
pnpm prod:dirver 打包生产  

#货主端
pnpm dev:cus 启动开发
pnpm build:cus 打包测试  
pnpm prod:cus 打包生产 
```

# 目录结构

```js
├─packages 
│      api.js
│├─weapp-cus 商户端
│    
│├─weapp-driver 司机端
│      

```