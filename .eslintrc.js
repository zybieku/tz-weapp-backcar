// ESLint 检查 .vue 文件需要单独配置编辑器：
// https://eslint.vuejs.org/user-guide/#editor-integrations
module.exports = {
  extends: ["taro/vue3", "plugin:@typescript-eslint/recommended"],
  parser: "vue-eslint-parser",
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
    parser: "@typescript-eslint/parser",
    ecmaFeatures: {
      jsx: true,
      tsx: true,
    },
  },
  env: {
    es6: true,
    node: true,
  },
  globals: {
    $ztStore: "readonly",
    $Taro: "readonly",
    $TzNotify: "readonly",
    ENV_BASE_API: "readonly",
    ENV_DOWNLOAD_API: "readonly",
    TZ_ENV_VESION: "readonly",
    TaroGeneral: "readonly",
  },
  rules: {
    "no-var": 1, //不使用var
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off", //生产环境不能用 console.log()
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off", // 生产环境不能有 debugger
    "no-empty": "warn", //块语句中的内容不能为空,warn警告
    "no-undef": "off", //变量未声明就使用

    //ts

    //定义了但未使用得变量
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": [
      "warn",
      {
        vars: "all",
        args: "after-used",
        argsIgnorePattern: "^_",
      },
    ],
    "@typescript-eslint/ban-ts-comment": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-var-requires": "off",
    "@typescript-eslint/ban-types": "off",

    //vue
    "vue/multi-word-component-names": "off",
    "@typescript-eslint/no-non-null-assertion": "off",
  },
};
