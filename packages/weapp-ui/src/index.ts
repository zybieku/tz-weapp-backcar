import TzIcon from "./components/icon";
import TzText from "./components/text";
import TzView from "./components/view";
import TzImage from "./components/image";
import TzButton from "./components/button";
import TzCanvas from "./components/canvas";
import TzInput from "./components/input";
import TzInputNumber from "./components/input-number";
import TzForm from "./components/form";
import TzSwitch from "./components/switch";
import TzPicker from "./components/picker";
import TzRadioGroup from "./components/radio-group";
import TzRadio from "./components/radio";
import { TzCheckboxGroup, TzCheckbox } from "./components/checkbox-group";
import { TzTabs, TzTabPane } from "./components/tabs";
import {
  TzCellForm,
  TzCell,
  useFormReadonly,
  type ICell,
} from "./components/cell-form";
import TzSearch from "./components/search";
import TzAutocomplete from "./components/auto-complete";
import TzPopup from "./components/popup";
import TzRichText from "./components/rich-text";
import TzSearchPicker from "./components/search-picker";
import TzDialog from "./components/dialog";
import TzSafeBottom from "./components/safe-bottom";
import { TzNavbar, initNavbarLayout } from "./components/navbar";
import TzSwiper from "./components/swiper";
import TzMultiPicker from "./components/multi-picker";
import TzTimePicker from "./components/time-picker";
import TzTextarea from "./components/text-area";
import TzNotify from "./components/notify";

const tzComponents = {
  install(app) {
    app.component("tz-icon", TzIcon);
    app.component("tz-text", TzText);
    app.component("tz-view", TzView);
    app.component("tz-image", TzImage);
    app.component("tz-button", TzButton);
    app.component("tz-canvas", TzCanvas);
    app.component("tz-input", TzInput);
    app.component("tz-input-number", TzInputNumber);
    app.component("tz-form", TzForm);
    app.component("tz-switch", TzSwitch);
    app.component("tz-cell", TzCell);
    app.component("tz-cell-form", TzCellForm);
    app.component("tz-checkbox", TzCheckbox);
    app.component("tz-checkbox-group", TzCheckboxGroup);
    app.component("tz-picker", TzPicker);
    app.component("tz-radio", TzRadio);
    app.component("tz-radio-group", TzRadioGroup);
    app.component("tz-tabs", TzTabs);
    app.component("tz-tab-pane", TzTabPane);
    app.component("tz-search", TzSearch);
    app.component("tz-autocomplete", TzAutocomplete);
    app.component("tz-popup", TzPopup);
    app.component("tz-rich-text", TzRichText);
    app.component("tz-search-picker", TzSearchPicker);
    app.component("tz-dialog", TzDialog);
    app.component("tz-safe-bottom", TzSafeBottom);
    app.component("tz-navbar", TzNavbar);
    app.component("tz-swiper", TzSwiper);
    app.component("tz-multi-picker", TzMultiPicker);
    app.component("tz-time-picker", TzTimePicker);
    app.component("tz-textarea", TzTextarea);
  },
};

export default tzComponents;

export { useFormReadonly, ICell, TzNotify, initNavbarLayout };
