import { PropType } from "vue";

export interface ICell {
  /**
   * 字段名
   */
  field: string;
  label: string;
  show?: () => boolean;
  component?: {
    name?: string;
    attrs?: Record<string, any>;
    render?: (cell?: ICell, index?: number) => JSX.Element | string | null;
  };
  render?: (cell?: ICell, index?: number) => JSX.Element | string | null;
  attrs?: Record<string, any>;
}

export const props = {
  cells: {
    type: Array as PropType<ICell[]>,
    default: () => [],
  },
  modelValue: {
    type: Object,
    default: () => ({}),
  },
};
