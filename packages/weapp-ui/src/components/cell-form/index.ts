import { reactive } from "vue";
import TzCellForm from "./CellForm.vue";
import TzCell from "./Cell.vue";
import type { ICell } from "./cellForm";

/**
 * 设置tz-cell-form只读
 */
export const useFormReadonly = (formConf: ICell[], disabled = true) => {
  formConf.forEach((item) => {
    if (item.component) {
      if (item.component.attrs) {
        item.component.attrs.disabled = disabled;
      } else {
        item.component.attrs = reactive({ disabled: disabled });
      }
    }
  });
};

export { ICell, TzCellForm, TzCell };
