 ### tz-cell-form 表单组件,主要用于一页多个cell的场景
 

 ```ts
  const formRef = ref();
 const state = reactive({
      form: {
        reasonType: "",
        contacts: "",
      },
  });

 let cells=[
     {
        label: "xx类型", //标签名
        field: "reasonType", //字段
        attrs: {   //tz-cell 属性
          required: true,
          border: true,
        },
        component: {  //组件信息
          name: "tz-picker",  // 组件名
          attrs: { //tz-picker 的组件属性
            mode: "selector",
            rangeKey: "label",
            valueKey: "value",
            placeholder: "请选择xxx类型",
            range: niceList,
            onChange: () => handleChangeType(),
          },
        },
      },
    {
        label: "联系xx",
        field: "contacts",
        attrs: {
          required: true,
          border: true,
        },
        render:()=><div>xxx</div>  //使用render函数代替component
    }
],

//如果需要校验可以用到 
//const { validate, errMsg } = formRef.value.validate();

return ()=> <tz-cell-form
              ref={formRef}
              cells={cells}
              v-model={state.form}
            >
            </tz-cell-form>        
```