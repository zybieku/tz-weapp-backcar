import TzTabs from "./Tabs.vue";
import TzTabPane from "./TabPane.vue";

export { TzTabs, TzTabPane };
