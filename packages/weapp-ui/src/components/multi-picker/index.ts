import TzMultiPicker from "./MultiPicker.vue";

export type TzMultiPickerInstance = InstanceType<typeof TzMultiPicker>;

export default TzMultiPicker;
