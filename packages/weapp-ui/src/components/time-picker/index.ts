import TzTimePicker from "./TimePicker.vue";

export type TzTimePickerInstance = InstanceType<typeof TzTimePicker>;

export default TzTimePicker;
