import { PropType } from "vue";

export type PositionProperty =
  | "absolute"
  | "fixed"
  | "relative"
  | "static"
  | "sticky";

export enum EnumTheme {
  /**
   * 高亮主题
   */
  Light = "light",
  Dark = "dark",
  DEFAULT = "default",
}

export const props = {
  title: {
    type: String,
    default: "",
  },
  leftArrow: {
    type: Boolean,
    default: false,
  },
  leftText: {
    type: String,
    default: "",
  },
  leftClass: {
    type: String,
    default: "",
  },
  position: {
    type: String as PropType<PositionProperty>,
    default: "relative",
  },
  theme: {
    type: String,
    default: EnumTheme.DEFAULT,
  },
  clearfix: {
    type: Boolean,
    default: false,
  },
  autoTheme: {
    type: Boolean,
    default: false,
  },
  onBack: {
    type: Function,
    default: undefined,
  },
};
