import Taro from "@tarojs/taro";
import TzNavbar from "./Navbar.vue";
import { provide } from "vue";
import { NAVBAR_KEY } from "./constants";

const initNavbarLayout = (homeUrl?: string) => {
  if (Taro.getEnv() !== Taro.ENV_TYPE.WEB) {
    const { height, top } = Taro.getMenuButtonBoundingClientRect(); //获取胶囊对象
    const { statusBarHeight = 20, windowHeight } = Taro.getSystemInfoSync(); // 获取状态栏高度
    const navBarHeight = statusBarHeight + height + (top - statusBarHeight) * 2;
    provide(NAVBAR_KEY, {
      statusBarHeight,
      navBarHeight,
      windowHeight,
      homeUrl,
    });
  } else {
    const windowHeight = window.innerHeight;
    provide(NAVBAR_KEY, {
      statusBarHeight: 0,
      navBarHeight: 40,
      windowHeight,
      homeUrl,
    });
  }
};

export { initNavbarLayout, TzNavbar };
