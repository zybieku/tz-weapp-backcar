/**
 * 防抖函数
 * @param {Function} func  函数
 * @param {Number} delay  延迟执行毫秒数
 * @returns
 */
export const debounce = (func: Function, delay = 200) => {
  let timeout: ReturnType<typeof setTimeout>;
  return function (...args) {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      func.apply(this, args);
    }, delay);
  };
};

/**
 * @desc 函数节流
 * @param func 函数
 * @param wait 延迟执行毫秒数
 * @param type 1 表时间戳版，2 表定时器版
 */
export const throttle = (func: Function, wait: number, type: 1 | 2 = 1) => {
  let previous = 0;
  let timeout;
  return function (...args) {
    if (type === 1) {
      const now = Date.now();
      if (now - previous > wait) {
        func.apply(this, args);
        previous = now;
      }
    } else if (type === 2) {
      if (!timeout) {
        timeout = setTimeout(() => {
          clearTimeout(timeout);
          timeout = null;
          func.apply(this, args);
        }, wait);
      }
    }
  };
};

/**
 * 深拷贝
 * @param {T} target 要深拷贝的值
 */
export const deepclone = <T = any>(target: T): T => {
  if (typeof target !== "object" || target === null) return target;

  const obj = (Array.isArray(target) ? [] : {}) as T;
  for (const prop in target) {
    if (target.hasOwnProperty(prop)) {
      if (typeof target === "object") {
        obj[prop] = deepclone(target[prop]);
      } else {
        obj[prop] = target[prop];
      }
    }
  }
  return obj;
};
