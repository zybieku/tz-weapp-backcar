import { api } from "@tz-mall/shared";

//获取详情
export const getDetail = (id) => {
  const option = {
    url: `/transport/order/transportOrder/getById/${id}`,
  };
  return api.post(option);
};

//获取详情
export const getLocusDetail = (data) => {
  const option = {
    url: `/transport/order/tracked/listOrderTracked`,
    data: { id: data },
  };
  return api.post(option);
};
