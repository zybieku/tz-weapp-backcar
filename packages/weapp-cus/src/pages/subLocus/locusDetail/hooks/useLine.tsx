interface ICell {
  title: string;
  render?: Function;
  prop: string;
  class?: string;
}
import { dateFormat } from "@tz-mall/shared";
export const useLine = (state) => {
  const cells: ICell[] = [
    {
      title: "车型",
      prop: "truckType",
      class: "nowrap-ellipsis",
      render: () => {
        return (
          <>
            {state.formData.truckType}【{state.formData.truckLength}m*
            {state.formData.truckWidth}m*{state.formData.truckHeight}m/
            {state.formData.truckBoatload}KG/
            {state.formData.truckVolume}CBM】
          </>
        );
      },
    },
    {
      title: "价格有效时间",
      prop: "truckType",
      render: () => {
        return (
          <>
            {dateFormat(state.formData.lineValidTimeBegin || "", "MM-DD HH:mm")}
            ~{dateFormat(state.formData.lineValidTimeEnd || "", "MM-DD HH:mm")}
          </>
        );
      },
    },
  ];

  return () => (
    <div class="line">
      <tz-view class="title">订单编号：{state.formData.sysOrderNo}</tz-view>
      <tz-view class="address">
        <tz-view class="icon">
          <i class="circle"></i>
          <i class="wire"></i>
          <tz-view class="arrow">
            <tz-icon name="d-arrow-right"></tz-icon>
          </tz-view>
          <i class="wire"></i>
          <i class="circle"></i>
        </tz-view>
        <tz-view class="area">
          <div>{state.formData?.consignerRegionName?.replaceAll(",", "/")}</div>
          <div>{state.formData?.consigneeRegionName?.replaceAll(",", "/")}</div>
        </tz-view>
      </tz-view>
      {cells.map((cell) => (
        <tz-cell title={cell.title}>
          <div class={"conent " + (cell.class || "")}>
            {cell?.render?.() || state.formData[cell.prop]}
          </div>
        </tz-cell>
      ))}
    </div>
  );
};
