import { api } from "@tz-mall/shared";

/**
 * 查询已上传附件信息
 * @returns
 */
export const getAttachmentsbyBizIds = (data) => {
  const option = {
    url: "/basicdata/base/attachment/getAttachmentsByBizIds",
    data,
  };
  return api.post(option);
};
