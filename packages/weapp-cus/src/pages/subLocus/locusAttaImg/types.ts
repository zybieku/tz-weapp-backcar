export interface attachementArr {
  bizId: string;
  fileName: string;
  id: string;
  remoteFileName: string;
}

export interface locusState {
  attachementArr: attachementArr[];
}
