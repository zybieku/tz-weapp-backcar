import { api } from "@tz-mall/shared";
// import type { IOrderResp } from "./types";

// 预约下单
export const getGoodsList = (data) => {
  const option = {
    url: `/transport/order/goodsHistoryInfo/listByLikeGoodsName`,
    data,
  };
  return api.post(option);
};

/**
 * 根据id查询明细
 * @returns
 */
export const getBaseAddressById = (data) => {
  const option = {
    url: "/basicdata/base/address/getBaseAddressById",
    data: { intId: data },
  };
  return api.post(option);
};

/**
 * 保存新增常用路线
 * @returns
 */
export const saveComLines = (data) => {
  const option = {
    url: "/transport/line/oftenLine/save",
    data,
  };
  return api.post(option);
};

/**
 * 修改常用路线
 * @returns
 */
export const updateComLines = (data) => {
  const option = {
    url: "/transport/line/oftenLine/update",
    data,
  };
  return api.post(option);
};

//获取常用路线详情
export const getComLinesDetail = (id) => {
  const option = {
    url: `/transport/line/oftenLine/getById/${id}`,
  };
  return api.post(option);
};
