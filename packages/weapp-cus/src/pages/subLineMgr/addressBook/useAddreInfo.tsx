import { useTzRouter } from "@tz-mall/shared";

export const useAddreInfo = (state, handleEditAddress) => {
  let { navigateBack, route } = useTzRouter();
  const selectAddreBook = (item) => {
    if (route.query.type === "mine") {
      return;
    }
    navigateBack({
      event: {
        type: "selectAddre",
        data: {
          id: item.id,
          addressAreaName: item.addressAreaName,
          type: route.query.type,
        },
      },
    });
  };
  return {
    render: () => (
      <div>
        {state.addressArr.map((item) => (
          <div
            class="search-item"
            onClick={() => {
              selectAddreBook(item);
            }}
          >
            <div class="addre-name nowrap-ellipsis">{item.name}</div>
            <div class="addre-info">
              <div class="info-detail">
                <div class="address-info nowrap-ellipsis">
                  {item.addressAreaName.split("/").join("")}
                  {item.detailAddress}
                </div>
                <div class="user-phone">
                  <div class="user nowrap-ellipsis">{item.contacts}</div>
                  <div class="nowrap-ellipsis">{item.phone}</div>
                </div>
              </div>
              <div class="info-block">
                <tz-button
                  class="info-but but-edit"
                  type="primary"
                  plain
                  round
                  size="small"
                  link
                  onClick={(e) => {
                    e.stopPropagation();
                    handleEditAddress(item);
                  }}
                >
                  编辑
                </tz-button>
                <tz-button
                  class="info-but but-del"
                  type="danger"
                  plain
                  round
                  size="small"
                  link
                  onClick={(e) => {
                    e.stopPropagation();
                    state.deleteConfShow(item);
                  }}
                >
                  删除
                </tz-button>
              </div>
            </div>
          </div>
        ))}
      </div>
    ),
  };
};
