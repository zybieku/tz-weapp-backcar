import { api } from "@tz-mall/shared";

// 获取地址簿
export const getAddressList = (data) => {
  const option = {
    url: `/basicdata/base/address/list`,
    data,
  };
  return api.post(option);
};

/**
 * 根据id删除
 * @returns
 */
export const deleteBaseAddress = (data) => {
  const option = {
    url: "/basicdata/base/address/deleteBaseAddress",
    data: { intId: data },
  };
  return api.post(option);
};
