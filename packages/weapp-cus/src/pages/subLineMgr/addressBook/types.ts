export interface ListItem {
  name: string;
}

export interface addressArr {
  addressAreaCode: string;
  addressAreaName: string;
  contacts: string;
  createrName: string;
  detailAddress: string;
  id: number;
  name: string;
  phone: string;
  type: string;
}
export interface addressBookData {
  searchName: string;
  showType: string;
  addressArr: addressArr[];
  pageParams: {
    size: number;
    current: number;
    addressAreaCode: string;
  };
}
