import { delcomLinesById } from "./api";
import { reactive } from "vue";

export const useDelete = (state, handleGetList) => {
  const dialogState = reactive({
    show: false,
    formData: {
      id: "",
    },
  });
  const closeDialog = () => {
    dialogState.show = false;
  };

  state.deleteConfShow = (item) => {
    dialogState.show = true;
    dialogState.formData = item;
  };

  const handleConfirm = () => {
    delcomLinesById(dialogState.formData.id)
      .then((res) => {
        $TzNotify.success(res.msg);
        closeDialog();
        handleGetList();
      })
      .catch((res) => {
        $TzNotify.danger(res.data.msg);
      });
  };
  return () => (
    <>
      <tz-dialog
        class="cancel-dialog"
        title=""
        v-model={dialogState.show}
        v-slots={{
          footer: () => (
            <div class="dialog-footer">
              <tz-button class="btn btn-cancel" plain onClick={closeDialog}>
                取消
              </tz-button>
              <tz-button class="btn" type="primary" onClick={handleConfirm}>
                删除
              </tz-button>
            </div>
          ),
        }}
      >
        <div class="dialog-title">
          <tz-icon name="yonghuxieyi"></tz-icon>确认删除当前路线吗？
        </div>
      </tz-dialog>
    </>
  );
};
