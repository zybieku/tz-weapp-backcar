export const useAddreInfo = (state, navigateTo) => {
  //新增、编辑地址簿
  const handleEditAddress = (item) => {
    navigateTo({
      path: "/pages/subLineMgr/comLinesEdit/index",
      query: {
        type: "edit",
        id: item.id,
      },
    });
  };

  return {
    render: () => (
      <div>
        {state.linesList.map((item) => (
          <div
            class="search-item"
            onClick={() => {
              state.handleComLinesChoice(item);
            }}
          >
            <div class="addre-title nowrap-ellipsis">{item.lineName}</div>
            <div class="addre-info">
              <div class="info-detail">
                <div class="title-name">
                  <div>
                    <tz-icon class="label-icon red" name="zhuang"></tz-icon>
                  </div>
                  <div class="address-name nowrap-ellipsis">
                    {item.consignerAddressName}
                  </div>
                </div>
                <div class="address-info nowrap-ellipsis">
                  {item.consignerRegionName.split("/").join("") +
                    item.consignerAddress}
                </div>
                <div class="user-phone nowrap-ellipsis">
                  <div class="user nowrap-ellipsis">{item.consignerName}</div>
                  <div class="phone nowrap-ellipsis">{item.consignerPhone}</div>
                </div>
              </div>
              <div class="info-detail nowrap-ellipsis">
                <div class="title-name">
                  <div>
                    <tz-icon class="label-icon blue" name="xie"></tz-icon>
                  </div>
                  <div class="address-name">{item.consigneeAddressName}</div>
                </div>
                <div class="address-info nowrap-ellipsis">
                  {item.consigneeRegionName.split("/").join("") +
                    item.consigneeAddress}
                </div>
                <div class="user-phone">
                  <div class="user nowrap-ellipsis">{item.consigneeName}</div>
                  <div class="phone nowrap-ellipsis">{item.consigneePhone}</div>
                </div>
              </div>
            </div>
            <div class="info-block">
              <tz-button
                class="info-but but-edit"
                type="primary"
                plain
                round
                size="small"
                link
                onClick={(e) => {
                  e.stopPropagation();
                  handleEditAddress(item);
                }}
              >
                编辑
              </tz-button>
              <tz-button
                class="info-but but-del"
                type="danger"
                plain
                round
                size="small"
                link
                onClick={(e) => {
                  e.stopPropagation();
                  state.deleteConfShow(item);
                }}
              >
                删除
              </tz-button>
            </div>
          </div>
        ))}
      </div>
    ),
  };
};
