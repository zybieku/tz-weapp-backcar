import { api } from "@tz-mall/shared";

// 获取地址簿
export const getComLinesList = (data) => {
  const option = {
    url: `/transport/line/oftenLine/page`,
    data,
  };
  return api.post(option);
};
//删除常用路线
export const delcomLinesById = (id) => {
  const option = {
    url: `/transport/line/oftenLine/delById/${id}`,
  };
  return api.post(option);
};
