export interface ListItem {
  name: string;
}

export interface linesList {
  consigneeAddress: string;
  consigneeName: string;
  consigneePhone: string;
  consigneeRegion: string;
  consigneeRegionName: string;
  consignerAddress: string;
  consignerName: string;
  consignerPhone: string;
  consignerRegion: string;
  consignerRegionName: string;
  id: number;
  lineName: string;
}

export interface comLinesData {
  searchName: string;
  pageParams: {
    size: number;
    current: number;
  };
  linesList: linesList[];
  handleComLinesChoice?: Function;
}
