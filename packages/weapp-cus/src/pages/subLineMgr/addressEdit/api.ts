import { api } from "@tz-mall/shared";
import type { IProvince } from "./types";

/**
 * 查询粤港行政区划
 * @returns
 */
export const apiListGuangdongHongKong = () => {
  const option = {
    url: "/basicdata/base/province/listGuangdongHongKong",
  };
  return api.post<IProvince>(option);
};

/**
 * 新增
 * @returns
 */
export const addBaseAddress = (data) => {
  const option = {
    url: "/basicdata/base/address/addBaseAddress",
    data,
  };
  return api.post(option);
};

/**
 * 编辑
 * @returns
 */
export const updateBaseAddress = (data) => {
  const option = {
    url: "/basicdata/base/address/updateBaseAddress",
    data,
  };
  return api.post(option);
};
/**
 * 根据id查询明细
 * @returns
 */
export const getBaseAddressById = (data) => {
  const option = {
    url: "/basicdata/base/address/getBaseAddressById",
    data: { intId: data },
  };
  return api.post(option);
};
