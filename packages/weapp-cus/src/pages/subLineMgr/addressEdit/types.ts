export interface ICity {
  code: string;
  name: string;
  list: ICity[];
}

export interface IProvince extends IBaseResp {
  data: ICity[];
}
