import { useAppStore } from "@tz-mall/shared";
import { userAgreementUrl, userPrivateUrl, userAboutUrl } from "@/assets";
import Taro from "@tarojs/taro";

interface ICell {
  icon: string;
  title: string;
  path: string;
  color: string;
  query?: Record<string, string>;
  isLogin?: Boolean;
}

export const useList = (navigateTo) => {
  const app = useAppStore();

  const handleClickItem = (cell: ICell) => {
    if (cell.isLogin && !app.isLogin) {
      navigateTo({ path: "/pages/login/index" });
      return;
    }
    navigateTo({
      path: cell.path,
      query: cell.query,
    });
  };
  const cells: ICell[] = [
    {
      icon: "location",
      title: "地址簿",
      color: "#1C7BEF",
      path: "/pages/subLineMgr/addressBook/index",
      isLogin: true,
      query: {
        type: "mine",
      },
    },
    {
      icon: "changyongxianlu",
      title: "常用路线",
      color: "#f95e5a",
      path: "/pages/subLineMgr/commonLines/index",
      isLogin: true,
      query: {
        type: "mine",
      },
    },
    {
      icon: "qiyerenzheng",
      title: "企业验证",
      color: "#FF7A03",
      path: "/pages/subUser/verification/index",
      isLogin: true,
    },
    {
      icon: "bangzhuzhongxin",
      title: "帮助中心",
      color: "#00DEFF",
      path: "/pages/subUser/webPage/index",
      query: {
        url: "https://mp.weixin.qq.com/s/sle3o48FACXFHCo7isz4Gw",
        title: "帮助中心",
      },
    },
    {
      icon: "guanyuwomen",
      title: "关于我们",
      color: "#F95E5A",
      path: "/pages/subUser/webPage/index",
      query: {
        url: userAboutUrl,
        title: "关于我们",
      },
    },
    {
      icon: "lianxiwomen",
      title: "联系我们",
      color: "#1C7BEF",
      path: "/pages/subUser/webPage/index",
      query: {
        url: "https://mp.weixin.qq.com/s/aFEFdJ5QgC4c6iUYvSnbkw",
        title: "联系我们",
      },
    },
    {
      icon: "caozuozhinan",
      title: "操作指南",
      color: "#F96460",
      path: "/pages/subUser/webPage/index",
      query: {
        url: "https://mp.weixin.qq.com/s/mgl7WMz_MSpSzIy_TYUl-A",
        title: "操作指南",
      },
    },
    {
      icon: "yonghuxieyi",
      title: "用户协议",
      color: "#FF7A03",
      path: "/pages/subUser/webPage/index",
      query: {
        url: userAgreementUrl,
        title: "用户协议",
      },
    },
    {
      icon: "yinsixieyi",
      title: "隐私协议",
      color: "#00DEFF",
      path: "/pages/subUser/webPage/index",
      query: {
        url: userPrivateUrl,
        title: "隐私协议",
      },
    },
  ];

  const handleLogout = () => {
    Taro.reLaunch({
      url: "/pages/login/index",
    });
  };
  return () => (
    <div class="list">
      {cells.map((cell, index) => (
        <tz-cell
          key={index}
          onClick={(e: MouseEvent) => {
            e.stopPropagation();
            handleClickItem(cell);
          }}
          border
        >
          <div class="label">
            <tz-icon name={cell.icon} style={{ color: cell.color }}></tz-icon>
            <div class="title">{cell.title}</div>
          </div>
          <tz-icon name="arrow-right" class="arrow-right"></tz-icon>
        </tz-cell>
      ))}
      <tz-button v-show={app.isLogin} class="logout" onClick={handleLogout}>
        退出登录
      </tz-button>
    </div>
  );
};
