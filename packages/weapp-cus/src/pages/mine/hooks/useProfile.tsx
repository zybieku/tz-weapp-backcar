import { bgMineHeader, icLogo } from "@/assets";
import { useAppStore } from "@tz-mall/shared";

export const useProfile = (navigateTo: (o: INavOption) => void) => {
  const app = useAppStore();

  const handleLogin = (e: MouseEvent) => {
    e.stopPropagation();
    if (app.isLogin) return;
    navigateTo({ path: "/pages/login/index" });
  };
  return () => (
    <div class="profile">
      <tz-navbar leftText="我的" fixed leftArrow={false}></tz-navbar>
      <tz-image class="bg-profile" src={bgMineHeader}></tz-image>
      <div class="info" onClick={handleLogin}>
        <tz-image class="logo" src={icLogo}></tz-image>
        <div class="account">{app.isLogin ? app.account : "登录/注册"}</div>
      </div>
    </div>
  );
};
