import { api } from "@tz-mall/shared";
import type { IOrderResp } from "./types";

// 预约下单
export const getGoodsList = (data) => {
  const option = {
    url: `/transport/order/goodsHistoryInfo/listByLikeGoodsName`,
    data,
  };
  return api.post(option);
};
