import { imgOrderNodata } from "@/assets";
import { getList } from "../api";
export const useList = (state) => {
  const statusJson = {
    10: { name: "待付款", color: "warning" },
    20: { name: "付款中", color: "warning" },
    30: { name: "付款失败", color: "danger" },
    40: { name: "已取消", color: "danger" },
    50: { name: "已付款", color: "success" },
  };
  const handleGetList = () => {
    getList(state.id)
      .then((res) => {
        state.listData = res.data || [];
      })
      .catch(() => {
        return false;
      });
  };
  handleGetList();
  return () => (
    <>
      {state.listData.map((item, i) => {
        return (
          <div class="list-item" key={i}>
            <div class="title">
              <i></i>费用名称：{item.costName}
            </div>
            <div class="fee">
              <div>
                {item.originalCurrency !== "RMB" && (
                  <>
                    <p>
                      原币种金额:
                      <span>
                        {item.originalCurrency} {item.originalAmount}
                      </span>
                    </p>
                    <p>
                      汇&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;率:
                      <span> {item.rmbExchangeRate}</span>
                    </p>
                  </>
                )}
                <p>
                  费用金额(含税):
                  <span> RMB {item.feeAmount}</span>
                </p>
                <span v-show={item.preferentialTypeName !== ""}>
                  <p>
                    调整金额:
                    <span class={{ preferential: item.preferentialAmount < 0 }}>
                      RMB {item.preferentialAmount}
                    </span>
                  </p>
                  <p>调整类型: {item.preferentialTypeName}</p>
                </span>
                <p>
                  结算金额: <span class="blur"> RMB {item.settleAmount}</span>
                </p>

                <p>创建时间: {item.createTime}</p>
                <p>支付时间: {item.paymentTime}</p>
              </div>
              <div class={statusJson[item.status].color}>
                {statusJson[item.status].name}
              </div>
            </div>
          </div>
        );
      })}
      {state.listData.length === 0 && (
        <div class="no-data">
          <tz-image src={imgOrderNodata} class="img"></tz-image>
          暂无数据
        </div>
      )}
    </>
  );
};
