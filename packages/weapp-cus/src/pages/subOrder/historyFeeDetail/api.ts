import { api } from "@tz-mall/shared";

export const getList = (data) => {
  const option = {
    url: `/transport/order/fee/listOrderFeeByOrderId`,
    data: { id: data },
  };
  return api.post(option);
};
