export interface IOrderData {
  consignerRegionName?: string;
  consignerRegion?: string;
  consigneeRegion?: string;
  consigneeRegionName?: string;
  lineOfferId?: string;
  truckType: string;
  content?: string;
  validityEnd?: string;
  validityStart?: string;
  lineLabelName: Array<string>;
  truckBoatload?: number;
  truckVolume?: number;
  truckLength?: number;
  truckWidth?: number;
  truckHeight?: number;
}
export interface PDetailState {
  orderData: Record<string, string | number>;
  formData: IOrderData;
  loadInfoArrs: Array<Record<string, any>>;
}
export interface PDetailCell {
  title: string;
  class?: string;
  prop: string;
  render?: Function;
  icon?: string;
}
