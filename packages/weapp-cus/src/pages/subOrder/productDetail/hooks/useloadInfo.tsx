import type { PDetailState } from "../types";

export const useloadInfo = (state: PDetailState) => {
  return () => (
    <div class="info-content">
      {state.loadInfoArrs.map((item) => {
        return (
          <>
            <tz-view class="title">{item.typeName}</tz-view>
            <tz-rich-text content={item.content}></tz-rich-text>
          </>
        );
      })}
    </div>
  );
};
