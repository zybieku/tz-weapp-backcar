import type { PDetailCell, PDetailState } from "./../types";
export const useLine = (state: PDetailState) => {
  const cells: PDetailCell[] = [
    {
      title: "车型：",
      prop: "truckType",
      icon: "yunshuche",
      class: "nowrap-ellipsis",
      render: () => {
        return (
          <>
            {state.formData.truckType}【{state.formData.truckLength}m*
            {state.formData.truckWidth}m*{state.formData.truckHeight}m/
            {state.formData.truckBoatload}KG/
            {state.formData.truckVolume}CBM】
          </>
        );
      },
    },
    {
      title: "价格有效时间：",
      prop: "lineValidTimeBegin",
      icon: "shijian",
      render: () => {
        return (
          <>
            {state.formData.validityStart} ~ {state.formData.validityEnd}
          </>
        );
      },
    },
  ];
  return () => (
    <div class="line">
      <tz-view class="address">
        <tz-view class="icon">
          <i class="circle"></i>
          <i class="wire"></i>
          <tz-view class="arrow">
            <tz-icon name="d-arrow-right"></tz-icon>
          </tz-view>
          <i class="wire"></i>
          <i class="circle"></i>
        </tz-view>
        <tz-view class="area">
          <div>{state.formData.consignerRegionName}</div>
          <div>{state.formData.consigneeRegionName}</div>
        </tz-view>
      </tz-view>
      {cells.map((cell) => (
        <tz-cell
          v-slots={{
            label: () => (
              <div class="tz-line-car-time">
                <div
                  class={
                    cell.icon === "yunshuche" ? "cell car-type" : "cell time"
                  }
                >
                  <tz-icon name={cell.icon}></tz-icon>
                  {cell.title}
                </div>
              </div>
            ),
          }}
        >
          <div class={"conent " + (cell.class || "")}>
            {cell.render ? cell?.render?.() : state.formData[cell.prop]}
          </div>
        </tz-cell>
      ))}
      <div class="label">
        {state.formData.lineLabelName.map((item) => {
          return <tz-view class="title">{item}</tz-view>;
        })}
      </div>
    </div>
  );
};
