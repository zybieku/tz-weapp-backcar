import { api } from "@tz-mall/shared";

// 预约下单
export const saveOrder = (data) => {
  const option = {
    url: `/transport/order/transportOrder/save`,
    data,
  };
  return api.post(option);
};

//获取详情
export const getDetail = (data) => {
  const option = {
    url: `/transport/line/offer/getAppletLineOfferById`,
    data,
  };
  return api.post(option);
};
