import { usePay } from "@tz-mall/shared";

interface ICell {
  title: string;
  render?: Function;
  prop: string;
}
export const useList = (state, navigateTo, redirectTo) => {
  const cells: ICell[] = [
    {
      title: "费用名称",
      prop: "costName",
    },
    {
      title: "费用金额",
      prop: "feeAmount",
      render: () => <span class="money">￥{state.detailData.feeAmount}</span>,
    },
    {
      title: "创建时间",
      prop: "feeTime",
    },
  ];
  const handleView = () => {
    navigateTo({
      path: "/pages/subOrder/orderDetail/index",
      query: {
        id: state.detailData.orderId,
      },
    });
  };
  const { payOrder } = usePay();
  //支付
  const handlePay = () => {
    payOrder({ feeId: state.detailData.id }).then(() => {
      redirectTo({
        path: "/pages/subOrder/paySuccess/index",
        query: {
          id: state.detailData.orderId,
        },
      });
    });
  };
  return () => (
    <>
      <div class="list-item">
        <div class="title">
          订单信息
          <span class="btn-view" onClick={handleView}>
            查看
          </span>
        </div>
        {cells.map((cell) => (
          <tz-cell title={cell.title}>
            <div class="conent">
              {cell?.render?.() || state.detailData[cell.prop]}
            </div>
          </tz-cell>
        ))}
      </div>
      <div class="footer">
        <tz-button size="large" class="pay-btn" onClick={handlePay}>
          支付
        </tz-button>
      </div>
    </>
  );
};
