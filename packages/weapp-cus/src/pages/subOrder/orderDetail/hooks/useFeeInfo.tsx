import { usePay } from "@tz-mall/shared";
import { useFeeDetail } from "../hooks";
export const useFeeInfo = (state, redirectTo) => {
  const { payOrder } = usePay();
  //支付
  const handlePay = () => {
    const feeIds =
      state.formData.feeDetail.feeAmountList?.reduce((prev, item) => {
        if (item.id) {
          prev.push(item.id);
        }
        return prev;
      }, []) || [];

    payOrder({
      feeIds,
      orderId: state.formData.id,
      sysOrderNo: state.formData.sysOrderNo,
    }).then(() => {
      redirectTo({
        path: "/pages/subOrder/paySuccess/index",
        query: {
          id: state.formData.id,
        },
      });
    });
  };

  //费用详情
  const handleFeeDetail = () => {
    // if (showDialog) {
    state.isDialogShow = !state.isDialogShow;
    // } else {
    //   navigateTo({
    //     path: "/pages/subOrder/historyFeeDetail/index",
    //     query: {
    //       id: state.formData.id,
    //     },
    //   });
    // }
  };
  const feeDetailRender = useFeeDetail(state);
  return () => (
    <>
      {feeDetailRender()}
      {state.isUnPay && (
        <div class="footer">
          <div class="price">
            <tz-view class="primay">
              RMB
              <tz-view class="fz20">
                {state.formData.feeDetail.unpaidAmount}
              </tz-view>
            </tz-view>
          </div>
          <div class="btn">
            <tz-view onClick={handleFeeDetail}>
              明细
              <tz-icon
                name={state.isDialogShow ? "arrow-bottom" : "arrow-top"}
              ></tz-icon>
            </tz-view>
            <tz-button round size="small" type="warning" onClick={handlePay}>
              支付
            </tz-button>
          </div>
        </div>
      )}
    </>
  );
};
