import { StatusEnum } from "@/pages/order/data";
import { reactive } from "vue";
import { cancelOrder } from "../api";

export const useCancelOrder = (state, detailOrder, backOptions) => {
  const dialogState = reactive({
    show: false,
    formData: {
      orderId: state.id,
      operationContent: "",
    },
  });
  const closeDialog = () => {
    dialogState.show = false;
  };

  const handleConfirm = () => {
    if (!dialogState.formData.operationContent.trim()) {
      $TzNotify.danger("请输入取消订单原因");
      return;
    }
    cancelOrder(dialogState.formData)
      .then(() => {
        $TzNotify.success("取消成功");
        closeDialog();
        // 回退时刷新页面
        backOptions.event = {
          type: "backRefresh",
        };
        detailOrder();
      })
      .catch((err) => {
        $TzNotify.danger(err?.data?.msg);
      });
  };
  return () => (
    <>
      <tz-dialog
        class="cancel-dialog"
        title=""
        v-model={dialogState.show}
        v-slots={{
          footer: () => (
            <div class="dialog-footer">
              <tz-button class="btn btn-cancel" plain onClick={closeDialog}>
                取消
              </tz-button>
              <tz-button class="btn" type="primary" onClick={handleConfirm}>
                确认取消
              </tz-button>
            </div>
          ),
        }}
      >
        <div class="dialog-title">
          <tz-icon name="yonghuxieyi"></tz-icon>确定取消该笔订单？
        </div>
        <tz-textarea
          class="reason-area"
          placeholder-class="phcolor"
          placeholder="请输入取消订单原因"
          v-model={dialogState.formData.operationContent}
          clearable
          maxlength={200}
        ></tz-textarea>
        <span class="red">*如订单有已支付的费用，请联系客服处理！</span>
      </tz-dialog>
      {StatusEnum.UnTakeOrder === state.formData.status && !state.isUnPay && (
        <div class="footer">
          <div></div>
          <div class="btn">
            <tz-button
              round
              size="small"
              class="btn-cancel"
              plain
              onClick={() => {
                dialogState.show = true;
                dialogState.formData.operationContent = "";
              }}
            >
              取消订单
            </tz-button>
          </div>
        </div>
      )}
    </>
  );
};
