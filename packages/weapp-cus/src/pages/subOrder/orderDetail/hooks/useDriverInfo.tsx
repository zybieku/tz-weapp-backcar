import { StatusEnum } from "@/pages/order/data";

export const useDriverInfo = (state) => {
  return () => (
    <>
      {state.formData.status >= StatusEnum.TakeOrder &&
        state.formData.orderSub.inlandLicenseNumber && (
          <div class="order">
            <tz-view class="title">司机/车辆信息</tz-view>
            <tz-view>
              <tz-cell title="大陆车牌">
                <div class="conent">
                  {state.formData.orderSub.inlandLicenseNumber}
                </div>
              </tz-cell>
            </tz-view>
            <tz-view>
              <tz-cell title="香港车牌">
                <div class="conent">
                  {state.formData.orderSub.hkLicenseNumber}
                </div>
              </tz-cell>
            </tz-view>
            <tz-view>
              <tz-cell title="大陆段司机">
                <div class="conent">{state.formData.orderSub.inlandDriver}</div>
              </tz-cell>
            </tz-view>
            <tz-view>
              <tz-cell title="香港段司机">
                <div class="conent"> {state.formData.orderSub.hkDriver}</div>
              </tz-cell>
            </tz-view>
          </div>
        )}
      <div class="order">
        <tz-view>
          <tz-cell title="下单时间">
            <div class="conent">{state.formData.createTime}</div>
          </tz-cell>
        </tz-view>
        {state.formData.orderSub.rejectReasons && (
          <tz-view>
            <tz-cell title="拒单原因">
              <div class="conent">{state.formData.orderSub.rejectReasons}</div>
            </tz-cell>
          </tz-view>
        )}
      </div>
    </>
  );
};
