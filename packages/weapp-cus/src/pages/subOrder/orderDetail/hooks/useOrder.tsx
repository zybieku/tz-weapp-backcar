interface ICell {
  title: string;
  class?: string;
  prop: string;
  render?: Function;
}
import { dateFormat } from "@tz-mall/shared";
export const useOrder = (state) => {
  const tradeTypeJson = {
    BR: "商业登记号码",
    HKID: "香港身份证号码",
    RIN: "ROCARS Identification Number",
    TD: "护照/旅行证件号码",
  };
  const cells: ICell[] = [
    {
      title: "装货信息",
      prop: "consignerRegionName",
    },
    {
      title: " ",
      prop: "consignerAddress",
    },
    {
      title: " ",
      prop: "consignerName",
      render: () => {
        return (
          <>
            {state.formData.consignerName} {state.formData.consignerMobile}
          </>
        );
      },
    },
    {
      title: " ",
      class: "border load-unload-data",
      prop: "loadingTimeBegin",
      render: () => {
        return (
          <>
            装货时间：
            {dateFormat(state.formData.loadingTimeBegin || "", "MM-DD HH:mm")}
          </>
        );
      },
    },
    {
      title: "卸货信息",
      prop: "consigneeRegionName",
    },
    {
      title: " ",
      prop: "consigneeAddress",
    },
    {
      title: " ",
      prop: "consigneeName",
      render: () => {
        return (
          <>
            {state.formData.consigneeName} {state.formData.consigneeMobile}
          </>
        );
      },
    },
    {
      title: " ",
      class: "border load-unload-data",
      prop: "unloadTimeBegin",
      render: () => {
        return (
          <>
            卸货时间：
            {dateFormat(state.formData.unloadTimeBegin || "", "MM-DD HH:mm")}
          </>
        );
      },
    },
    {
      title: "有无托盘",
      prop: "tray",
      class: "border",
      render: () => {
        return (
          <>
            {state.formData.tray === "1"
              ? "木质托盘"
              : state.formData.tray === "2"
              ? "非木质托盘"
              : "无托盘"}
          </>
        );
      },
    },
    {
      title: "货物名称",
      prop: "goodsName",
    },
    {
      title: "总件数",
      prop: "goodsNumber",
    },
    {
      title: "毛重(KG)",
      prop: "roughWeight",
    },
    {
      title: "净重(KG)",
      prop: "goodsWeight",
    },
    {
      title: "体积(CBM)",
      prop: "goodsVolume",
    },
    {
      title: "货品价值(元)",
      prop: "goodsValue",
    },
    {
      title: "备注",
      class: "border",
      prop: "remark",
    },
    {
      title: "委托报关",
      prop: "drawback",
      render: () => {
        return <>{state.formData.entrustDeclarer ? "是" : "否"}</>;
      },
    },
    {
      title: "是否退税",
      class: "border",
      prop: "drawback",
      render: () => {
        return <>{state.formData.drawback ? "是" : "否"}</>;
      },
    },
    // {
    //   title: "下单时间",
    //   prop: "createTime",
    // },
  ];

  return () => (
    <div class="fee-data-order">
      <tz-view class="title">订单信息</tz-view>
      {cells.map((cell) => (
        <tz-cell title={cell.title} class={cell.class}>
          <div class="conent">
            {cell?.render?.() || state.formData[cell.prop]}
          </div>
        </tz-cell>
      ))}
    </div>
  );
};
