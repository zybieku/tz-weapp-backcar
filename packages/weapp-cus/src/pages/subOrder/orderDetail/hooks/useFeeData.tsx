import { useTzRouter } from "@tz-mall/shared";
export const useFeeData = (state) => {
  const { navigateTo } = useTzRouter();

  //费用详情
  const handleFeeDetail = () => {
    navigateTo({
      path: "/pages/subOrder/historyFeeDetail/index",
      query: {
        id: state.formData.id,
      },
    });
  };

  return () => (
    <div class="order order-history pd-50">
      <tz-view class="title">
        费用信息
        <tz-view class="history" onClick={handleFeeDetail}>
          费用明细
        </tz-view>
      </tz-view>
      {state.formData.feeDetail?.feeAmountList?.map((cell) => (
        <tz-cell class="fee-data-cell" title={cell.costName}>
          <div class="conent">
            <span>RMB {cell.feeAmount}</span>
          </div>
        </tz-cell>
      ))}
      {state.formData.feeDetail?.preferentialFeeAmountList?.map((cell) => (
        <tz-cell class="fee-data-cell" title={cell.costName}>
          <div class={{ conent: true, "orange-amount": cell.feeAmount < 0 }}>
            <span>RMB {cell.feeAmount}</span>
          </div>
        </tz-cell>
      ))}
      <tz-cell class="fee-data-cell border-t1" title="费用合计(预估)">
        <div class="conent">
          <span>RMB {state.formData.feeDetail?.totalFeeAmount || 0}</span>
        </div>
      </tz-cell>
      <tz-view>
        <tz-cell title="已支付">
          <div class="conent">
            RMB <text>{state.formData.feeDetail?.paidFeeAmount || 0}</text>
          </div>
        </tz-cell>
      </tz-view>
      <tz-view>
        <tz-cell title="待支付">
          <div class="conent">
            RMB <text>{state.formData.feeDetail?.unpaidAmount || 0}</text>
          </div>
        </tz-cell>
      </tz-view>
    </div>
  );
};
