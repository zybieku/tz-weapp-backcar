export interface feeAmountList {
  costName: string;
  feeAmount: number;
}

export interface IFeeDetail {
  feeAmountList: feeAmountList[];
  feeAmount: number;
  paidFeeAmount: number;
  preferentialFeeAmountList: feeAmountList[];
  totalFeeAmount: number;
}
export interface IOrderDetail {
  feeAmount: string;
  payStatus: string;
  status: string;
  id: string;
  feeDetail: IFeeDetail;
  orderSub: Record<string, any>;
}
export interface orderDetalState {
  id: string;
  isUnPay: Boolean; //是否是待付款
  formData: IOrderDetail;
  isDialogShow: boolean;
  payedPrice: number;
}
