import { api } from "@tz-mall/shared";

//获取详情
export const getDetail = (id) => {
  const option = {
    url: `/transport/order/transportOrder/getById/${id}`,
  };
  return api.post(option);
};
//获取已支付金额
export const getPayPrice = (data) => {
  const option = {
    url: `/transport/order/fee/listOrderFeePayment`,
    data,
  };
  return api.post(option);
};
//接单、取消订单
export const cancelOrder = (data) => {
  const option = {
    url: `/transport/order/transportOrder/cancelOrder`,
    data,
  };
  return api.post(option);
};
