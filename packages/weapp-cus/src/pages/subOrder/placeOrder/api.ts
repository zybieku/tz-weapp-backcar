import { api } from "@tz-mall/shared";
import type { IOrderResp } from "./types";

// 预约下单
export const saveOrder = (data) => {
  const option = {
    url: `/transport/order/transportOrder/save`,
    data,
  };
  return api.post(option);
};

//获取详情
export const getDetail = (data) => {
  const option = {
    url: `/transport/line/offer/getAppletLineOfferById`,
    data,
  };
  return api.post<IOrderResp>(option);
};

/**
 * 根据id查询地址薄明细
 * @returns
 */
export const getBaseAddressById = (data) => {
  const option = {
    url: "/basicdata/base/address/getBaseAddressById",
    data: { intId: data },
  };
  return api.post(option);
};

//获取常用路线详情
export const getComLinesDetail = (id) => {
  const option = {
    url: `/transport/line/oftenLine/getById/${id}`,
  };
  return api.post(option);
};
