import type { IOrderState } from "./../types";

export const useFeeDetail = (state: IOrderState) => {
  return {
    openDialog: () => {
      state.isDialogShow = !state.isDialogShow;
    },
    render: () => (
      <tz-popup
        v-model={state.isDialogShow}
        class="feeDetail"
        position="bottom"
        catch-move={true}
        height="25%"
      >
        <tz-view class="fee-title">
          <tz-view class="del" onClick={() => (state.isDialogShow = false)}>
            ×
          </tz-view>
          费用明细<text></text>
        </tz-view>
        <tz-view class="fee-content">
          <tz-view class="flex">
            <tz-view>订单预估费用</tz-view>
            <i></i>
            <tz-view class="price">RMB {state.formData.orderValue}</tz-view>
          </tz-view>
          <p>
            系统根据路线预估的费用，不代表最终费用，以实际履约产生的价格为准。
          </p>
        </tz-view>
      </tz-popup>
    ),
  };
};
