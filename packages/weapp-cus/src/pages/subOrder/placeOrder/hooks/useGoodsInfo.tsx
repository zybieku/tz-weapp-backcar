import { ref } from "vue";
import { useTzRouter } from "@tz-mall/shared";
import { IOrderCell, IOrderState } from "./../types";
export const useGoodsInfo = (state: IOrderState) => {
  const { navigateTo } = useTzRouter();
  const cells: IOrderCell[] = [
    {
      title: "货物名称",
      prop: "goodsName",
      icon: "huopin",
      render: () => (
        <>
          <div class="goods-name-flex">
            <tz-input
              v-model={state.formData.goodsName}
              placeholder="请输入货物名称"
              maxlength="100"
              onFocus={handleSearch}
            ></tz-input>
            <tz-button
              link
              class="goods-but"
              size="small"
              onClick={handleSearch}
            >
              {">"}
            </tz-button>
          </div>
        </>
      ),
    },
    {
      title: "总件数",
      prop: "goodsNumber",
      icon: "jianshu",
      render: () => (
        <tz-input
          v-model={state.formData.goodsNumber}
          placeholder="请输入总件数"
          maxlength="9"
          rules={(val) => {
            return val?.match(/^[1-9]\d{0,8}/)?.[0] || "";
          }}
        ></tz-input>
      ),
    },
    {
      title: "毛重(KG)",
      prop: "roughWeight",
      icon: "zhongliang",
      render: () => (
        <tz-input
          v-model={state.formData.roughWeight}
          placeholder="请输入毛重"
          rules={(val) => {
            return val?.match(/^[1-9][0-9]{0,8}(\.[0-9]{0,5})?/g)?.[0] || "";
          }}
        ></tz-input>
      ),
    },
    {
      title: "净重(KG)",
      prop: "goodsWeight",
      icon: "zhongliang",
      render: () => (
        <tz-input
          v-model={state.formData.goodsWeight}
          placeholder="请输入净重"
          rules={(val) => {
            return val?.match(/^[1-9][0-9]{0,8}(\.[0-9]{0,5})?/g)?.[0] || "";
          }}
        ></tz-input>
      ),
    },
    {
      title: "体积(CBM)",
      prop: "goodsVolume",
      icon: "tiji",
      render: () => (
        <tz-input
          v-model={state.formData.goodsVolume}
          placeholder="请输入体积"
          rules={(val) => {
            return val?.match(/^[1-9][0-9]{0,8}(\.[0-9]{0,5})?/g)?.[0] || "";
          }}
        ></tz-input>
      ),
    },
    {
      title: "货品价值(元)",
      prop: "goodsValue",
      icon: "rili",
      render: () => (
        <tz-input
          v-model={state.formData.goodsValue}
          placeholder="请输入货品价值"
          rules={(val) => {
            return val?.match(/^[1-9][0-9]{0,8}(\.[0-9]{0,2})?/g)?.[0] || "";
          }}
        ></tz-input>
      ),
    },
    {
      title: "备注",
      prop: "remark",
      icon: "beizhu",
      render: () => (
        <tz-input
          v-model={state.formData.remark}
          placeholder="请输入备注"
          maxlength="500"
        ></tz-input>
      ),
    },
  ];
  const isShow = ref(true);
  const handleSearch = () => {
    navigateTo({
      path: "/pages/subOrder/commodity/index",
      query: {},
    });
  };
  return () => (
    <div>
      <div class="info-content tray">
        <tz-cell
          border
          class="radio-tray-cus"
          v-slots={{
            label: () => (
              <div class="tz-cell-label label-tray">
                <tz-icon name="tuopan"></tz-icon>
                <text class="tz-cell_required">*</text>
                有无托盘
              </div>
            ),
          }}
        >
          <tz-radio-group
            v-model={state.formData.tray}
            class="tray-group"
            options={[
              {
                value: 1,
                text: "木质托盘",
              },
              {
                value: 2,
                text: "非木质托盘",
              },
              {
                value: 3,
                text: "无托盘",
              },
            ]}
          ></tz-radio-group>
        </tz-cell>
      </div>
      <div class="info-content">
        <tz-view class="title">货物信息(选填)</tz-view>
        {isShow.value &&
          cells.map((cell) => (
            <tz-cell
              border
              v-slots={{
                label: () => (
                  <div class="tz-cell-label">
                    <tz-icon name={cell.icon}></tz-icon>
                    {cell.title}
                  </div>
                ),
              }}
            >
              {cell.render ? cell?.render?.() : state.formData[cell.prop]}
            </tz-cell>
          ))}
        <div class="btn-toogle">
          <span onClick={() => (isShow.value = !isShow.value)}>
            {isShow.value ? "收起详情" : "展开详情"}
            <tz-icon
              name={isShow.value ? "arrow-top" : "arrow-bottom"}
            ></tz-icon>
          </span>
        </div>
      </div>
    </div>
  );
};
