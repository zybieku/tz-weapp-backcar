import type { IOrderCell, IOrderState } from "../types";

import { geCurrDate } from "@tz-mall/shared";
export const useloadInfo = (
  state: IOrderState,
  placeOrderVerifi: Function,
  handleToEditAdd: Function
) => {
  const cells: IOrderCell[] = [
    // {
    //   title: "装货地址",
    //   prop: "consignerRegionName",
    //   icon: "location",
    //   render: () => (
    //     <>
    //       <div class="region-name-flex">
    //         <tz-input
    //           v-model={state.consignerSimpleName}
    //           placeholder="请输入装货地址"
    //           disabled
    //         ></tz-input>
    //         <tz-button
    //           link
    //           class="address-book"
    //           onClick={() => {
    //             toAddressBook("consigner");
    //           }}
    //           size="small"
    //         >
    //           <span class="division">| </span>地址薄
    //         </tz-button>
    //       </div>
    //     </>
    //   ),
    // },
    // {
    //   title: "详细地址",
    //   prop: "consignerAddress",
    //   icon: "location",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.consignerAddress}
    //       placeholder="请输入详细地址"
    //       maxlength="100"
    //       onInput={() => {
    //         placeOrderVerifi("consignerAddress");
    //       }}
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "联系人",
    //   prop: "consignerName",
    //   icon: "yonghu",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.consignerName}
    //       placeholder="请输入联系人名称"
    //       maxlength="50"
    //       onInput={() => {
    //         placeOrderVerifi("consignerName");
    //       }}
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "联系电话",
    //   prop: "consignerMobile",
    //   icon: "dianhua",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.consignerMobile}
    //       placeholder="请输入联系人电话"
    //       maxlength="50"
    //       onInput={() => {
    //         placeOrderVerifi("consignerMobile");
    //       }}
    //     ></tz-input>
    //   ),
    // },
    {
      title: "装货日期",
      prop: "consignerMobile",
      icon: "rili",
      render: () => (
        <>
          <tz-picker
            v-model={state.formData.consignerMobile}
            mode="date"
            start={geCurrDate()}
          >
            <text>
              {state.formData.consignerMobile || (
                <text class="picker-date">点击选择日期</text>
              )}
            </text>
          </tz-picker>
        </>
      ),
    },
    {
      title: "装货时间",
      prop: "loadingTimeBegin",
      icon: "rili",
      render: () => (
        <>
          <tz-time-picker
            v-model={state.dateTime.loadingTimeBegin}
            placeholder="选择时间"
            onChange={() => {
              placeOrderVerifi("loadingTimeBegin");
            }}
            maxDay={7}
          ></tz-time-picker>
        </>
      ),
    },
  ];

  return () => (
    <div class="info-content">
      <tz-view class="title-icon">
        <tz-view>
          <tz-icon class="label-icon red" name="zhuang"></tz-icon>
        </tz-view>
        <tz-view class="title-name">装货信息</tz-view>
      </tz-view>
      {cells.map((cell) => (
        <tz-cell
          border
          v-slots={{
            label: () => (
              <div class="tz-cell-label">
                <tz-icon name={cell.icon}></tz-icon>
                <text class="tz-cell_required">*</text>
                {cell.title}
              </div>
            ),
          }}
        >
          {cell.render ? cell?.render?.() : state.formData[cell.prop]}
        </tz-cell>
      ))}
      <div class="pending-check">
        <tz-checkbox value="cb" checked="true">
          <span class="peding">待定</span>
          <span class="recom">建议选择具体时间以便提前安排车辆~</span>
        </tz-checkbox>
      </div>
      <div class="loadinfo-list">
        <div class="loadinfo-item">
          <div class="item-info">装货地:荣超经贸中心</div>
          <div class="item-info">
            详细地址:广东省/深圳市某某街道XX号YY大厦1101
          </div>
          <div class="item-info">联系人:李四</div>
          <div class="item-info phone-but">
            联系电话:18833339999
            <div class="info-block">
              <tz-button
                class="info-but but-edit"
                type="primary"
                plain
                round
                size="small"
                link
                onClick={(e) => {
                  e.stopPropagation();
                  handleEditAddress(item);
                }}
              >
                编辑
              </tz-button>
              <tz-button
                class="info-but but-del"
                type="danger"
                plain
                round
                size="small"
                link
                onClick={(e) => {
                  e.stopPropagation();
                  state.deleteConfShow(item);
                }}
              >
                删除
              </tz-button>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-add">
        <tz-button round size="small" onClick={handleToEditAdd} type="primary">
          <tz-icon name="circle-plus"></tz-icon>
          添加装货地
        </tz-button>
      </div>
    </div>
  );
};
