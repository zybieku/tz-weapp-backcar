import { ref } from "vue";
import { IOrderCell, IOrderState } from "./../types";

export const useCustoms = (state: IOrderState, placeOrderVerifi: Function) => {
  const cells: IOrderCell[] = [
    {
      title: "委托报关",
      prop: "entrustDeclarer",
      icon: "shoufahuorenmingcheng",
      render: () => (
        <tz-radio-group
          v-model={state.formData.entrustDeclarer}
          onChange={() => {
            placeOrderVerifi("entrustDeclarer");
          }}
          options={[
            {
              value: true,
              text: "是",
            },
            {
              value: false,
              text: "否",
            },
          ]}
        ></tz-radio-group>
      ),
    },
    {
      title: "是否退税",
      prop: "drawback",
      icon: "shifoutuishui",
      render: () => (
        <tz-radio-group
          v-model={state.formData.drawback}
          onChange={() => {
            placeOrderVerifi("drawback");
          }}
          options={[
            {
              value: true,
              text: "是",
            },
            {
              value: false,
              text: "否",
            },
          ]}
        ></tz-radio-group>
      ),
    },
    // {
    //   title: "发货人名称",
    //   prop: "tdecConsignerName",
    //   icon: "shoufahuorenmingcheng",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.tdecConsignerName}
    //       placeholder="请输入发货人名称"
    //       maxlength="70"
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "发货人地址",
    //   prop: "tdecConsignerAddress",
    //   icon: "location",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.tdecConsignerAddress}
    //       placeholder="请输入发货人地址"
    //       maxlength="100"
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "收货人名称",
    //   prop: "tdecConsigneeName",
    //   icon: "shoufahuorenmingcheng",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.tdecConsigneeName}
    //       placeholder="请输入收货人名称"
    //       maxlength="70"
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "收货人地址",
    //   prop: "tdecConsigneeAddress",
    //   icon: "location",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.tdecConsigneeAddress}
    //       placeholder="请输入收货人地址"
    //       maxlength="100"
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "进/出口商ID类型",
    //   prop: "goodsVolume",
    //   icon: "jinchukoushangID",
    //   render: () => (
    //     <tz-picker
    //       v-model={state.formData.tradeType}
    //       mode="selector"
    //       range={[
    //         { label: "--请选择--", value: null },
    //         { label: "商业登记号码", value: "BR" },
    //         { label: "香港身份证号码", value: "HKID" },
    //         { label: "ROCARS Identification Number", value: "RIN" },
    //         { label: "护照/旅行证件号码", value: "TD" },
    //       ]}
    //       rangeKey="label"
    //       valueKey="value"
    //     ></tz-picker>
    //   ),
    // },
    // {
    //   title: "进/出口商ID",
    //   prop: "tradeIdentity",
    //   icon: "jinchukoushang",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.tradeIdentity}
    //       placeholder="请输入进/出口商ID"
    //       maxlength="17"
    //       rules={(val) => {
    //         return val.toUpperCase()?.match(/[0-9A-Z/-]{0,17}/g)?.[0] || "";
    //       }}
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "进/出口商名称",
    //   prop: "tradeMerchant",
    //   icon: "jinchukoushangmingcheng",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.tradeMerchant}
    //       placeholder="请输入进/出口商名称"
    //       maxlength="70"
    //     ></tz-input>
    //   ),
    // },
    // {
    //   title: "进/出口商地址",
    //   prop: "tradeAddress",
    //   icon: "location",
    //   render: () => (
    //     <tz-input
    //       v-model={state.formData.tradeAddress}
    //       placeholder="请输入进/出口商地址"
    //       maxlength="70"
    //     ></tz-input>
    //   ),
    // },
  ];
  const isShow = ref(true);
  return () => (
    <div class="info-content">
      <tz-view class="title">
        关务信息 <span class="pc-more">(请登录PC端操作,功能更齐全哦~)</span>
      </tz-view>
      {isShow.value &&
        cells.map((cell) => (
          <tz-cell
            border
            class="radio-tray-cus"
            v-slots={{
              label: () => (
                <div class="tz-cell-label cell-label-customs">
                  <tz-icon name={cell.icon}></tz-icon>
                  <text class="tz-cell_required">*</text>
                  {cell.title}
                </div>
              ),
            }}
          >
            {cell.render ? cell?.render?.() : state.formData[cell.prop]}
          </tz-cell>
        ))}
      <div class="btn-toogle">
        <span onClick={() => (isShow.value = !isShow.value)}>
          {isShow.value ? "收起详情" : "展开详情"}
          <tz-icon name={isShow.value ? "arrow-top" : "arrow-bottom"}></tz-icon>
        </span>
      </div>
    </div>
  );
};
