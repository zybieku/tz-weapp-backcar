import { IOrderState } from "./../types";
interface ICell {
  title: string;
  class?: string;
  prop: string;
  render?: Function;
  icon?: string;
}
export const useDischarge = (
  state: IOrderState,
  placeOrderVerifi: Function,
  toAddressBook: Function
) => {
  const cells: ICell[] = [
    {
      title: "卸货地址",
      prop: "consigneeRegionName",
      icon: "location",
      render: () => (
        <>
          <div class="region-name-flex">
            <tz-input
              v-model={state.consigneeSimpleName}
              placeholder="请输入卸货地址"
              disabled
            ></tz-input>
            <tz-button
              link
              class="address-book"
              onClick={() => {
                toAddressBook("consignee");
              }}
              size="small"
            >
              <span class="division">| </span>地址薄
            </tz-button>
          </div>
        </>
      ),
    },
    {
      title: "详细地址",
      prop: "consigneeAddress",
      icon: "location",
      render: () => (
        <tz-input
          v-model={state.formData.consigneeAddress}
          placeholder="请输入详细地址"
          maxlength="100"
          onInput={() => {
            placeOrderVerifi("consigneeAddress");
          }}
        ></tz-input>
      ),
    },
    {
      title: "联系人",
      prop: "consigneeName",
      icon: "yonghu",
      render: () => (
        <tz-input
          v-model={state.formData.consigneeName}
          placeholder="请输入联系人名称"
          maxlength="50"
          onInput={() => {
            placeOrderVerifi("consigneeName");
          }}
        ></tz-input>
      ),
    },
    {
      title: "联系电话",
      prop: "consigneeMobile",
      icon: "dianhua",
      render: () => (
        <tz-input
          v-model={state.formData.consigneeMobile}
          placeholder="请输入联系人电话"
          maxlength="50"
          onInput={() => {
            placeOrderVerifi("consigneeMobile");
          }}
        ></tz-input>
      ),
    },
    {
      title: "卸货时间",
      prop: "unloadTimeBegin",
      icon: "rili",
      render: () => (
        <>
          <tz-time-picker
            v-model={state.dateTime.unloadTimeBegin}
            placeholder="选择时间"
            onChange={() => {
              placeOrderVerifi("unloadTimeBegin");
            }}
            maxDay={7}
          ></tz-time-picker>
          {/* <view class="connect">至</view>
          <view class="data-time">
            <tz-time-picker
              v-model={state.dateTime.unloadTimeEnd}
              placeholder="选择最晚日期"
              onChange={() => {
                placeOrderVerifi("unloadTimeEnd");
              }}
            ></tz-time-picker>
          </view> */}
        </>
      ),
    },
  ];

  return () => (
    <div class="info-content">
      <tz-view class="title-icon">
        <tz-view>
          <tz-icon class="label-icon blue" name="xie"></tz-icon>
        </tz-view>
        <tz-view class="title-name">卸货信息(必填)</tz-view>
      </tz-view>
      {cells.map((cell) => (
        <tz-cell
          border
          v-slots={{
            label: () => (
              <div class="tz-cell-label">
                <tz-icon name={cell.icon}></tz-icon>
                <text class="tz-cell_required">*</text>
                {cell.title}
              </div>
            ),
          }}
        >
          {cell.render ? cell?.render?.() : state.formData[cell.prop]}
        </tz-cell>
      ))}
    </div>
  );
};
