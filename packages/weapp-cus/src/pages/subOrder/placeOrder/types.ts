export interface IOrderResp extends IBaseResp {
  data: IOrderSubData;
}

interface IOrderSubData {
  lineOfferId?: string;

  lineId: string;

  lineName: string;
  consignerRegion: string;
  consignerRegionName: string;
  consigneeRegion: string;
  consigneeRegionName: string;
  validityStart: string;
  validityEnd: string;
  sellingPrice: number;
  truckBoatload: string;
  truckVolume: string;
  truckType: string;
  truckLength?: number;
  truckWidth?: number;
  truckHeight?: number;
}

export interface IOrderData extends IOrderSubData {
  orderValue: number;
  lineValidTimeEnd: string;
  lineValidTimeBegin: string;
  truckType: string;
  loadingTimeBegin: string;
  loadingTimeEnd: string;
  unloadTimeBegin: string;
  unloadTimeEnd: string;
  consignerName: string;
  consignerMobile: string;
  consignerAddress: string;
  consigneeName: string;
  consigneeMobile: string;
  consigneeAddress: string;
  goodsName: string;
  goodsNumber: string;
  roughWeight: string;
  goodsWeight: string;
  goodsVolume: string;
  goodsValue: string;
  remark: string;
  drawback: boolean | null;
  tdecConsignerName: string;
  tdecConsignerAddress: string;
  tdecConsigneeName: string;
  tdecConsigneeAddress: string;
  tradeType: string;
  tradeIdentity: string;
  tradeMerchant: string;
  tradeAddress: string;
  entrustDeclarer: boolean | null;
  tray: number;
  dataSource: "01" | "02" | "03" | "04" | "05" | "06";
}
export interface IOrderState {
  editField: string;
  placeOrderDisab: boolean;
  orderData: Record<string, string | number>;
  id: string;
  formData: IOrderData;
  isDialogShow: boolean;
  consignerSimpleName: string;
  consigneeSimpleName: string;
  dateTime: {
    loadingTimeBegin: string[];
    loadingTimeEnd: string[];
    unloadTimeBegin: string[];
    unloadTimeEnd: string[];
    validityEnd: string;
    validityStart: string;
  };
}

export interface IOrderCell {
  title: string;
  class?: string;
  prop: string;
  render?: Function;
  icon?: string;
}
