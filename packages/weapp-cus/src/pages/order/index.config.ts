export default {
  enableShareAppMessage: true,
  enableShareTimeline: true,
  navigationBarTitleText: '订单',
  enablePullDownRefresh: true,
  navigationStyle: "custom",
}
