import { api } from "@tz-mall/shared";
import { ListResData } from "./types";
export const getList = (data) => {
  const option = {
    url: "/transport/order/transportOrder/list",
    data,
  };
  return api.post<ListResData>(option);
};
export const getUnPayList = () => {
  const option = {
    url: "/transport/order/transportOrder/listOrderPay",
  };
  return api.post(option);
};
