export interface ListResData {
  code: number;
  data: {
    records: ListItem[];
    total: number;
    size: number;
    current: number;
    searchCount: boolean;
    pages: number;
  };
}
export interface ListItem {
  id: string;
  sysOrderNo: string;
  lineName: string;
  consignerRegionName: string;
  consignerAddress: string;
  unloadTimeBegin: string;
  unloadTimeEnd: string;
  consigneeRegionName: string;
  consigneeAddress: string;
  loadingTimeBegin: string;
  loadingTimeEnd: string;
  createrName: string;
  goodsName: string;
  orderValue: number;
  createrFirmId: string;
  createrFirmName: string;
  userCode: string;
  enterpriseVerifyStatus: boolean;
  createTime: string;
  status: number;
  payStatus: number;
  truckType: string;
  /**
   * 1: 订单
   * 2: 费用
   */
  feeSource?: 1 | 2;
  costName?: string;
  feeAmount?: string;
  feeTime?: string;
  feeStatus: number;
  settlementAmount?: string;
}

export interface UnPayData {
  list: ListItem[];
  total: number;
  current: number;
}
