export const StatusEnum = {
  /**
   *待接单
   */
  UnTakeOrder: 100,
  /**
   *已拒单
   */
  RejectOrder: 190,
  /**
   *已接单
   */
  TakeOrder: 200,
  /**
   *运输中
   */
  Transport: 300,
  /**
   *已完成
   */
  Complete: 400,
  /**
   *已取消
   */
  Cancel: 500,
};
export const statusOptions = [
  { value: StatusEnum.UnTakeOrder, label: "待接单", color: "#1C7BEF" },
  { value: StatusEnum.RejectOrder, label: "已拒单", color: "#999999" },
  { value: StatusEnum.TakeOrder, label: "已接单", color: "#2BBB59" },
  { value: StatusEnum.Transport, label: "运输中", color: "#1C7BEF" },
  { value: StatusEnum.Complete, label: "己完成", color: "#2BBB59" },
  { value: StatusEnum.Cancel, label: "己取消", color: "#999999" },
];
export const statusJson = statusOptions.reduce((prev, item) => {
  prev[item.value] = item.label;
  return prev;
}, {});
export const statusColorJson = statusOptions.reduce(
  (prev, item) => {
    prev[item.value] = item.color;
    return prev;
  },
  { unPay: "#1C7BEF" }
);
export const PayStatusEnum = {
  /**
   * 待付款
   */
  UnPay: 10,
  /**
   * 已付款
   */
  Pay: 20,
  /**
   * 部分付款
   */
  PartPay: 30,
  /**
   * 已取消
   */
  Cancel: 40,
};
export const payStatusOptions = [
  { value: PayStatusEnum.UnPay, label: "待付款", color: "#1C7BEF" },
  { value: PayStatusEnum.Pay, label: "已付款", color: "#2BBB59" },
  { value: PayStatusEnum.PartPay, label: "部分付款", color: "#1C7BEF" },
  { value: PayStatusEnum.Cancel, label: "己取消", color: "#999999" },
];
export const payStatusJson = payStatusOptions.reduce((prev, item) => {
  prev[item.value] = item.label;
  return prev;
}, {});
export const payStatusColorJson = payStatusOptions.reduce((prev, item) => {
  prev[item.value] = item.color;
  return prev;
}, {});
