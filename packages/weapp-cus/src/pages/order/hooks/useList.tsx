import { dateFormat } from "@tz-mall/shared";
import { reactive, ref } from "vue";
import { getList, getUnPayList } from "../api";
import {
  StatusEnum,
  statusJson,
  statusColorJson,
  payStatusColorJson,
  payStatusJson,
} from "../data";
import { ListItem, UnPayData } from "../types";
import { imgOrderNodata } from "@/assets";
import Taro, { usePullDownRefresh, useReachBottom } from "@tarojs/taro";
export const useList = ({ navigateTo, state }) => {
  const listData = ref<ListItem[]>([]);
  const unPayData = reactive<UnPayData>({
    list: [],
    total: 0,
    current: 1,
  });
  let pageCount = 0;
  const searchParams = {
    current: 1,
    status: "",
  };
  const handleGetList = () => {
    if (!state.isLogin) return;
    getList(searchParams)
      .then((res) => {
        if (searchParams.current === 1) {
          listData.value = res.data.records || [];
        } else {
          listData.value.push(...res.data.records);
        }
        pageCount = res.data.pages;
      })
      .catch(() => {
        listData.value = [];
      });
  };
  const handleGetUnPayList = () => {
    if (!state.isLogin) return;
    getUnPayList().then((res) => {
      unPayData.list = res.data;
      unPayData.total = res.data.length;
      if (selectType.value === "unPay") {
        handleLoadUnPayList();
      }
    });
  };
  /**
   * 加载待付款数据：前端虚拟加载（后端是两张表只能返回全部）
   */
  const handleLoadUnPayList = () => {
    if (unPayData.current === 1) {
      listData.value = unPayData.list.slice(0, 10);
    } else if (listData.value.length < unPayData.list.length) {
      listData.value.push(
        ...unPayData.list.slice(
          10 * (unPayData.current - 1),
          unPayData.current * 10
        )
      );
    } else {
      Taro.showToast({
        title: "已全部加载完成",
        icon: "none",
        duration: 2000,
      });
    }
  };

  const selectType = ref<string | number>("");
  const searchList = [
    {
      label: "全部",
      icon: "quanbu",
      value: "",
    },
    {
      label: "待付款",
      icon: "duichonghexiao",
      value: "unPay",
    },
    {
      label: "待接单",
      icon: "daijiedan",
      value: StatusEnum.UnTakeOrder,
    },
    {
      label: "运输中",
      icon: "yunshuzhong",
      value: StatusEnum.Transport,
    },
    {
      label: "已完成",
      icon: "yiwancheng",
      value: StatusEnum.Complete,
    },
  ];

  const handleSelectType = (item) => {
    selectType.value = item.value;
    if (selectType.value === "unPay") {
      unPayData.current = 1;
      handleLoadUnPayList();
    } else {
      searchParams.current = 1;
      searchParams.status = item.value;
      handleGetList();
    }
  };
  const handleView = (item) => {
    if (item.feeSource === 2) {
      // 跳转费用详情
      navigateTo({
        path: "/pages/subOrder/feeDetail/index",
        query: item,
      });
    } else {
      const query = {
        id: item.orderId || item.id,
        isUnPay: selectType.value === "unPay" ? true : undefined,
      };
      // 跳转订单详情
      navigateTo({
        path: "/pages/subOrder/orderDetail/index",
        query,
      });
    }
  };

  // 下拉刷新
  usePullDownRefresh(() => {
    if (selectType.value !== "unPay") {
      searchParams.current = 1;
      handleGetList();
    }
    // 下拉刷新必须刷新待付款数据
    unPayData.current = 1;
    handleGetUnPayList();
    Taro.stopPullDownRefresh();
  });
  // 触底分页加载
  useReachBottom(() => {
    if (selectType.value === "unPay") {
      unPayData.current++;
      handleLoadUnPayList();
    } else {
      if (searchParams.current < pageCount) {
        searchParams.current++;
        handleGetList();
      } else {
        Taro.showToast({
          title: "已全部加载完成",
          icon: "none",
          duration: 2000,
        });
      }
    }
  });

  //查看轨迹
  const handleCheckTrack = (item) => {
    navigateTo({
      path: "/pages/subLocus/locusDetail/index",
      query: {
        id: item.orderId || item.id,
      },
    });
  };

  const copySysOrderNo = (item) => {
    Taro.setClipboardData({
      data: item.sysOrderNo,
      success: function () {
        Taro.getClipboardData({
          success: function () {
            Taro.showToast({
              title: "复制成功",
            });
          },
        });
      },
    });
  };

  const initPage = () => {
    handleGetList();
    handleGetUnPayList();
  };
  initPage();
  return {
    render: () => (
      <div>
        <div class="search-comp">
          {searchList.map((item) => (
            <div
              class={["item", item.value === selectType.value ? "active" : ""]}
              onClick={() => handleSelectType(item)}
            >
              <tz-icon class="icon" name={item.icon}></tz-icon>
              {item.label}
              {item.value === "unPay" && (
                <div class="num">{unPayData.total}</div>
              )}
            </div>
          ))}
        </div>
        {state.isLogin &&
          listData.value.map((item) => {
            // if (item.feeSource === 2) {
            //   // 费用明细
            //   return (
            //     <div
            //       class="list-comp"
            //       key={item.id}
            //       onClick={() => handleView(item)}
            //     >
            //       <div class="border-b1 title-area">
            //         <div class="title">费用名称：{item.costName}</div>
            //         <div class="money">RMB{item.feeAmount}</div>
            //       </div>
            //       <div class="info-area">
            //         <div class="time">
            //           创建时间：
            //           {dateFormat(item.feeTime || "", "YY-MM-DD HH:mm")}
            //         </div>
            //         <span
            //           class="label-status mini-top"
            //           style={{
            //             color: payStatusColorJson[item.feeStatus],
            //           }}
            //         >
            //           {payStatusJson[item.feeStatus]}
            //         </span>
            //       </div>
            //     </div>
            //   );
            // } else {
            return (
              <div
                class="list-comp"
                key={item.id}
                onClick={() => handleView(item)}
              >
                <div class="border-b1 title-area">
                  <div class="title">
                    订单编号：{item.sysOrderNo}
                    <tz-icon
                      onClick={(e) => {
                        e.stopPropagation();
                        copySysOrderNo(item);
                      }}
                      class="order-no-copy"
                      name="copy1"
                    ></tz-icon>
                  </div>
                  <div class="money">
                    RMB {item.settlementAmount || item.orderValue}
                  </div>
                </div>
                <div class="info-area">
                  <span class="label-icon red">装</span>
                  <div>
                    {/* {item.consignerRegionName.replace(/,/g, "")} */}
                    <div class="address">{item.consignerAddress}</div>
                    <div class="time">
                      装货时间：
                      {dateFormat(item.loadingTimeBegin, "YY-MM-DD HH:mm")}
                    </div>
                  </div>
                </div>
                <div class="info-area">
                  <span class="label-icon blue">卸</span>
                  <div>
                    <div class="address">{item.consigneeAddress}</div>
                    <div class="time">
                      卸货时间：
                      {dateFormat(item.unloadTimeBegin, "YY-MM-DD HH:mm")}
                    </div>
                  </div>
                </div>
                <div class="status-locus">
                  <span
                    class="label-status"
                    style={{
                      color:
                        selectType.value === "unPay"
                          ? payStatusColorJson[item.payStatus]
                          : statusColorJson[item.status],
                    }}
                  >
                    {selectType.value === "unPay"
                      ? payStatusJson[item.payStatus]
                      : statusJson[item.status]}
                  </span>
                  <tz-button
                    type="primary"
                    size="small"
                    round
                    class="btn-locus"
                    v-show={item.status !== 100}
                    onClick={(e) => {
                      e.stopPropagation();
                      handleCheckTrack(item);
                    }}
                  >
                    查轨迹
                  </tz-button>
                </div>
              </div>
            );
            // }
          })}
        {state.isLogin && listData.value.length === 0 && (
          <div class="no-data">
            <tz-image src={imgOrderNodata} class="img"></tz-image>
            您还没有订单哦~
          </div>
        )}
      </div>
    ),
    initPage,
    resetPage: () => {
      listData.value = [];
      unPayData.list = [];
      searchParams.current = 1;
      searchParams.status = "";
      unPayData.current = 1;
      unPayData.total = 0;
      selectType.value = "";
    },
  };
};
