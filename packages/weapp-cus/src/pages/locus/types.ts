export interface ListItem {
  name: string;
}
export interface ILocusArr {
  bizId: string; //: "1078018167526887424",
  bizNameName: string; //: "待接单",
  bizStatusCode: number; //: 109,
  createTime: string; //: "2023-82-22 18:19:23",
  createrId: string; //:"6fa824dc-a262-4d96-ad67-2039fdcd6529",
  createrName: string; //:“刘加宝",
  id: number; //: 47,
  operateCode: number; //: 19,
  operateName: string; //:“下单预约",
  trackCode: number; //: 19,
  trackName: string; //:"下单,
  trackSort: number; //: 10,
}
export interface addressArr {
  addressAreaCode: string;
  addressAreaName: string;
  contacts: string;
  createrName: string;
  detailAddress: string;
  id: number;
  name: string;
  phone: string;
  type: string;
}

export interface AddressBookState {
  searchSysOrderNo: string;
  showType: string;
  addressArr: addressArr[];
  locusArr: ILocusArr[];
  pageParams: {
    size: number;
    current: number;
    addressAreaCode: string;
  };
}
