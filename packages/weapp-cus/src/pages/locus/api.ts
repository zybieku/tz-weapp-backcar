import { api } from "@tz-mall/shared";

// 搜索
export const getLocusDetail = (data) => {
  const option = {
    url: `/transport/order/tracked/listOrderFeeBySysOrderNo`,
    data: { str: data },
  };
  return api.post(option);
};
