import { useTzRouter } from "@tz-mall/shared";
import type { AddressBookState, ILocusArr } from "./types";
export const useLocusFlow = (state: AddressBookState) => {
  const { navigateTo } = useTzRouter();

  const hasAttach = (trackCode: number) => {
    return [50, 99].includes(trackCode);
  };

  const handleHasAttach = (item: ILocusArr) => {
    if (hasAttach(item.trackCode)) {
      navigateTo({
        path: "/pages/subLocus/locusAttaImg/index",
        query: {
          id: item.bizId,
          trackType:
            item.trackCode === 99 ? "sign_for_documents" : "shipping_order",
        },
      });
    }
  };

  return () => (
    <div class="track-line">
      {state.locusArr.map((item, index) => (
        <div class="item" key={index}>
          <div class="dot"></div>
          <div class="label">
            <div
              onClick={() => {
                handleHasAttach(item);
              }}
              class={hasAttach(item.trackCode) ? "hasAttach" : ""}
            >
              {item.trackName}
              {hasAttach(item.trackCode) && (
                <tz-button type="primary" class="track-item-btn" size="mini">
                  查看附件
                </tz-button>
              )}
            </div>
          </div>
          <div class="time">{item.createTime}</div>
        </div>
      ))}
    </div>
  );
};
