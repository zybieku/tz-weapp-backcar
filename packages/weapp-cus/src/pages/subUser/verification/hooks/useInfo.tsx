import { submitApply } from "../api";
export const useInfo = (state) => {
  //提交
  const handleSubmit = () => {
    if (state.formData.type === 0 && !state.accessoryVOList[0].filePath) {
      $TzNotify.danger("请上传营业执照!");
      return false;
    }
    if (!state.accessoryVOList[1]?.filePath) {
      $TzNotify.danger("请上传身份证正面!");
      return false;
    }
    if (!state.accessoryVOList[2].filePath) {
      $TzNotify.danger("请上传身份证反面!");
      return false;
    }
    submitApply({ type: state.formData.type })
      .then(() => {
        $TzNotify.success("提交成功");
        state.formData.verifyStatus = 1;
      })
      .catch((res) => {
        $TzNotify.danger(res.msg || "提交失败，请重试！");
      });
  };
  return () => (
    <>
      <tz-view class="item">
        <tz-cell title="验证状态">
          <i
            class={`status s-${
              state.statusJson[state.formData.verifyStatus].type
            }`}
          ></i>
          {state.statusJson[state.formData.verifyStatus].name}
        </tz-cell>

        {
          /* 已拒绝已通过 */
          [3, 2].includes(state.formData.verifyStatus) && (
            <tz-cell title="验证说明">{state.formData.verifyNote}</tz-cell>
          )
        }
        {
          /* 已通过 */
          [3].includes(state.formData.verifyStatus) && (
            <>
              <tz-cell title={state.formData.type === 0 ? "企业名称" : "姓名"}>
                {state.formData.name}
              </tz-cell>
              {state.formData.type === 0 ? (
                <tz-cell title="公司简称">{state.formData.shortName}</tz-cell>
              ) : (
                <tz-cell title="手机号">{state.formData.mobile}</tz-cell>
              )}
              <tz-cell
                title={state.formData.type === 0 ? "信用代码" : "身份证号"}
              >
                {state.formData.cardNumber}
              </tz-cell>
            </>
          )
        }
      </tz-view>
      {
        /* 未验证，已拒绝 */
        [0, 2].includes(state.formData.verifyStatus) && (
          <tz-button class="submit-btn" type="primary" onClick={handleSubmit}>
            提交申请
          </tz-button>
        )
      }
    </>
  );
};
