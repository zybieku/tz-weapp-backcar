export const useAccountType = (state) => {
  //账号类型选择
  const handleChangeType = () => {
    if (state.formData.type === 0) {
      //企业时，需要营业执照,不需要资料三
      state.pics[0] = state.accessoryVOList[0].filePath;
      state.pics[5] = "";
    } else {
      //个人时，需要资料三,不需要营业执照
      state.pics[0] = "";
      state.pics[5] = state.accessoryVOList[5].filePath;
    }
  };
  return {
    render: () => (
      <tz-view class="item">
        <tz-cell class="required" title="账号类型">
          <tz-picker
            disabled={![0, 2].includes(state.formData.verifyStatus)}
            mode="selector"
            range={state.typeOptions}
            v-model={state.formData.type}
            rangeKey="label"
            valueKey="value"
            onChange={handleChangeType}
          ></tz-picker>
          <tz-view class="link-area">
            <tz-icon name="arrow-right"></tz-icon>
          </tz-view>
        </tz-cell>
      </tz-view>
    ),
    handleChangeType,
  };
};
