import { apiListMultipleType, deletePic, uploadPic } from "@/api/basicApi";
import { imgCamera, imgFront, imgReverse } from "@/assets";
import Taro from "@tarojs/taro";
import { getFirmId } from "@tz-mall/shared";
import { autonymUploadEnum } from "../constanst";

export const useUpload = (state, handleChangeType) => {
  const typeJson = {
    0: autonymUploadEnum.BusinessLicense,
    1: autonymUploadEnum.NationalEmblem,
    2: autonymUploadEnum.Portrait,
    3: autonymUploadEnum.OtherInfor1,
    4: autonymUploadEnum.OtherInfor2,
    5: autonymUploadEnum.OtherInfor3,
  };
  const typeEnumJson = {};
  for (const key of Object.keys(typeJson)) {
    typeEnumJson[typeJson[key]] = Number(key);
  }
  // 获取已上传的图片
  const getImgList = () => {
    apiListMultipleType({
      bizId: getFirmId(),
      bizTypes: Object.values(autonymUploadEnum),
    })
      .then((data) => {
        for (const key in data) {
          if (data[key].length > 0) {
            state.accessoryVOList[typeEnumJson[key]] = {
              ...data[key][0],
              ...{ filePath: data[key][0].url },
            };
            //查看大图集合，仅图片
            state.pics[typeEnumJson[key]] = data[key][0].url;
          }
        }
        handleChangeType();
      })
      .catch(() => {
        return false;
      });
  };

  const handleChooseImage = async (type) => {
    if (![0, 2].includes(state.formData.verifyStatus)) {
      return false;
    }
    const { tempFilePaths, tempFiles } = await Taro.chooseImage({
      count: 1,
      sizeType: ["compressed"],
      sourceType: ["camera", "album"],
    }).catch((err) => {
      console.log(err);
      return { tempFilePaths: [], tempFiles: [] };
    });
    if (!tempFilePaths.length) return;
    if (tempFiles[0].size > 4000000) {
      $TzNotify.danger("上传图片不能大于4M!");
      return false;
    }

    const obj = {
      id: "",
      formData: {
        bizId: getFirmId(),
        bizType: typeJson[type],
      },
      filePath: tempFilePaths[0],
    };
    //上传图片
    const data = await uploadImgs(obj);
    if (data.id) {
      obj.id = data.id;
      state.accessoryVOList[type] = obj;
      //查看大图集合，仅图片
      state.pics[type] = tempFilePaths[0];
    }
  };

  //上传图片
  const uploadImgs = async (obj) => {
    const res = await uploadPic(obj).catch((res) => {
      $TzNotify.danger(JSON.parse(res.data)?.msg || "上传图片错误,请重试");
    });
    return res && res.code === 0 ? res.data : {};
  };

  //删除图片
  const handleClose = async (type) => {
    if (![0, 2].includes(state.formData.verifyStatus)) {
      return false;
    }
    const obj = state.accessoryVOList[type];
    const res = await deletePic({ id: obj.id }).catch((err) => {
      $TzNotify.danger(err.data.msg || "删除失败");
      return false;
    });
    if (res) {
      //删除图片
      const n = state.pics.indexOf(obj.filePath);
      state.accessoryVOList[type] = {};
      state.pics[n] = "";
    }
  };

  const numberJson = {
    0: "一",
    1: "二",
    2: "三",
  };
  return {
    render: () => (
      <>
        {state.formData.type === 0 && (
          <tz-view class="item">
            <tz-view class="label required">营业执照</tz-view>
            <tz-view class="tags largeTags">
              {state.accessoryVOList[0].filePath ? (
                <div class="position-rel">
                  <tz-image
                    class="upload"
                    src={state.accessoryVOList[0].filePath}
                    mode="scaleToFill"
                    onClick={() =>
                      Taro.previewImage({
                        current: state.accessoryVOList[0].filePath,
                        urls: state.pics.filter(Boolean),
                      })
                    }
                  ></tz-image>
                  {[0, 2].includes(state.formData.verifyStatus) && (
                    <tz-view class="close" onClick={() => handleClose(0)}>
                      x
                    </tz-view>
                  )}
                </div>
              ) : (
                <tz-view class="upload" onClick={() => handleChooseImage(0)}>
                  <tz-image src={imgCamera}></tz-image>
                  上传营业执照
                </tz-view>
              )}
            </tz-view>
          </tz-view>
        )}
        <tz-view class="item">
          <tz-view class="label required">
            {state.formData.type === 0 ? "法人" : "个人身份证"}信息
          </tz-view>
          <tz-view class="flex">
            <div class="cardTags">
              <tz-view class="tags">
                {state.accessoryVOList[1].filePath ? (
                  <div class="position-rel">
                    <tz-image
                      class="upload"
                      src={state.accessoryVOList[1].filePath}
                      mode="scaleToFill"
                      onClick={() =>
                        Taro.previewImage({
                          current: state.accessoryVOList[1].filePath,
                          urls: state.pics.filter(Boolean),
                        })
                      }
                    ></tz-image>
                    {[0, 2].includes(state.formData.verifyStatus) && (
                      <tz-view class="close" onClick={() => handleClose(1)}>
                        x
                      </tz-view>
                    )}
                  </div>
                ) : (
                  <>
                    <tz-view
                      class="upload"
                      onClick={() => handleChooseImage(1)}
                    >
                      <tz-image src={imgReverse}></tz-image>
                    </tz-view>
                  </>
                )}
              </tz-view>
              <tz-view class="uploadTitle">身份证-国徽面</tz-view>
            </div>
            <div class="cardTags">
              <tz-view class="tags">
                {state.accessoryVOList[2].filePath ? (
                  <div class="position-rel">
                    <tz-image
                      class="upload"
                      src={state.accessoryVOList[2].filePath}
                      mode="scaleToFill"
                      onClick={() =>
                        Taro.previewImage({
                          current: state.accessoryVOList[2].filePath,
                          urls: state.pics.filter(Boolean),
                        })
                      }
                    ></tz-image>
                    {[0, 2].includes(state.formData.verifyStatus) && (
                      <tz-view class="close" onClick={() => handleClose(2)}>
                        x
                      </tz-view>
                    )}
                  </div>
                ) : (
                  <>
                    <tz-view
                      class="upload"
                      onClick={() => handleChooseImage(2)}
                    >
                      <tz-image src={imgFront}></tz-image>
                    </tz-view>
                  </>
                )}
              </tz-view>
              <tz-view class="uploadTitle">身份证-人像面</tz-view>
            </div>
          </tz-view>
        </tz-view>
        <tz-view class="item">
          <tz-view class="label">其他资料(选填)</tz-view>
          <tz-view class="flex">
            {state.accessoryVOList
              .slice(3, state.formData.type === 0 ? 5 : 6)
              .map((item, i) => {
                const otherUpload = item?.filePath ? (
                  <div class="position-rel">
                    <tz-image
                      class="upload"
                      src={item.filePath}
                      mode="scaleToFill"
                      onClick={() =>
                        Taro.previewImage({
                          current: item.filePath,
                          urls: state.pics.filter(Boolean),
                        })
                      }
                    ></tz-image>
                    {[0, 2].includes(state.formData.verifyStatus) && (
                      <tz-view class="close" onClick={() => handleClose(3 + i)}>
                        x
                      </tz-view>
                    )}
                  </div>
                ) : (
                  <tz-view
                    class="upload"
                    onClick={() => handleChooseImage(3 + i)}
                  >
                    <div>
                      <tz-image src={imgCamera}></tz-image>
                      <p>资料{numberJson[i]}</p>
                    </div>
                  </tz-view>
                );
                return (
                  <tz-view key={i} class="tags largeTags">
                    {otherUpload}
                  </tz-view>
                );
              })}
          </tz-view>
        </tz-view>
      </>
    ),
    getImgList,
  };
};
