/**
 * 实名认证上传附件 枚举
 * uc_verify_card_national_emblem 身份证国徽面附件
 * uc_verify_card_portrait 身份证人像面附件
 * uc_verify_business_license 营业执照或其它资料附件
 * uc_verify_others_one 其它资料1附件
 * uc_verify_others_two 其它资料2附件
 * uc_verify_others_three 其它资料3附件
 */

export const autonymUploadEnum = {
  /**
   * 身份证国徽面附件
   */
  NationalEmblem: "uc_verify_card_national_emblem",
  /**
   * 身份证人像面附件
   */
  Portrait: "uc_verify_card_portrait",
  /**
   * 营业执照托
   */
  BusinessLicense: "uc_verify_business_license",
  /**
   * 其它资料1
   */
  OtherInfor1: "uc_verify_others_one",
  /**
   * 其它资料2
   */
  OtherInfor2: "uc_verify_others_two",
  /**
   * 其它资料3
   */
  OtherInfor3: "uc_verify_others_three",
};
