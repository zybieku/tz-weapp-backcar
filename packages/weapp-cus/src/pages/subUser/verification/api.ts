import { api } from "@tz-mall/shared";

//企业验证获取详情
export const getDetail = () => {
  const option = {
    url: "/uc/customer/verify/getVOById",
  };
  return api.post(option);
};
//提交申请
export const submitApply = (data = {}) => {
  const option = {
    url: `/uc/customer/verify/submitApply/`,
    data,
  };
  return api.post(option);
};
