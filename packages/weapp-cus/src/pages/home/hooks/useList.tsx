import { reactive } from "vue";
import { useAppStore } from "@tz-mall/shared";
import { apiAppletLineOfferList, getDetail } from "../api";
import type { ILineOffer, ITransReq } from "../types";
import { icHomeHot } from "@/assets";
import Taro from "@tarojs/taro";

export const useList = (navigateTo, state) => {
  const app = useAppStore();
  const listState = reactive({
    list: [] as ILineOffer[],
  });

  const getList = (queryParams = {} as ITransReq) => {
    const params = Object.assign({ specialOfferFlag: true }, queryParams);
    if (queryParams.consigneeAddr) {
      params.specialOfferFlag = false;
    }
    apiAppletLineOfferList(params).then((res) => {
      listState.list = res.data;
    });
  };

  getList();

  const handleOrderClick = (item) => {
    navigateTo({
      path: "/pages/subOrder/placeOrder/index",
      query: {
        packId: state.packId,
        dischargeId: state.dischargeId,
        comLinesId: state.comLinesId,
        lineOfferId: item.lineOfferId,
      },
    });
  };
  const handleProductDetail = (item) => {
    navigateTo({
      path: "/pages/subOrder/productDetail/index",
      query: {
        lineOfferId: item.lineOfferId,
        productCode: item.productCode,
      },
    });
  };

  const handleEffective = (type, item) => {
    if (!app.isLogin) {
      navigateTo({ path: "/pages/login/index" });
      return;
    }
    getDetail({ id: item.lineOfferId })
      .then(() => {
        if (type === "ProductDetail") {
          handleProductDetail(item);
        } else if (type === "OrderClick") {
          handleOrderClick(item);
        }
      })
      .catch((res) => {
        Taro.showToast({
          title: res.data.msg,
          icon: "none",
          duration: 2000,
        });
      });
  };

  return {
    listState,
    getList,
    render: () => (
      <div class="section-list">
        {listState.list.map((item, index) => {
          const validityStart = item.validityStart.slice(5, 16);
          const validityEnd = item.validityEnd.slice(5, 16);

          return (
            <div class="item" key={index}>
              <div class="title">
                <tz-view class="address">
                  <tz-view class="icon">
                    <i class="circle"></i>
                    <i class="wire"></i>
                    <tz-view class="arrow">
                      <tz-icon name="d-arrow-right"></tz-icon>
                    </tz-view>
                    <i class="wire"></i>
                    <i class="circle"></i>
                  </tz-view>
                  <tz-view class="area">
                    <div>{`${item.consignorAddr}/${item.consignorTown}`}</div>
                    <div>{`${item.consigneeAddr}/${item.consigneeTown}`}</div>
                  </tz-view>
                </tz-view>
              </div>
              <div class="info">
                <div class="car-type red">
                  <tz-icon name="yunshuche"></tz-icon>车型: {item.truckTonnages}
                </div>
                <div class="car-type">
                  <tz-icon name="zaizhong"></tz-icon>载重: {item.truckBoatload}
                  KG
                </div>
                <div class="car-type w100">
                  <tz-icon name="rongji"></tz-icon>车厢容积: {item.truckVolume}
                  CBM
                </div>
                <div class="car-type w100">
                  <tz-icon name="chicun"></tz-icon>尺寸: {item.truckLength}m*
                  {item.truckWidth}m*{item.truckHeight}m
                </div>
                <div class="time">
                  <tz-icon name="shijian"></tz-icon>
                  有效时间:
                  {` ${validityStart}~${validityEnd}`}
                </div>
                <tz-image
                  class="img-flag"
                  v-show={item.specialOfferFlag}
                  src={icHomeHot}
                ></tz-image>
              </div>
              <div class="label-line">
                {item.lineLabelName.split(",").map((item) => {
                  return <div class="label-item">{item}</div>;
                })}
              </div>
              <div className="bottom">
                <div class="price">￥{item.sellingPrice}</div>
                <div class="link-detail">
                  <tz-view
                    onClick={() => {
                      handleEffective("ProductDetail", item);
                    }}
                  >
                    查看产品详情
                  </tz-view>
                  <tz-button
                    type="primary"
                    round
                    class="btn-order"
                    onClick={() => {
                      handleEffective("OrderClick", item);
                    }}
                  >
                    去下单
                  </tz-button>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    ),
  };
};
