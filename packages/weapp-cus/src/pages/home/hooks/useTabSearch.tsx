import { deepclone, useTzRouter, useAppStore } from "@tz-mall/shared";
import { onMounted, reactive, ref } from "vue";
import { apiListGuangdongHongKong } from "../api";
import { ICity } from "../types";
import Taro from "@tarojs/taro";
export const useTabSearch = (getList, navigateTo, state) => {
  const app = useAppStore();
  const { route, EventChannel } = useTzRouter();
  const tabState = reactive({
    activeIndex: 0,
    picker: {
      from: [],
      target: [],
    },
    idStorage: {
      packId: "",
      dischargeId: "",
      comLinesId: "",
    },
    range: {
      from: [] as ICity[],
      target: [] as ICity[],
      fromLevel: 4,
      targetLevel: 3,
    },
    comLinesDetail: {},
  });
  EventChannel.on(route.path, (event) => {
    console.log(event);
    if (event.type === "selectAddre") {
      if (event.data.type === "pack") {
        tabState.idStorage.packId = event.data.id;
        tabState.idStorage.comLinesId = "";
        tabState.picker.from = event.data.addressAreaName.split("/");
      } else if (event.data.type === "discharge") {
        tabState.idStorage.dischargeId = event.data.id;
        tabState.idStorage.comLinesId = "";
        tabState.picker.target = event.data.addressAreaName.split("/");
      }
    } else if (event.type === "selectComLines") {
      tabState.idStorage.packId = "";
      tabState.idStorage.dischargeId = "";
      tabState.idStorage.comLinesId = event.data.id;
      tabState.picker.from = event.data.consignerRegionName.split("/");
      tabState.picker.target = event.data.consigneeRegionName.split("/");
    } else if (event.type === "deactivate") {
      setTimeout(() => {
        Taro.showToast({
          title: event.data.msg,
          icon: "none",
          duration: 2000,
        });
      }, 300);
    }
  });

  const tabList = [
    {
      title: "大陆到香港",
      from: {
        level: 4,
        code: "440000",
      },
      target: {
        level: 3,
        code: "810000",
      },
    },
    {
      title: "香港到大陆",
      from: {
        level: 3,
        code: "810000",
      },
      target: {
        level: 4,
        code: "440000",
      },
    },
  ];
  const range = {} as Record<string, ICity[]>;

  const handleTabClick = (index: number) => {
    tabState.activeIndex = index;
    const from = tabList[index].from;
    const target = tabList[index].target;

    tabState.range.from = range[from.code];
    tabState.range.target = range[target.code];

    tabState.range.fromLevel = from.level;
    tabState.range.targetLevel = target.level;

    tabState.picker = {
      from: [],
      target: [],
    };
  };

  //地址薄
  const toAddressBook = (type) => {
    if (!app.isLogin) {
      navigateTo({ path: "/pages/login/index" });
      return;
    }
    navigateTo({
      path: "/pages/subLineMgr/addressBook/index",
      query: {
        type: type,
      },
    });
  };
  //常用路线
  const toCommonLines = () => {
    if (!app.isLogin) {
      navigateTo({ path: "/pages/login/index" });
      return;
    }
    navigateTo({
      path: "/pages/subLineMgr/commonLines/index",
      query: {},
    });
  };

  onMounted(() => {
    apiListGuangdongHongKong().then((res) => {
      res.data.forEach((curr) => {
        curr.list = [{ code: curr.code, name: curr.name, list: curr.list }];
        range[curr.code] = curr.list;
      });
      handleTabClick(0);
    });
  });
  //装货地选择改变
  const pickerFormChange = () => {
    console.log("装货地选择改变");
    state.packId = "";
    state.comLinesId = "";
  };
  //卸货地选择改变
  const dischargeFormChange = () => {
    console.log("卸货地选择改变");
    state.dischargeId = "";
    state.comLinesId = "";
  };

  const cells = [
    {
      field: "from",
      attrs: {
        border: true,
      },
      component: {
        render: () => {
          return (
            <>
              <div>
                <tz-icon class="label-icon red" name="zhuang"></tz-icon>
              </div>
              <tz-multi-picker
                v-model={tabState.picker.from}
                data={tabState.range.from}
                placeholder="请选择装货地"
                onChange={pickerFormChange}
                level={tabState.range.fromLevel}
              ></tz-multi-picker>
              <tz-button
                link
                class="address-book"
                size="small"
                onClick={() => {
                  toAddressBook("pack");
                }}
              >
                <span class="division">|</span> 地址薄
              </tz-button>
            </>
          );
        },
      },
    },
    {
      field: "target",
      attrs: {
        border: true,
      },
      component: {
        render: () => {
          return (
            <>
              <div>
                <tz-icon class="label-icon blue" name="xie"></tz-icon>
              </div>
              <tz-multi-picker
                v-model={tabState.picker.target}
                data={tabState.range.target}
                placeholder="请选择卸货地"
                onChange={dischargeFormChange}
                level={tabState.range.targetLevel}
              ></tz-multi-picker>
              <tz-button
                link
                class="address-book"
                size="small"
                onClick={() => {
                  toAddressBook("discharge");
                }}
              >
                <span class="division">| </span>地址薄
              </tz-button>
            </>
          );
        },
      },
    },
  ];

  const handleSearch = () => {
    const { from, target } = deepclone(tabState.picker);
    //发货
    const consignorTown = from.splice(-1)[0];
    const consignorAddr = from.join("/");
    //收货
    const consigneeTown = target.splice(-1)[0];
    const consigneeAddr = target.join("/");
    if (consignorAddr && !consigneeAddr) {
      Taro.showToast({
        title: "请选择卸货地",
        icon: "none",
        duration: 2000,
      });
      return;
    } else if (!consignorAddr && consigneeAddr) {
      Taro.showToast({
        title: "请选择装货地",
        icon: "none",
        duration: 2000,
      });
      return;
    }
    state.packId = tabState.idStorage.packId;
    state.dischargeId = tabState.idStorage.dischargeId;
    state.comLinesId = tabState.idStorage.comLinesId;
    console.log(
      "packId:" +
        state.packId +
        "dischargeId:" +
        state.dischargeId +
        "comLinesId:" +
        state.comLinesId
    );
    getList({
      consignorTown,
      consignorAddr,
      consigneeAddr,
      consigneeTown,
    });
  };
  //清空
  const handleEmptyAddress = () => {
    tabState.picker = {
      from: [],
      target: [],
    };
    tabState.idStorage.packId = "";
    tabState.idStorage.dischargeId = "";
    tabState.idStorage.comLinesId = "";
  };

  return () => (
    <div class="section-search">
      <div class="search-tabbar">
        {tabList.map((tab, index) => {
          return (
            <div
              class={[
                "tab",
                tabState.activeIndex === index ? "tab_active" : "",
              ]}
              key={index}
              onClick={() => handleTabClick(index)}
            >
              {tab.title}
            </div>
          );
        })}
      </div>
      <div class="search-form">
        <tz-cell-form cells={cells} v-model={tabState.picker}></tz-cell-form>
        <div class="common-lines">
          <tz-button
            link
            class="choose-lines line-but"
            size="small"
            onClick={toCommonLines}
          >
            <tz-icon name="changyongxianlu"></tz-icon>
            常用路线
          </tz-button>
          <tz-button
            link
            class="line-empty line-but"
            size="small"
            onClick={handleEmptyAddress}
          >
            清空
          </tz-button>
        </div>

        <div class="btn-save">
          <tz-button round size="small" type="warning" onClick={handleSearch}>
            查询
          </tz-button>
        </div>
      </div>
    </div>
  );
};
