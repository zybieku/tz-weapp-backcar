/**
 * 图片统一管理，方便全局替换
 */
export { default as icLogo } from "@/assets/images/ic_logo.png";
export { default as icTitleLogo } from "@/assets/images/ic_title_logo.png";

//radio
export { default as icRadio } from "@/assets/images/ic_radio.png";
export { default as icRadioActive } from "@/assets/images/ic_radio_active.png";

const baseUrl = "https://minio.kuajingzhilian.com/wx-applet";

//用户协议
const htmlVer = (Date.now() / (1000 * 60 * 10)).toFixed(0);
export const userAgreementUrl = `${baseUrl}/html/cus/register.html?${htmlVer}`;
export const userPrivateUrl = `${baseUrl}/html/cus/user.html?${htmlVer}`;
export const userAboutUrl = `${baseUrl}/html/cus/about.html?${htmlVer}`;

//首页banner
export const getHomeBannerArr = () => {
  const _baseUrl = baseUrl + "/car_20230104/img_home_banner";
  const arr = [".png"];
  //5分钟一次缓存
  const version = (Date.now() / (1000 * 60 * 30)).toFixed(0);
  return arr.map((url) => {
    return `${_baseUrl + url}?${version}`;
  });
};
export { default as icHomeHot } from "@/assets/images/ic_home_hot.png";
export const imgHomeEmpty = baseUrl + "/car_20230104/img_home_empty.png";

//登录
export const bgLogin = baseUrl + "/car_20230104/bg_login.jpg";

//我的
export const bgMineHeader = baseUrl + "/car_20230104/bg_mine_header.png";

// 企业认证
export { default as imgCamera } from "@/assets/images/img_camera.png";
export { default as imgFront } from "@/assets/images/img_front.png";
export { default as imgReverse } from "@/assets/images/img_reverse.png";

// 地址簿
export { default as imgAddress } from "@/assets/images/img_address.png";
export { default as imgCommonLines } from "@/assets/images/img_common_lines.png";

// 订单
export const imgOrderNodata = `${baseUrl}/car_20230104/img_order_nodata.png`;
export const imgOrderUnlogin = `${baseUrl}/car_20230104/img_order_unlogin.png`;
export const imgOrderSuccess = `${baseUrl}/car_20230104/img_order_success.png`;
export const imgPaySuccess = `${baseUrl}/car_20230104/img_pay_success.png`;

//轨迹
export { default as imgLocusEmpty } from "@/assets/images/img_locus.png";
