export default {
  pages: [
    "pages/home/index",
    "pages/order/index",
    "pages/mine/index",
    "pages/login/index",
    "pages/locus/index",
  ],
  subpackages: [
    {
      //sub 用户中心自包
      root: "pages/subUser",
      pages: [
        "register/index",
        "findPwd/index",
        "webPage/index",
        "verification/index",
      ],
    },
    {
      //sub 订单,下单列表
      root: "pages/subOrder",
      pages: [
        "placeOrder/index",
        "commodity/index",
        "orderDetail/index",
        "feeDetail/index",
        "historyFeeDetail/index",
        "paySuccess/index",
        "productDetail/index",
      ],
    },
    {
      //addreBookLine 地址簿，常用路线
      root: "pages/subLineMgr",
      pages: [
        "addressBook/index",
        "addressEdit/index",
        "commonLines/index",
        "comLinesEdit/index",
      ],
    },
    {
      //locus  轨迹
      root: "pages/subLocus",
      pages: ["locusDetail/index", "locusAttaImg/index"],
    },
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "粤港回头车",
    navigationBarTextStyle: "black",
    onReachBottomDistance: 50,
  },
  requiredPrivateInfos: ["chooseLocation"],
  tabBar: {
    color: "#626567",
    selectedColor: "#1c78ef",
    backgroundColor: "#FBFBFB",
    borderStyle: "white",
    position: "bottom",
    list: [
      {
        pagePath: "pages/home/index",
        text: "首页",
        selectedIconPath: "assets/images/ic_home_active.png",
        iconPath: "assets/images/ic_home.png",
      },
      {
        pagePath: "pages/order/index",
        text: "订单",
        selectedIconPath: "assets/images/ic_order_active.png",
        iconPath: "assets/images/ic_order.png",
      },
      {
        pagePath: "pages/locus/index",
        text: "轨迹",
        selectedIconPath: "assets/images/ic_locus_active.png",
        iconPath: "assets/images/ic_locus.png",
      },
      {
        pagePath: "pages/mine/index",
        text: "我的",
        selectedIconPath: "assets/images/ic_user_active.png",
        iconPath: "assets/images/ic_user.png",
      },
    ],
  },
};
