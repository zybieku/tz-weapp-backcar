let conf = require("@tz-mall/conf");

let config = {
  defineConstants: {
    //api版本号,用于后台区分灰度和正式环境
    ENV_API_VERSION: '"y1.1.0"',
    ENV_HOME_URL: '"/pages/home/index"',
    LOCATION_APIKEY: JSON.stringify("XXOBZ-5LYWW-UKRRG-3GR7A-QQZO2-LJB2A"),
  },
};

module.exports = function (merge) {
  return conf.merge(config, merge);
};
