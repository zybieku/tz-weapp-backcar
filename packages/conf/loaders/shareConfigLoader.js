const path = require("path");
const fs = require("fs");
module.exports = function(ctx, options) {
  // plugin 主体
  ctx.onBuildStart(() => {
    const { sourcePath } = ctx.paths;
    fileDisplay(path.join(sourcePath, "pages"));
  });
  ctx.onBuildFinish(() => {
    console.log("编译结束！");
  });
};

function witeConfig(path, data) {
  let content = data;
  //if (content.includes(`enableShareAppMessage`)) return content;
  content = content.replace(/  enableShareTimeline: true,\r\n/, "");
  // content = content.replace(/export default.*\{/, (reg) => {
  //   return `${reg}
  // enableShareAppMessage: true,
  // enableShareTimeline: true,`;
  // });
  console.log(content);
  return fs.writeFileSync(path, content);
}

/**
 * 文件遍历方法
 * @param filePath 需要遍历的文件路径
 */
function fileDisplay(filePath) {
  //根据文件路径读取文件，返回文件列表
  const files = fs.readdirSync(filePath);
  files.forEach(function(filename) {
    //获取当前文件的绝对路径
    const filedir = path.join(filePath, filename);
    //根据文件路径获取文件信息，返回一个fs.Stats对象
    const stats = fs.statSync(filedir);
    const isFile = stats.isFile(); //是文件
    if (isFile && filedir.endsWith("index.config.js")) {
      const data = fs.readFileSync(filedir, "utf-8");
      witeConfig(filedir, data);
    }
    const isDir = stats.isDirectory(); //是文件夹
    if (isDir) {
      fileDisplay(filedir);
    }
  });
}
