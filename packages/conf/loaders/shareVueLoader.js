module.exports = function (source) {
  let content = source;
  if (content.includes(`{useShareMsg }`)) return content;
  if (content.includes(`useManualShareMsg`)) return content;
  content = content
    .replace(/export default.*\{/, (reg) => {
      return `import {useShareMsg } from '@/vhooks/useShareMsg';

      ${reg}`;
    })
    .replace(/setup\(.*\) {/, (reg) => {
      return `${reg}
      useShareMsg();

      `;
    });
  return content;
};
