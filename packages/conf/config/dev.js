const baseApi = "https://tzmallgatewaytest.sztaizhou.com";
const downloadApi = "https://miniotest.sztaizhou.com";
const isH5 = process.env.TARO_ENV === "h5";

module.exports = {
  env: {
    NODE_ENV: '"development"',
  },
  defineConstants: {
    ENV_BASE_API: isH5 ? '"/api"' : JSON.stringify(baseApi),
    ENV_DOWNLOAD_API: JSON.stringify(downloadApi),
  },
  mini: {},
  h5: {
    devServer: {
      proxy: {
        "/api": {
          target: baseApi,
          pathRewrite: {
            "^/api": "",
          },
          changeOrigin: true,
        },
      },
    },
  },
};
