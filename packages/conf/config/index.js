let path = require("path");

const getEnvVersion = () => {
  if (process.env.NODE_ENV === "development") return "develop";
  if (process.argv.includes("api_prod")) return "release";
  return "trial";
};

let getOutputRoot = () => {
  let cwd = process.cwd();
  let baseName = path.basename(cwd);
  return path.resolve(cwd, "../../", `deploy/${baseName}/dist`);
};

const config = {
  projectName: "tz-weApp-driver",
  date: "2022-12-19",
  designWidth: 375,
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
    375: process.env.TARO_ENV === "h5" ? 640 / 375 : 2 / 1,
  },
  sourceRoot: "src",
  outputRoot: getOutputRoot(),
  plugins: [],
  defineConstants: {
    TZ_ENV_VESION: JSON.stringify(getEnvVersion()),
  },
  copy: {
    patterns: [],
    options: {},
  },
  alias: {
    "@": path.resolve(process.cwd(), "src"),
  },
  sass: {
    resource: [path.resolve(process.cwd(), "src/styles/variables.scss")],
  },
  framework: "vue3",
  compiler: {
    type: "webpack5",
    // 仅 webpack5 支持依赖预编译配置
    prebundle: {
      enable: false,
    },
  },
  cache: {
    enable: false,
  },
  plugins: ["@tarojs/plugin-html"],
  mini: {
    webpackChain(chain) {
      chain.merge({
        module: {
          rule: {
            mjsScript: {
              test: /\.mjs$/,
              include: [/pinia/],
              use: {
                babelLoader: {
                  loader: require.resolve("babel-loader"),
                },
              },
            },
          },
        },
      });
    },
    postcss: {
      pxtransform: {
        enable: true,
        config: {},
      },
      url: {
        enable: true,
        config: {
          limit: 1024, // 设定转换尺寸上限
        },
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: "module", // 转换模式，取值为 global/module
          generateScopedName: "[name]__[local]___[hash:base64:5]",
        },
      },
    },
    // 自定义 Webpack 配置
    webpackChain(chain) {
      chain.plugin("providerPlugin").tap((args) => {
        args[0].$TzNotify = ["@tz-mall/weapp-ui", "TzNotify"];
        return args;
      });
    },
  },
  h5: {
    publicPath: "/",
    staticDirectory: "static",
    postcss: {
      autoprefixer: {
        enable: true,
        config: {},
      },
      cssModules: {
        enable: false, // 默认为 false，如需使用 css modules 功能，则设为 true
        config: {
          namingPattern: "module", // 转换模式，取值为 global/module
          generateScopedName: "[name]__[local]___[hash:base64:5]",
        },
      },
    },
    // 自定义 Webpack 配置
    webpackChain(chain, webpack) {
      chain.plugin("providerPlugin").use(webpack.ProvidePlugin, [
        {
          $TzNotify: ["@tz-mall/weapp-ui", "TzNotify"],
        },
      ]);
    },
  },
};

module.exports.merge = function (_config, mergeFunc) {
  if (process.env.NODE_ENV === "development") {
    return mergeFunc({}, config, _config, require("./dev"));
  }
  return mergeFunc({}, config, _config, require("./prod"));
};
