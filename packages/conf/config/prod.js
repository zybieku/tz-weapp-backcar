let path = require("path");
let baseApi = "https://tzmallgatewaytest.sztaizhou.com";
let downloadApi = "https://fastdfstest.sztaizhou.com";

//prod指令控制 链接后台xcx正式服务器
if (process.argv.includes("api_prod")) {
  baseApi = "https://gateway.kuajingzhilian.com";
  downloadApi = "https://minio.kuajingzhilian.com";
}
console.log("\x1B[31m%s\x1B[0m", "------- 本次打包后台地址 ---------------");
console.log("\x1B[36m%s\x1B[0m", "-------- " + baseApi + "---------------");

const isH5 = process.env.TARO_ENV === "h5";

module.exports = {
  env: {
    NODE_ENV: '"production"',
  },
  defineConstants: {
    ENV_BASE_API: isH5 ? '"/api"' : JSON.stringify(baseApi),
    ENV_DOWNLOAD_API: JSON.stringify(downloadApi),
  },
  mini: {
    commonChunks: ["runtime", "vendors", "taro", "common"],
    webpackChain(chain) {
      chain.module
        .rule("sharevue")
        .test(function (path) {
          return path.includes("\\src\\pages\\") && path.endsWith("index.vue");
        })
        .use("shareVueLoader")
        .loader(path.resolve(__dirname, "../loaders/shareVueLoader"));

      chain.plugin("providerPlugin").tap((args) => {
        args[0].$TzNotify = ["@tz-mall/weapp-ui", "TzNotify"];
        return args;
      });

      /* chain
          .plugin("analyzer")
          .use(require("webpack-bundle-analyzer").BundleAnalyzerPlugin, []); */
    },
  },
  h5: {
    //如果h5端编译后体积过大，可以使用webpack-bundle-analyzer插件对打包体积进行分析。
    // 参考代码如下：
    webpackChain(chain, webpack) {
      chain.plugin("providerPlugin").use(webpack.ProvidePlugin, [
        {
          $TzNotify: ["@tz-mall/weapp-ui", "TzNotify"],
        },
      ]);
      /*     chain.merge({
        optimization: {
          splitChunks: {
            cacheGroups: {
              nutui: {
                name: "nutui",
                test: /[\\/]node_modules[\\/]@nutui[\\/]/,
                priority: 3,
                reuseExistingChunk: true,
              },
            },
          },
        },
      }),
        chain
          .plugin("analyzer")
          .use(require("webpack-bundle-analyzer").BundleAnalyzerPlugin, []); */
    },
  },
};
