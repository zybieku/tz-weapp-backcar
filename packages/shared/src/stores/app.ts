// https://pinia.esm.dev/introduction.html
import { defineStore } from "pinia";
import { clear, getToken, getUserInfo, setToken, setUserInfo } from "../utils";

interface IAppStore {
  userInfo: IUserInfo;
  token: string;
  isLogin: boolean;
  account: string;
  firmId: string;
  nickname?: string;
}

export const useAppStore = defineStore("app-store", {
  state: (): IAppStore => {
    const userInfo = getUserInfo();
    return {
      userInfo: userInfo,
      token: getToken(),
      isLogin: !!getToken(),
      account: userInfo.account,
      nickname: userInfo.nickname,
      firmId: userInfo.firmId,
    };
  },
  actions: {
    setToken(this: IAppStore, token: string) {
      this.token = token;
      this.isLogin = !!token;
      return setToken(token);
    },

    setUseInfo(this: IAppStore, userInfo: IUserInfo) {
      this.account = userInfo.account;
      this.nickname = userInfo.nickname;
      return setUserInfo(userInfo);
    },

    logout(this: IAppStore) {
      this.account = "";
      this.isLogin = false;
      this.token = "";
      this.userInfo = {} as IUserInfo;
      clear();
    },
  },
});
