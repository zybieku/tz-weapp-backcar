export const EnumUseType = {
  /**
   * 平台,后台
   */
  ADMIN: 0,
  /**
    普通用户
    (回头车小程序)
 */
  CUS: 1,
  /**
    供应商
    (小程序司机端)
  */
  BIZ: 2,
} as const;

export const EnumUserOrigin = {
  /**
   * PC
   */
  PC: 1,
  /**
   * Wap
   */
  Wap: 2,
  /**
   * 小程序
   */
  WEAPP: 3,
  /**
   * App
   */
  APP: 4,
} as const;

/**
 * 短信类型
 */
export const EnumSmsType = {
  /**
   *  标准发送
   * */
  STANDARD_SEND: "STANDARD_SEND",

  /**
   * web端注册(客户)
   */
  WEB_CUS_REGISTER: "WEB_C_REGISTER",

  /**
   * web端修改/忘记密码(客户)
   */
  WEB_CUS_UPDATE_PASSWORD: "WEB_C_UPDATE_PASSWORD",

  /**
   * web端登录(客户)
   */
  WEB_CUS_LOGIN: "WEB_C_LOGIN",

  /**
   * WX端注册(客户)
   */
  WX_CUS_REGISTER: "WX_C_REGISTER",

  /**
   * WX端注册(司机端)
   */
  WX_BIZ_REGISTER: "WX_P_REGISTER",

  /**
   * WX端忘记密码(客户)
   */
  WX_CUS_UPDATE_PASSWORD: "WX_C_UPDATE_PASSWORD",

  /**
   * WX端忘记密码(司机端)
   */
  WX_BIZ_UPDATE_PASSWORD: "WX_P_UPDATE_PASSWORD",

  /**
   * WX端登录(客户)
   */
  WX_CUS_LOGIN: "WX_C_LOGIN",

  /**
   * WX_BIZ_LOGIN  WX端登录(司机端)
   */
  WX_BIZ_LOGIN: "WX_P_LOGIN",
};
