import { EnumUserOrigin } from "../constants";
import api from "../tzAxios";
import { IUserInfoResp } from "./types";
/**
 * 获取token
 * @param {*} data
 * @returns
 */
export const apiLogin = (data) => {
  const option = {
    url: "/uc/account/login",
    data,
  };
  return api.post(option);
};

/**
 * 获取用户信息
 */
export const apiGetUserinfo = () => {
  const option = {
    url: "/uc/account/getUser",
  };
  return api.post<IUserInfoResp>(option);
};

/**
 * 提供 wx.login code 给后端获取 openId
 * @param {*} data
 * @param tmpCode
 */
export const apiRecordOpenId = (data) => {
  const option = {
    url: "/uc/account/recordOpenId",
    data,
  };
  return api.post(option);
};

/**
 * 发送验证嘛
 * @returns
 */
export const apiSendValidCode = (data) => {
  const option = {
    url: "/open/sms/send/sendSMS",
    data,
  };
  return api.post(option);
};

/**
 * 修改密码
 * @returns
 */
export const apiChangePwd2 = (data) => {
  const option = {
    url: "/uc/account/forgetPwd",
    data,
  };
  return api.post(option);
};

/**
 * 检查是否注册
 * @returns
 */
export const apiCheckNotRegister = (data) => {
  const option = {
    url: "/uc/account/checkNotRegister",
    data,
  };
  return api.post(option);
};
/**
 * 检查手机号是否注册
 * @param data
 * @returns
 */
export const apiCheckRegister = (data) => {
  const option = {
    url: "/uc/account/checkRegister",
    data,
  };
  return api.post(option);
};

/**
 * 更新 token 接口
 */
export const apiRefreshToken = (data) => {
  const option = {
    url: "/uc/account/refreshToken",
    data,
  };
  return api.post(option);
};

/**
 * 普通登录
 */
export const apiRegister = (data) => {
  const option = {
    url: "/uc/account/register",
    data: { userOrigin: EnumUserOrigin.WEAPP, ...data },
  };
  return api.post(option);
};

/**
 * 微信注册登录
 */
export const apiWxFastLogin = (data) => {
  const option = {
    url: "/uc/account/wxFastLogin",
    data,
  };
  return api.post(option);
};

/**
 * 获取各类型验证码的过期时间
 */
export const apiGetExpireTime = () => {
  const option = {
    url: "/open/sms/validCode/getExpireTime",
  };
  return api.post(option);
};
