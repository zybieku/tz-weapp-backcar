import { api } from "@tz-mall/shared";
import type { IPayResp } from "./types";

/**
 * 支付接口
 */
export const apiPay = (data) => {
  const option = {
    url: "/transport/pay/feeSubmit",
    data,
  };
  return api.post<IPayResp>(option);
};

/**
 * 更新支付结果
 * @param data
 * @returns
 */
export const apiUpdatePayStatus = (data) => {
  const option = {
    url: "/transport/pay/updatePayStatusByPayOrderNo",
    data,
  };
  return api.post(option);
};
/**
 * 根据代码集合查询产品详情
 * @param data
 * @returns
 */
export const listProductDetailByCode = (data) => {
  const option = {
    url: "/transport/base/productDetail/listProductDetailByCode",
    data,
  };
  return api.post(option);
};

/** 
获取粤港车下拉静态数据
所有前端页面静态的下拉数据，全部统一都后台配置后查询
@param []data
[{"type":"PRODUCT_DETAIL TYPE"}]
@returns
PRODUCT_DETAIL TYP:[]
*/
export const getDicMap = (data) => {
  const option = {
    url: "/transport/base/dropDown/getDropDownMap",
    data,
  };
  return api.post(option).then((res) => {
    return res.data;
  });
};
