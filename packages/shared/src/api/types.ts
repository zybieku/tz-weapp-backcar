export interface IUserInfoResp extends IBaseResp {
  data: IUserInfo;
}

export interface IPayResp extends IBaseResp {
  data: { nonceStr; prepayId; sign; timestamp; payOrderNo };
}
