import Taro from "@tarojs/taro";

const USER_ID_KEY = "tz_userId"; //uid
const FIRM_ID_KEY = "tz_firmId"; //uid
const TOKEN_KEY = "tz_token"; //token
const TOKEN_INFO_KEY = "tz_token_info"; //token_info
const USER_INFO_KEY = "tz_user_info"; //用户信息
const DICT_MAP_KEY = "tz_dict_map"; //数据字典

//设置UserToken
export function setToken(token) {
  return set(TOKEN_KEY, token);
}

//设置 UserTokenInfo
export function setTokenInfo(data: ITokenInfo) {
  data.now = Date.now();
  return set(TOKEN_INFO_KEY, data);
}

//设置UserId
export function setUserId(id: string) {
  set(USER_ID_KEY, id);
}

//设置firmId
export function setFirmId(id: string) {
  set(FIRM_ID_KEY, id);
}

//设置userInfo
export function setUserInfo(userInfo: IUserInfo) {
  setUserId(userInfo.id);
  setFirmId(userInfo.firmId);
  return set(USER_INFO_KEY, userInfo);
}

//设置数据字典 dicMap
export function setDicMap(dicMap) {
  return set(DICT_MAP_KEY, dicMap);
}

/* *************************** get ********************************** */

//获取Token
export function getToken() {
  return get<string>(TOKEN_KEY) || "";
}

//获取Token_info
export function getTokenInfo() {
  return get<ITokenInfo>(TOKEN_INFO_KEY) as ITokenInfo;
}

//获取 USER_ID
export function getUserId() {
  return get<string>(USER_ID_KEY) || "";
}

//获取 firm_ID
export function getFirmId() {
  return get<string>(FIRM_ID_KEY) || "";
}

//获取 USER_Info
export function getUserInfo() {
  return get<IUserInfo>(USER_INFO_KEY) || ({} as IUserInfo);
}

//获取数据字典 dicMap
export function getDicMap() {
  return get(DICT_MAP_KEY);
}

/* ****************************** remove ******************************* */

// 删除token
export function removeToken() {
  remove(TOKEN_KEY);
}

// 删除token_info
export function removeTokenInfo() {
  remove(TOKEN_INFO_KEY);
}

// 删除UID
export function removeUserId() {
  remove(USER_ID_KEY);
}
// 删除UINFO
export function removeUserInfo() {
  remove(USER_INFO_KEY);
}

/* ****************************** 包装方法 ******************************* */

export function set(key, content) {
  return Taro.setStorage({ key, data: content });
}

export function get<T>(key) {
  try {
    return Taro.getStorageSync<T>(key);
  } catch (error) {
    console.log(error);
    return null;
  }
}
export function remove(key) {
  return Taro.removeStorage(key);
}
export function clear() {
  return Taro.clearStorageSync();
}
