/**
 * 是否为闫年
 * @return {Boolse} true|false
 */
export const isLeapYear = (y: number) => {
  return (y % 4 === 0 && y % 100 !== 0) || y % 400 === 0;
};

/**
 * 返回对应日期是星期几
 * @return {String}
 */
export const getDayOfWeek = (dateStr: string | Date): string => {
  if (!dateStr) return "";
  let date = dateStr as Date;
  if (typeof dateStr === "string") {
    date = new Date(dateStr);
  }
  const index = date.getDay();
  const dayNames = [
    "星期日",
    "星期一",
    "星期二",
    "星期三",
    "星期四",
    "星期五",
    "星期六",
  ];
  return dayNames[index];
};

/**
 * 返回给定月份第一天星期数
 * @return {Number}
 */
export const getMonthPreDay = (
  year: number | string,
  month: number | string
) => {
  const date = new Date(year + "/" + month + "/01");
  let day = date.getDay();
  if (day === 0) {
    day = 7;
  }
  return day;
};

/**
 * 返回月份天数
 * @return {Number}
 */
export const getMonthDays = (year: number | string, month: number | string) => {
  month = month.toString();
  if (/^0/.test(month)) {
    month = month.split("")[1];
  }
  return [
    0,
    31,
    isLeapYear(Number(year)) ? 29 : 28,
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31,
  ];
};

/**
 * 日期对象转成字符串
 * @param date Date
 * @param format 比如 'yyyy-MM-dd HH:mm:ss'
 */
export function dateToString(date: Date, fmt = "YYYY-MM-DD HH:mm:ss"): string {
  let ret;
  const opt = {
    "Y+": date.getFullYear().toString(), // 年
    "M+": (date.getMonth() + 1).toString(), // 月
    "D+": date.getDate().toString(), // 日
    "H+": date.getHours().toString(), // 时
    "m+": date.getMinutes().toString(), // 分
    "s+": date.getSeconds().toString(), // 秒
  };

  // eslint-disable-next-line no-unused-vars
  for (const k in opt) {
    ret = new RegExp("(" + k + ")").exec(fmt);
    if (ret) {
      const length = ret[1].length;
      fmt = fmt.replace(
        ret[1],
        ret[1].length === 1
          ? opt[k]
          : opt[k].slice(-length).padStart(length, "0")
      );
    }
  }
  return fmt;
}

/**
 * 日期对象转成字符串(没有时分秒)
 * @return {string}
 */
export const date2DayStr = (date: Date) => {
  return dateToString(date, "YYYY-MM-DD");
};

/**
 * @param {String|Date} date date-日期
 * @param {String} fmt fmt-格式 YYYY-MM-DD HH:mm:ss
 * @returns date-日期
 */
export const dateFormat = (
  date: string | Date,
  fmt = "YYYY-MM-DD HH:mm:ss"
) => {
  if (!date) return "";
  if (typeof date === "string") {
    date = new Date(date.replace(/-/g, "/"));
  }
  return dateToString(date, fmt);
};

/**
 * 返回前后i天的日期字符串
 * @param {Number} 0返回今天的日期、1返回明天的日期，2返回后天得日期，依次类推
 * @return {string} '2014-12-31'
 */
export function geCurrDate(i = 0, format = "YYYY-MM-DD"): string {
  const today = new Date();
  const targetDate = new Date(today.getTime() + i * 24 * 60 * 60 * 1000);
  return dateToString(targetDate, format);
}

/**
 * 时间比较
 * @return {Boolean}
 */
export const compareDate = (date1: string, date2: string) => {
  const startTime = new Date(date1.replace(/-/g, "/"));
  const endTime = new Date(date2.replace(/-/g, "/"));
  if (startTime >= endTime) {
    return false;
  }
  return true;
};

/**
 * 时间是否相等
 * @return {Boolean}
 */
export const isEqual = (date1: string, date2: string) => {
  const startTime = new Date(date1).getTime();
  const endTime = new Date(date2).getTime();
  if (startTime === endTime) {
    return true;
  }
  return false;
};
