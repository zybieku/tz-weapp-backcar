import Big from "big.js";

declare global {
  interface Number {
    /**
     * 加法(精度)
     * @param {BigSource} n 加数
     */
    plus(n: BigSource): number;
    /**
     * 减法(精度)
     * @param {BigSource} n 减数
     */
    minus(n: BigSource): number;
    /**
     * 乘法(精度)
     * @param {BigSource} n 乘数
     */
    times(n: BigSource): number;
    /**
     * 除法(精度)
     * @param {BigSource} n 除数
     */
    div(n: BigSource): number;

    /**
     * 4舍5入(精度兼容)
     * @param {number} n 保留位数
     * @param {number} rm 0: 向下取整 1: 四舍五入 2: 向上取整
     */
    round(n: number, rm?: number): number;

    /**
     * 4舍6入5成双(精度兼容)
     * @param {number} n 保留位数
     */
    toFixed(n: number): string;
  }
}

type BigSource = number | string | Big;

(function (Number) {
  // Number原型添加Round方法, 用于四舍五入
  Number.prototype.round = function (n: number, rm = 1) {
    return new Big(this).round(n, rm).toNumber();
  };

  // 重写 Number 原型对象上的 toFixed 方法
  Number.prototype.toFixed = function (decimalPlaces: number) {
    return new Big(this.toString()).toFixed(decimalPlaces);
  };

  // 重写 Number 原型对象上的 toPrecision 方法
  Number.prototype.toPrecision = function (precision: number) {
    return new Big(this.toString()).toPrecision(precision);
  };

  //加法
  Number.prototype.plus = function (n: BigSource) {
    return new Big(this).plus(n).toNumber();
  };

  //减法
  Number.prototype.minus = function (n: BigSource) {
    return new Big(this).minus(n).toNumber();
  };

  //乘法
  Number.prototype.times = function (n: BigSource) {
    return new Big(this).times(n).toNumber();
  };

  //除法
  Number.prototype.div = function (n: BigSource) {
    return new Big(this).div(n).toNumber();
  };
})(Number);

export {};
