/**
 * axios二次封装,处理了一些公共参数,token,key加密相关处理
 * 可根据自己个人习惯,删除修改
 */

import Taro from "@tarojs/taro";
import "./interceptor";
import config from "./config";
import { copyObject, errHandler } from "./util";
import { getToken } from "../utils/storage";
import type { RespResult } from "./types";

/**
 * 参数转化,主要处理公共参数,md5等验签加密
 * @param url  url
 * @param params 加密前的对象
 * @returns {string} 加密后的数据
 */
const paramsConvert = (params: TaroGeneral.IAnyObject) => {
  return copyObject(params, config.defaultParams());
};

/**
 * httpClient发送axios请求
 * @param {Object} option  //参数
 */
export const httpClient = async <T>(option: Taro.request.Option) => {
  option.data = paramsConvert(option.data);
  option.header = copyObject(option.header, config.defaultHeaders());
  option.timeout = option.timeout || 8000;
  const pattern = /^http[s]?:\/\/.+/;
  if (!pattern.test(option.url)) {
    option.url = ENV_BASE_API + option.url;
  }
  option.fail = async (errResponse: RespResult) => {
    if (Taro.getEnv() === Taro.ENV_TYPE.WEB) {
      const { status, statusText, url } = errResponse || {};
      const data = await errResponse?.json();
      const error = { data, status, statusText, url };
      errHandler(error);
    }
  };
  return Taro.request(option) as unknown as Promise<T>;
};

//这里只是初步封装,可以自行抽离
export default {
  /**
   * get请求,参数默认在query中,用?拼接
   * @param {String} url url
   * @param {Object} params 参数
   * @param {*} argType 参数拼接方式 （TYPE_BODY即参数在Body中）
   * @returns  Promise
   */
  get: <T = any>(option: Taro.request.Option<T>) => {
    option["method"] = "GET";
    return httpClient<T>(option);
  },

  /**
   * delete请求,参数默认在query中,用?拼接
   * @param {String} url url
   * @param {Object} params 参数
   * @param {*} argType 参数拼接方式 TYPE_BODY参数在Body中
   * @returns  Promise
   */
  delete: <T = any>(option: Taro.request.Option<T>) => {
    option["method"] = "DELETE";
    return httpClient<T>(option);
  },

  /**
   * post请求
   * @param {Object} option {url,headers,data,params}
   * @returns  Promise
   */
  post: <T = any>(option: Taro.request.Option<T>) => {
    option["method"] = "POST";
    return httpClient<T>(option);
  },

  /**
   * put请求,参数默认在body中
   * @param {String} url url
   * @param {Object} params 参数
   * @param {*} argType 参数拼接方式 TYPE_URL即参数在URL后面
   * @returns  Promise
   */
  put: <T = any>(option: Taro.request.Option<T>) => {
    option["method"] = "PUT";
    return httpClient<T>(option);
  },

  /**
   * 文件上传默认使用表单上传
   * @param {string} url  api路由
   * @param {Object} params 参数
   */
  upload: <T = any>(option: Taro.uploadFile.Option) => {
    option.header = copyObject(option.header, config.defaultHeaders());
    option.header["Authorization"] = "Bearer " + getToken();
    option.timeout = option.timeout || 20000;
    option.name = option.name || "file";
    const pattern = /^http[s]?:\/\/.+/;
    if (!pattern.test(option.url)) {
      option.url = ENV_BASE_API + option.url;
    }
    option.fail = async (errResponse: RespResult) => {
      let error = errResponse;
      console.log(error);
      if (Taro.getEnv() === Taro.ENV_TYPE.WEB) {
        const { status, statusText, url } = errResponse || {};
        const data = await errResponse?.json();
        error = { data, status, statusText, url } as RespResult;
        errHandler(error);
      }
    };
    return Taro.uploadFile(option).then((res) => {
      if (res.statusCode !== 200) {
        errHandler(res);
        return Promise.reject(res);
      } else {
        const data =
          typeof res.data === "string" ? JSON.parse(res.data) : res.data;
        if (data.code !== 0) {
          return Promise.reject(res);
        }
        return Promise.resolve(data) as unknown as Promise<T>;
      }
    });
  },
};
