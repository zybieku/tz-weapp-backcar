const md5Params = {
  md5key: "TJTXOswvftDc7zNc",
  signKey: "sign",
};

const defaultParams = (): TaroGeneral.IAnyObject | undefined => {
  return undefined;
};
const defaultHeaders = (): TaroGeneral.IAnyObject => {
  return {
    apiver: ENV_API_VERSION,
  };
};

export default {
  md5Params,
  defaultParams,
  defaultHeaders,
};
