import { getToken, getTokenInfo, setTokenInfo } from "../utils";
import Taro from "@tarojs/taro";
import { httpClient } from "./index";
import { useAppStore } from "../stores";
/**
 * 简单复制对象
 * 从resource->target
 */
export const copyObject = <T extends Record<string, string>>(
  target: T | undefined,
  resource: T | undefined
): T => {
  if (typeof target === "undefined") {
    return resource ?? ({} as T);
  }
  if (typeof resource === "undefined" || resource === null) {
    return target;
  }
  // eslint-disable-next-line no-unused-vars
  for (const key in resource) {
    if (resource.hasOwnProperty(key)) {
      if (resource[key] === null || resource[key] === "undefined") continue;
      target[key] = resource[key];
    }
  }
  return target;
};

const Toast = {
  fail: (title) => {
    $TzNotify.danger(title + "");
  },
};

/**
 *刷新token
 * @returns promise
 */
export const refreshTokenInfo = () => {
  const { refreshToken } = getTokenInfo();
  if (!refreshToken) return;
  //不要带token 就把token设置成none
  const option = {
    url: "/uc/account/refreshToken",
    method: "POST",
    header: { Authorization: "none" },
    data: { refreshToken },
  } as Taro.request.Option;

  return httpClient<{ data: ITokenInfo }>(option)
    .then((res) => {
      useAppStore().setToken(res.data.accessToken);
      return setTokenInfo(res.data);
    })
    .catch(() => {
      Taro.reLaunch({
        url: "/pages/login/index",
      });
      Taro.showToast({
        title: "账号过期，请重新登录！",
        icon: "none",
        duration: 2000,
      });
      return Promise.reject();
    });
};

let locked = false;
/**
 * 处理登录验证过期
 */
const handleUnauthorized = async () => {
  const { refreshToken, now } = getTokenInfo();
  const isExpire =
    28 * 12 * 3600 - parseInt(((Date.now() - now) / 1000).toString()) <= 0;
  const token = getToken();

  // 如果有refreshToken并且没有过期,并且有token
  if (refreshToken && !isExpire && token) {
    //刷新token
    if (locked) return;
    locked = true;
    await refreshTokenInfo();
    locked = false;

    //刷新成功后加载页面
    const url = Taro.getCurrentInstance().router?.path.split("?")[0] || "";
    Taro.redirectTo({ url }).catch((err) => {
      //如果是tabbar页面,就用reLaunch
      if (err.errMsg?.endsWith("a tabbar page")) {
        Taro.reLaunch({ url });
      }
    });
  }

  //如果没有token,代表第一次登录
  //显示游客无登录状态首页
  if (!token) return;

  //如果没有refreshToken,直接跳转到登录页
  Taro.reLaunch({
    url: `/pages/login/index`,
  });
  Taro.showToast({
    title: "账号过期，请重新登录！",
    icon: "none",
    duration: 2000,
  });
};

/**
 *网络错误状态处理
 */
export const errHandler = (error) => {
  const status = error.statusCode || error.status;
  switch (status) {
    case 401:
      // token失效到登录页
      handleUnauthorized();
      //Toast.fail(error.data?.msg || error.message);
      break;
    case 403:
      Toast.fail(error.message || "403异常");
      break;
    case 404:
      Toast.fail(error.message || "404异常");
      break;
    case 422:
      Toast.fail("请求参数错误");
      break;
    case 426:
      Toast.fail(error?.data.msg || "请求参数错误");
      break;
    case 428:
      Toast.fail(error?.data.msg || "无效的身份");
      break;
    case 500: {
      const url = error.url || "";
      Toast.fail(url + "-服务器无响应(500)");
      break;
    }

    default:
      Toast.fail(error.msg || status + "_ 未知的情况");
      break;
  }
};
