export interface RespResult<T = any> extends TaroGeneral.CallbackResult {
  data: T;
  /** 开发者服务器返回的 HTTP Response Header */
  header: TaroGeneral.IAnyObject;
  /** 开发者服务器返回的 HTTP 状态码 */
  statusCode: number;
  /** 调用结果 */
  errMsg: string;
  /** cookies */
  cookies?: string[];
  statusText: string;
  status: number;
  url: string;

  json: () => Record<string, any>;
}
