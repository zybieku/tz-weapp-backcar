import {
  apiGetUserinfo,
  apiLogin,
  apiRecordOpenId,
  apiWxFastLogin,
} from "../api/userApi";
import { useAppStore } from "../stores";
import type { MpEvent } from "@tarojs/runtime";
import Taro from "@tarojs/taro";
import { EnumUserOrigin, setTokenInfo } from "@tz-mall/shared";

export interface IUserFrom extends Partial<Record<string, string | number>> {
  phone?: string;
  pwd?: string;
  loginCode: string;
  password: string;
  userType: number;
}
export interface ILoginState {
  showLoading: boolean;
  form: IUserFrom;
}

export const useLogin = (state: ILoginState) => {
  let wxLoginCode = "";
  Taro.login().then((res) => {
    wxLoginCode = res.code;
  });

  const appStore = useAppStore();

  const login = async () => {
    return await apiLogin({
      userOrigin: EnumUserOrigin.WEAPP,
      userType: state.form.userType,
      loginCode: state.form.loginCode.trim(),
      password: state.form.password.trim(),
    })
      .then((res) => {
        setTokenInfo(res.data);
        return appStore.setToken(res.data.accessToken);
      })
      .then(() => {
        if (Taro.getEnv() !== Taro.ENV_TYPE.WEB) {
          // 发送 res.code 到后台换取 openId
          apiRecordOpenId({
            tmpCode: wxLoginCode,
            userType: state.form.userType,
          });
        }
        return initBaseInfo();
      })
      .then(() => {
        $TzNotify.success("登录成功");
        Taro.reLaunch({
          url: ENV_HOME_URL,
        });
      })
      .catch((err) => {
        console.log(err);
        $TzNotify.danger(err.data?.msg || "登录失败");
        return false;
      });
  };

  // 微信一键登录
  const wxLogin = (e: MpEvent) => {
    if (state.showLoading) return;
    state.showLoading = true;

    Taro.checkSession()
      .then(() => {
        const { iv, encryptedData } = e.detail;
        if (!iv || !encryptedData) return Promise.reject();
        return apiWxFastLogin({
          userType: state.form.userType,
          tmpCode: wxLoginCode,
          encryptData: encryptedData,
          iv,
        });
      })
      .then((res) => {
        if (res.data?.accessToken) {
          setTokenInfo(res.data);
          return appStore.setToken(res.data.accessToken);
        }
      })
      .then(() => {
        return initBaseInfo();
      })
      .then(() => {
        state.showLoading = false;
        $TzNotify.success("登录成功");
        Taro.reLaunch({
          url: ENV_HOME_URL,
        });
      })
      .catch((err) => {
        state.showLoading = false;
        console.log(err);
        $TzNotify.danger(err?.message || err?.data?.msg);
        Taro.login().then((res) => {
          wxLoginCode = res.code;
        });
      });
  };

  //获取基础资料和用户信息
  const initBaseInfo = async () => {
    return apiGetUserinfo().then((res) => {
      const userInfo = res?.data || {};
      return setStoreData(userInfo);
    });
  };

  //TODO 设置本地数据
  const setStoreData = (userInfo: IUserInfo) => {
    const { phone } = userInfo;
    userInfo.account = phone;
    return appStore.setUseInfo(userInfo);
  };
  return { wxLogin, login };
};
