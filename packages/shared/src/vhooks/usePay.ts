import Taro from "@tarojs/taro";
import { apiPay, apiUpdatePayStatus } from "../api/basicApi";
interface payOrderParams {
  [propName: string]: string;
}
/**
 * 统一支付接口
 * @returns
 */
export const usePay = () => {
  
  //支付接口
  const payOrder = async (apiParams: payOrderParams = {}) => {
    Taro.showLoading({ title: "加载中..." });

    const prePayRes = await Taro.login()
      .then((loginRes) => {
        const params = {
          code: loginRes.code,
          ...apiParams,
        };
        //后台充值接口
        return apiPay(params);
      })
      .catch((err) => {
        Taro.hideLoading();
        $TzNotify.danger(err.data?.msg);
        return false;
      });
    Taro.hideLoading();

    if (typeof prePayRes === "boolean") return Promise.reject();
    const { nonceStr, prepayId, sign, timestamp, payOrderNo } =
      prePayRes.data || {};
    return new Promise((resolve, reject) => {
      Taro.requestPayment({
        nonceStr,
        package: "prepay_id=" + prepayId,
        paySign: sign,
        timeStamp: timestamp,
        signType: "MD5",
        success() {
          console.log(prePayRes);

          resolve(prePayRes);
        },
        fail(err) {
          reject(err);
          apiUpdatePayStatus({
            payOrderNo,
            payResultFlag: false,
          });
          Taro.showToast({
            title: err.errMsg.indexOf("cancel") ? "取消支付" : err.errMsg,
            icon: "none",
            duration: 1000,
          });
        },
      });
    });
  };
  return { payOrder };
};
