/**
 * tz路由统一跳转
 */
import Taro, { useRouter } from "@tarojs/taro";
import { onBeforeUnmount } from "vue";

export const useTzRouter = () => {
  //切面pages
  let pages = Taro.getCurrentPages();
  //当前实例子
  const { params, path } = useRouter();
  const cPath = path.split("?")[0].substring(1);

  /**
   * 跳转到 tabBar 页面，并关闭其他所有非 tabBar 页面
   * @param {Object} option
   */
  const switchTab = (option: INavOption) => {
    const { path: url, complete, fail, success } = option;
    return Taro.switchTab({ url, complete, fail, success });
  };

  /**
   * 跳转到新的页面
   * @param {Object} option {path,query:{}}
   */
  const navigateTo = (option) => {
    const { path, query } = option;
    const args = objToUrl(query);
    if (!args) return Taro.navigateTo({ url: path });
    if (path.includes("?")) {
      return Taro.navigateTo({ url: path + "&" + args });
    }
    return Taro.navigateTo({ url: path + "?" + args });
  };

  /**
   * 关闭当前页面，返回上一页面或多级页面
   * @param {Object} option {delta: 1}
   */
  const navigateBack = (option = {} as Partial<INavOption>) => {
    const { delta = 1, event } = option || {};
    const at = pages.length - delta - 1;
    if (at < 0) {
      option.path = option.path || "/pages/home/index";
      return reLaunch(option as INavOption);
    }
    const prePage = pages[at];
    if (event) {
      const path = prePage.route || prePage.path.split("?")[0];
      Taro.eventCenter.trigger(path, event);
    }
    return Taro.navigateBack(option);
  };

  /**
   * 关闭所有页面，打开到应用内的某个页面
   * @param {Object} option
   */
  const reLaunch = (option: INavOption) => {
    const { path, query } = option;
    const args = objToUrl(query);
    if (!args) return Taro.reLaunch({ url: path });
    if (path.includes("?")) {
      return Taro.reLaunch({ url: path + "&" + args });
    }
    return Taro.reLaunch({ url: path + "?" + args });
  };

  /**
   * 关闭当前页面，跳转到应用内的某个页面。但是不允许跳转到 tabbar 页面。
   * @param {Object} options
   */
  const redirectTo = (option: INavOption) => {
    const { path, query } = option;
    const args = objToUrl(query);
    if (!args) return Taro.redirectTo({ url: path });
    if (path.includes("?")) {
      return Taro.redirectTo({ url: path + "&" + args });
    }
    return Taro.redirectTo({ url: path + "?" + args });
  };

  /**
   * 全局事件订阅
   */
  const EventChannel = {
    on: (eventName: string, fun) => {
      Taro.eventCenter.on(eventName, fun);
    },
    emit: (eventName: string, args) => {
      Taro.eventCenter.trigger(eventName, args);
    },
    off: (eventName: string, fun) => {
      Taro.eventCenter.off(eventName, fun);
    },
  };

  onBeforeUnmount(() => {
    Taro.eventCenter.off(cPath);
    pages = [];
  });

  return {
    navigateBack,
    switchTab,
    navigateTo,
    reLaunch,
    redirectTo,
    EventChannel,
    route: { path: cPath, query: decodeObj(params) },
  };
};

/**
 * 对象类型参数 转url
 * @param {} query
 * @returns
 */
const objToUrl = (query?: Record<string, string>) => {
  if (!query) return "";
  //对象转成url 参数，只支持一层对象
  let args = "";
  // eslint-disable-next-line no-unused-vars
  for (const key in query) {
    if (query.hasOwnProperty(key)) {
      let value = query[key];
      if (value === "undefined") continue;
      if (value === null) value = ""; //处理null会被转换null字符串
      value = value ? encodeURIComponent(value) : value;
      args += `&${key}=${value}`;
    }
  }
  return args.slice(1);
};

/**
 * 对象特殊字符 decode
 * @param {} query
 * @returns
 */
const decodeObj = (
  query: Partial<Record<string, string>>
): Record<string, string> => {
  //对象转成url 参数，只支持一层对象
  const obj = {};
  // eslint-disable-next-line no-unused-vars
  for (const key in query) {
    if (query.hasOwnProperty(key)) {
      if (key === "stamp" || key === "$taroTimestamp") continue;
      const value = query[key];
      obj[key] = decodeURIComponent(value || "");
    }
  }
  return obj;
};
