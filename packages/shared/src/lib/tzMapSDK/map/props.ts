import type { MapProps } from "@tarojs/components";
import type { PropType } from "vue";

export const props = {
  id: {
    type: String,
    default: "tz-map",
  },
  height: {
    type: Number,
  },
  center: {
    type: Object as PropType<MapProps.point>,
    default: () => ({ latitude: 22.549662, longitude: 114.070247 }),
  },
  scale: {
    type: Number,
    default: 16,
  },
  showLocation: {
    type: Boolean,
    default: true,
  },

  markers: {
    type: Array as PropType<MapProps.marker[]>,
    default: () => [],
  },
};
