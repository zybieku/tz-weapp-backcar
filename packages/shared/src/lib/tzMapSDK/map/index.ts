import TzMap from "./Map.vue";
import type { MapContext } from "@tarojs/taro";

type TzMapIntance = InstanceType<typeof TzMap> & {
  /** 添加 marker。
   */
  addMarkers(
    option: MapContext.AddMarkersOption
  ): Promise<TaroGeneral.CallbackResult>;

  /** 移除 marker。
   */
  removeMarkers(
    option: MapContext.RemoveMarkersOption
  ): Promise<TaroGeneral.CallbackResult>;
};
export { TzMap };
export type { TzMapIntance };
