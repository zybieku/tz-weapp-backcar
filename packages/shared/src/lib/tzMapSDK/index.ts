import Taro from "@tarojs/taro";
import QQMapWX from "./QQMapWX";
import api from "../../tzAxios";
import { TzMap, TzMapIntance } from "./map";
import TzLocation from "./location";

/**
 * 腾讯地图选择位置
 * wx.chooseLocation 的封装
 * @param option
 * @returns
 */
export const TzChooseLocation = async (
  option: Taro.chooseLocation.Option = {}
) => {
  const res = await Taro.chooseLocation(option);
  if (res.errMsg !== "chooseLocation:ok") {
    return Promise.reject(res);
  }
  return QQMapWX.instance
    .reverseGeocoder({
      location: {
        latitude: res.latitude.toString(),
        longitude: res.longitude.toString(),
      },
    })
    .then((resp) => {
      const { address_reference: refer, address_component: component } =
        resp.result;
      return api.post({
        url: "/basicdata/base/province/getByTownNameAndOther",
        data: {
          townName: refer.town.title,
          otherName: component.district || component.city || component.province,
        },
      });
    })
    .then(({ data = {} }) => {
      const { code, name: label } = data;
      return {
        ...res,
        code,
        label,
      };
    })
    .catch((err) => {
      console.log(err);
      return Promise.reject(err);
    });
};

export type { TzMapIntance };

export { QQMapWX, TzMap, TzLocation };
