import { getUserInfo } from "@tz-mall/shared";
import {
  useShareAppMessage,
  useShareTimeline,
  getLaunchOptionsSync,
  getCurrentPages,
} from "@tarojs/taro";

export const useShareMsg = () => {
  const { account } = getUserInfo() || {};
  const scene = account ? `?scene=${account}` : "";
  useShareAppMessage(() => {
    return {
      title: "粤港回头车司机端",
      path: ENV_HOME_URL + scene,
      //  imageUrl: imgShareMsg,
    };
  });

  useShareTimeline(() => {
    return {
      title: "粤港回头车司机端~~",
      query: account ? `scene=${account}` : "",
      //  imageUrl: icShareLine,
    };
  });
};

export const useManualShareMsg = () => {
  // const { params, path } = useRouter() || {};
  // const query = params.id ? `id=${params.id}` : "";

  let shareInfo = {
    title: "10000+中港司机喊你加入群聊~",
    query: "",
  };

  const setShareInfo = (info) => {
    shareInfo = info;
  };

  useShareAppMessage(() => {
    return {
      title: shareInfo.title,
      path: "/pages/home/index",
      // imageUrl: imgManualShareMsg,
    };
  });

  useShareTimeline(() => {
    return {
      title: shareInfo.title,
      query: shareInfo.query,
      // imageUrl: icManualShareLine,
    };
  });
  const { scene } = getLaunchOptionsSync();
  return {
    isSingeMode: scene === 1154,
    isSingeModeOpen: () => {
      const pages = getCurrentPages().length;
      return scene === 1155 && pages === 1;
    },
    setShareInfo,
    renderEmpty: () => (
      <div
        style={{
          width: "100%",
          height: "100vh",
          display: "flex",
          flexDirection: "column",
          alignItem: "center",
          justifyContent: "center",
          background: "#ff5800",
          position: "relative",
        }}
      >
        {/* <tz-image style={{ width: "100%" }} src={bgSingleModelMsg}></tz-image> */}
      </div>
    ),
  };
};
