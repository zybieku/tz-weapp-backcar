/**
 * 图片统一管理，方便全局替换
 */
export { default as icLogo } from "@/assets/images/ic_logo.png";
export { default as icTitleLogo } from "@/assets/images/ic_title_logo.png";

//radio
export { default as icRadio } from "@/assets/images/ic_radio.png";
export { default as icRadioActive } from "@/assets/images/ic_radio_active.png";

//粤港车图标
export { default as icTransXie } from "@/assets/images/ic_trans_xie.png";
export { default as icTransZhuang } from "@/assets/images/ic_trans_zhuang.png";
export { default as icCustomsZjsb } from "@/assets/images/ic_customs_zjsb.png";

export { default as icUserActive } from "@/assets/images/ic_user_active.png";

const baseUrl = "https://minio.kuajingzhilian.com/wx-applet";

//用户协议
const htmlVer = (Date.now() / (1000 * 60 * 10)).toFixed(0);
export const userAgreementUrl = `${baseUrl}/html/driver/register.html?${htmlVer}`;
export const userPrivateUrl = `${baseUrl}/html/driver/user.html?${htmlVer}`;
export const userAboutUrl = `${baseUrl}/html/driver/about.html?${htmlVer}`;

//首页banner
export const getHomeBannerArr = () => {
  const _baseUrl = baseUrl + "/car_20230104/img_home_banner";
  const arr = [".png"];
  //5分钟一次缓存
  const version = (Date.now() / (1000 * 60 * 30)).toFixed(0);
  return arr.map((url) => {
    return `${_baseUrl + url}?${version}`;
  });
};
export { default as icHomeHot } from "@/assets/images/ic_home_hot.png";
export const imgHomeEmpty = baseUrl + "/car_20230104/img_home_empty.png";

//登录
export const bgLogin = baseUrl + "/car_20230104/bg_login.jpg";

//我的
export const bgMineHeader = baseUrl + "/car_20230104/bg_mine_header.png";

// 地址簿
export { default as imgAddress } from "@/assets/images/img_address.png";
export { default as imgCommonLines } from "@/assets/images/img_common_lines.png";

// 订单
export const imgOrderNodata = `${baseUrl}/car_20230104/img_order_nodata.png`;
export const imgOrderUnlogin = `${baseUrl}/car_20230104/img_order_unlogin.png`;
export const imgOrderSuccess = `${baseUrl}/car_20230104/img_order_success.png`;
export const imgPaySuccess = `${baseUrl}/car_20230104/img_pay_success.png`;

//司机认证
export const imgIdCard0 = baseUrl + "/20230413/img_id_card0.png";
export const imgIdCard1 = baseUrl + "/20230413/img_id_card1.png";
export const imgDrivingCard = baseUrl + "/20230413/img_driving_card.png";
export const imgTravelCard = baseUrl + "/20230413/img_travel_card.png";
export const imgTransportCard = baseUrl + "/20230413/img_transport_card.png";
