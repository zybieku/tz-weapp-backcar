export default {
  pages: ["pages/order/index", "pages/mine/index", "pages/login/index"],
  subpackages: [
    {
      //sub 用户中心自包
      root: "pages/subUser",
      pages: ["register/index", "findPwd/index", "webPage/index"],
    },
    {
      //sub 订单子包
      root: "pages/subOrder",
      pages: [
        "applyDriver/index",
        "locusAttaImg/index",
        "uploadPacking/index",
        "uploadReceipt/index",
        "fee/home/index",
        "fee/edit/index",
        "locusDetail/index",
      ],
    },
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#fff",
    navigationBarTitleText: "粤港回头车司机端",
    navigationBarTextStyle: "black",
    onReachBottomDistance: 50,
  },
  permission: {
    "scope.userLocation": {
      desc: "订单定位需要获取您的位置信息(使用时和离开后)",
    },
  },
  requiredPrivateInfos: [
    "startLocationUpdateBackground",
    "getLocation",
    "startLocationUpdate",
    "onLocationChange",
    "chooseLocation",
  ],
  requiredBackgroundModes: ["location", ""],
  tabBar: {
    color: "#626567",
    selectedColor: "#1c78ef",
    backgroundColor: "#FBFBFB",
    borderStyle: "white",
    position: "bottom",
    list: [
      {
        pagePath: "pages/order/index",
        text: "订单",
        selectedIconPath: "assets/images/ic_order_active.png",
        iconPath: "assets/images/ic_order.png",
      },
      {
        pagePath: "pages/mine/index",
        text: "我的",
        selectedIconPath: "assets/images/ic_user_active.png",
        iconPath: "assets/images/ic_user.png",
      },
    ],
  },
};
