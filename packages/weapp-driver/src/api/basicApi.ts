import { api } from "@tz-mall/shared";
import type { IUploadResp } from "./types";
/**
 * 支付接口
 * @param data
 * @returns
 */
export const payApi = (data) => {
  const option = {
    url: "/transport/pay/feeSubmit",
    data,
  };
  return api.post(option);
};
/**
 * 更新支付结果
 * @param data
 * @returns
 */
export const updatePayStatus = (data) => {
  const option = {
    url: "/transport/pay/updatePayStatusByPayOrderNo",
    data,
  };
  return api.post(option);
};
/**
 * 根据代码集合查询产品详情
 * @param data
 * @returns
 */
export const listProductDetailByCode = (data) => {
  const option = {
    url: "/transport/base/productDetail/listProductDetailByCode",
    data,
  };
  return api.post(option);
};
/** 
获取粤港车下拉静态数据
所有前端页面静态的下拉数据，全部统一都后台配置后查询
@param []data
[{"type":"PRODUCT_DETAIL TYPE"}]
@returns
PRODUCT_DETAIL TYP:[]
*/
export const apiGetDicMap = (data) => {
  const option = {
    url: "/basicdata/base/dropDown/getDropDownMap",
    data,
  };
  return api.post(option).then((res) => {
    return res.data;
  });
};

/**
 * 上传文件公共方法(可以替换图片)
 * @param data
 * {
 *    filePath: item.src,
 *    formData: { bizId: id, bizType: "rocasr_licence",fileId:string },
 *  }
 */
export const apiUploadReplacePic = (data) => {
  const option = {
    url: "/basicdata/base/attachment/replaceFile",
    ...data,
  };
  return api.upload<IUploadResp>(option);
};

/**
 * 查询单证已上传附件信息
 * @param data {
      bizId: string, //业务单证id
      bizTypes:[trackType],
    };
 */
export const apiListMultipleType = async (data: {
  bizTypes: string[];
  bizId: string;
}) => {
  const option = {
    url: "/basicdata/base/attachment/listMultipleType",
    data,
  };
  const jsonData = data.bizTypes.reduce((prev, item) => {
    prev[item] = [];
    return prev;
  }, {});
  return await api.post(option).then((res) => {
    return (res.data || []).reduce((prev, item) => {
      prev[item.bizType].push({
        url: item.remoteFileName,
        fileUrl: item.remoteFileName,
        name: item.fileName,
        id: item.id,
        fileType: item.fileType,
      });
      return prev;
    }, jsonData) as Record<
      string,
      {
        url: string;
        name: string;
        id: string;
        fileUrl: string;
        fileType: string;
      }[]
    >;
  });
};
