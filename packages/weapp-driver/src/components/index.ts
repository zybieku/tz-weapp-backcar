import tzUiTaro from "@tz-mall/weapp-ui";
import TzEmpty from "./empty";
import type { App } from "vue";

export const useTzUi = (app: App) => {
  app.use(tzUiTaro);
  app.component("TzEmpty", TzEmpty);
};
