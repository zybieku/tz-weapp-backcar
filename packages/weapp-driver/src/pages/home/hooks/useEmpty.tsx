import { imgHomeEmpty } from "@/assets";
import Taro from "@tarojs/taro";
import { reactive } from "vue";
import type { ILineOffer } from "../types";

/**
 * 无数据
 */
export const useEmpty = (listState: { list: ILineOffer[] }) => {
  const eState = reactive({
    phone: "",
    servicePhoneNum: "400-020-8788",
  });

  //拨打客服电话
  const handleCall = () => {
    Taro.makePhoneCall({
      phoneNumber: eState.servicePhoneNum,
    }).catch((err) => {
      console.warn(err);
    });
  };

  return () => (
    <div class="empty" v-show={!listState.list.length}>
      <tz-image src={imgHomeEmpty} class="img-place"></tz-image>
      <div class="desc">
        <div class="desc-top">
          <span>未有匹配路线，请联系客服</span>
          <span class="phone" onClick={handleCall}>
            {eState.servicePhoneNum}
          </span>
        </div>
        {/* <span>或留下您的联系方式</span>
        <tz-input
          v-model={eState.phone}
          border
          class="phone-input"
          placeholder="请输入您的联系方式"
        ></tz-input>
        <tz-button plain type="primary" size="small" class="btn-submit">
          提交
        </tz-button> */}
      </div>
    </div>
  );
};
