import { getHomeBannerArr } from "@/assets";

interface ISwiper {
  path: string;
  url: string;
}

export const useBanner = () => {
  const routes = [
    {
      path: "/pages/subUser/webPage/index",
      query: {
        url: `https://mp.weixin.qq.com/s/Cv7U09MqodyBPGRjO1i7zw`,
        title: "新人指引",
      },
    },
    {
      path: "/pages/subActiv/newUsrWelfare/index",
    },
    {
      path: "/pages/subActiv/inviteWelfare/index",
    },
  ];

  const banners = getHomeBannerArr().map((url, index) => {
    const route = routes?.[index] || {};
    return { url, ...route } as ISwiper;
  });

  return () => (
    <tz-swiper
      class="section-swiper"
      indicatorDots={false}
      items={banners}
    ></tz-swiper>
  );
};
