export interface ITransReq {
  consignorTown: string;
  consigneeAddr: string;
  consigneeTown: string;
  consignorAddr: string;
  specialOfferFlag: boolean;
}

export interface ICity {
  code: string;
  name: string;
  list: ICity[];
}

export interface HomeState {
  packId: string;
  dischargeId: string;
  comLinesId: string;
}

export interface IProvince extends IBaseResp {
  data: ICity[];
}

export interface ILineOffer {
  lineId: string;
  lineName: string;
  truckTonnages: string;
  consignorAddrCode: string;
  consignorAddr: string;
  consignorTownCode: string;
  consignorTown: string;
  consigneeAddrCode: string;
  consigneeAddr: string;
  consigneeTownCode: string;
  consigneeTown: string;
  validityStart: string;
  validityEnd: string;
  sellingPrice: number;
  specialOfferFlag: boolean;
  truckBoatload: string;
  truckVolume: number;
  truckLength: number;
  truckWidth: number;
  truckHeight: number;
  lineLabelName: string;
}
export interface ILineOfferResp extends IBaseResp {
  data: ILineOffer[];
}
