import { api } from "@tz-mall/shared";
import type { ILineOfferResp, IProvince, ITransReq } from "./types";

/**
 * 获取用户信息
 */

/**
 * 查询中华人民共和国行政区划
 * @returns
 */
export const apiListProvince = () => {
  const option = {
    url: "/basicdata/base/province/listProvince",
  };
  return api.post(option);
};

/**
 * 查询粤港行政区划
 * @returns
 */
export const apiListGuangdongHongKong = () => {
  const option = {
    url: "/basicdata/base/province/listGuangdongHongKong",
  };
  return api.post<IProvince>(option);
};

/**
 * 运输路线报价查询
 * @returns
 */
export const apiAppletLineOfferList = (data: ITransReq) => {
  const option = {
    url: "/transport/line/offer/getAppletLineOfferList",
    data,
  };
  return api.post<ILineOfferResp>(option);
};

/**
 * 根据id查询常用路线明细
 * @returns
 */
export const saveComLines = (data) => {
  const option = {
    url: "/transport/line/oftenLine/save",
    data,
  };
  return api.post(option);
};

//获取详情
export const getDetail = (data) => {
  const option = {
    url: `/transport/line/offer/getAppletLineOfferById`,
    data,
  };
  return api.post(option);
};
