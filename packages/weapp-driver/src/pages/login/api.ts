import { api } from "@tz-mall/shared";

export const login = (data) => {
  const option = {
    url: "/wx/tp/login",
    data,
  };
  return api.post(option);
};
