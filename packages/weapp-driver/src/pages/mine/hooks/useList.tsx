import { isFunction, useAppStore } from "@tz-mall/shared";
import { userAgreementUrl, userPrivateUrl, userAboutUrl } from "@/assets";
import { useDidShow } from "@tarojs/taro";
import { apiGetApplicationStatus } from "@/pages/subOrder/applyDriver/api";
import { EnumAuditorStatus } from "@/pages/subOrder/constant";
import Taro from "@tarojs/taro";

interface ICell {
  icon: string;
  title: string;
  path: string | (() => string);
  color: string;
  query?: Record<string, string>;
  isLogin?: Boolean;
}

export const useList = (navigateTo) => {
  const app = useAppStore();
  //司机审核状态
  let auditorStatus = 0;

  const handleClickItem = (cell: ICell) => {
    if (cell.isLogin && !app.isLogin) {
      navigateTo({ path: "/pages/login/index" });
      return;
    }
    const path = isFunction(cell.path) ? cell.path() : cell.path;
    navigateTo({
      path: path,
      query: cell.query,
    }).catch(() => {
      Taro.switchTab({ url: path });
    });
  };

  const cells: ICell[] = [
    {
      icon: "zigeshenqing",
      title: "司机资格申请",
      color: "#00DEFF",
      isLogin: true,
      path: () => {
        if (auditorStatus === EnumAuditorStatus.EXIST) {
          Taro.showModal({
            content: "无申请信息，你的回头车司机资格可能已由平台设置",
            confirmText: "确定",
            showCancel: false,
          });
          return "";
        }
        return auditorStatus !== EnumAuditorStatus.OFF_STOCKS
          ? "/pages/order/index"
          : `/pages/subOrder/applyDriver/index?status=${auditorStatus}`;
      },
    },
    {
      icon: "guanyuwomen",
      title: "关于我们",
      color: "#F95E5A",
      path: "/pages/subUser/webPage/index",
      query: {
        url: userAboutUrl,
        title: "关于我们",
      },
    },
    {
      icon: "lianxiwomen",
      title: "联系我们",
      color: "#1C7BEF",
      path: "/pages/subUser/webPage/index",
      query: {
        url: "https://mp.weixin.qq.com/s/5AYMkv4nqruhCuFjyVG4Bg",
        title: "联系我们",
      },
    },
    {
      icon: "caozuozhinan",
      title: "操作指南",
      color: "#F96460",
      path: "/pages/subUser/webPage/index",
      query: {
        url: "https://mp.weixin.qq.com/s/0TxY7H_d5r3XJxcpQB--qg",
        title: "操作指南",
      },
    },
    {
      icon: "yonghuxieyi",
      title: "用户协议",
      color: "#FF7A03",
      path: "/pages/subUser/webPage/index",
      query: {
        url: userAgreementUrl,
        title: "用户协议",
      },
    },
    {
      icon: "yinsixieyi",
      title: "隐私协议",
      color: "#00DEFF",
      path: "/pages/subUser/webPage/index",
      query: {
        url: userPrivateUrl,
        title: "隐私协议",
      },
    },
  ];

  const handleLogout = () => {
    Taro.reLaunch({
      url: "/pages/login/index",
    });
  };
  //获取当前司机的审核状态
  const getAuditorStatus = () => {
    apiGetApplicationStatus().then((res) => {
      auditorStatus = res.data.status;
    });
  };

  useDidShow(() => {
    if (!app.isLogin) return;
    getAuditorStatus();
  });
  return () => (
    <div class="list">
      {cells.map((cell, index) => (
        <tz-cell
          key={index}
          onClick={(e: MouseEvent) => {
            e.stopPropagation();
            handleClickItem(cell);
          }}
          border
        >
          <div class="label">
            <tz-icon name={cell.icon} style={{ color: cell.color }}></tz-icon>
            <div class="title">{cell.title}</div>
          </div>
          <tz-icon name="arrow-right" class="arrow-right"></tz-icon>
        </tz-cell>
      ))}
      <tz-button v-show={app.isLogin} class="logout" onClick={handleLogout}>
        退出登录
      </tz-button>
    </div>
  );
};
