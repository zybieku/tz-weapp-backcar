export const EnumTrackCode = {
  /**
   * 10:下单
   */
  XD: 10,

  /**
   *20: 接单
   */
  JD: 20,

  /**
   *30: 取货发车
   */
  QHFC: 30,

  /**
   *40: 抵达装货地
   */
  DDZHD: 40,

  /**
   *50: 上传装货单
   */
  SCZHD: 50,

  /**
   *60: 送货发车
   */
  SHFC: 60,

  /**
   *70: 到达海关
   */
  DDHG: 70,

  /**
   *80: 已过境
   */
  YGJ: 80,

  /**
   * 90: 抵达卸货地
   */
  DDXHD: 90,

  /**
   * 99:上传签收单
   */
  SCQSD: 99,
};

/**
 * 司机审核状态
 */
export const EnumAuditorStatus = {
  /**
   * 0:不存在
   */
  NONEXISTENT: 0,
  /**
   * 1:已经存在
   */
  EXIST: 1,
  /**
   * 120:审核不通过
   */
  UNDERWAY_REJECT: 120,
  /**
   * 200:"待审核"
   */
  UNDERWAY_UNDERREVIEW: 200,
  /**
   * 300,"审核通过"
   */
  OFF_STOCKS: 300,
} as const;

/**
 * 获取粤港车基础数据枚举
 */
export const EnumDropDownType = {
  /**
   * '':Fill默认枚举
   */
  EMPTY: "",
  /**
   * ( code: "TRUCK_TYPE"，name:"车型")
   */
  TRUCK_TYPE: "TRUCK_TYPE",
  /**
   * ( code:"PRODUCT.产品详情”)
   */
  PRODUCT_DETAIL: "PRODUCT_DETAIL",
  /**
   * ( code:"LABEL_LIBRARY"，name:"报价标签")
   */
  LABEL_LIBRARY: "LABEL_LIBRARY",
  /**
   * ( code:"PRODUCT_DETAIL_TYPE"，name:"产品详情类型")
   */
  PRODUCT_DETAIL_TYPE: "PRODUCT_DETAIL_TYPE",
  /**
  ( code:"TRUCK_INFO"，name:“车辆信息")
   */
  TRUCK_INFO: "TRUCK_INFO",
  /**
   * ( code:“DRIVER_INFO”，name:“司机信息")
   */
  DRIVER_INFO: "DRIVER_INFO",
  /**
   * ( code:"BASE_CURRENCY"，name:"市种")
   */
  CURRENCY: "BASE_CURRENCY",
  /**
   * ( code: "BASE_CURRENCY_EXCHANGE_RATE"，name:汇率)
   */
  EXCHANGE_RATE: "BASE_CURRENCY_EXCHANGE_RATE",
  /**
   * ( code: "CERTIFICATE_TYPE"，name:证件类型)
   */
  CERTIFICATE_TYPE: "CERTIFICATE_TYPE",
};

/**
 * 司机审核 文件的Biz的类型
 */
export const EnumBizType = {
  /**
   * 身份证头像面
   */
  DRIVER_ID_A: "driver_id_a",
  /**
   * 身份证国徽面
   */
  DRIVER_ID_B: "driver_id_b",
  /**
   * 行驶证信息
   */
  DRIVER_TRAVEL: "driver_travel",
  /**
   * 驾驶证信息
   */
  DRIVER_DRIVING: "driver_driving",
  /**
   * 道路运输证
   */
  DRIVER_ROAD_TRANSPORT: "driver_road_transport",
} as const;

/**
 * 费用文件的Biz的类型
 */
export const EnumFeeBizType = {
  /**
   * 费用上传
   */
  TRANSPORT_ORDER_FEE: "TRANSPORT_ORDER_FEE",
} as const;
