import { reactive, ref } from "vue";
import Taro from "@tarojs/taro";
import { uploadPic } from "./api";

export const useUpload = (state) => {
  const upState = reactive({
    pics: [] as string[],
  });

  const handleChooseImage = async () => {
    const { tempFilePaths } = await Taro.chooseImage({
      sizeType: ["compressed"],
      sourceType: ["camera"],
    }).catch((err) => {
      console.log(err);
      return { tempFilePaths: [] as string[] };
    });

    if (tempFilePaths.length) {
      upState.pics.push(tempFilePaths[0]);
    }
  };

  //批量上传图片
  const uploadImgs = async () => {
    if (!upState.pics.length) return [];
    console.log(state.formData.id);

    return await Promise.all(
      upState.pics.map((filePath) =>
        uploadPic({
          filePath,
          formData: { bizId: state.formData.id, bizType: "shipping_order" },
        })
      )
    )
      .then((arr) => arr.map((res) => res.data))
      .catch((err) => {
        console.log(err);

        $TzNotify.danger("上传图片错误,请重试");
        return [];
      });
  };

  return {
    uploadImgs,
    render: () => (
      <div class="item">
        <div class="label required">装货单(最多上传3个)</div>
        <div class="tags">
          {upState.pics.map((src) => (
            <tz-image
              class="upload"
              src={src}
              mode="scaleToFill"
              onClick={() =>
                Taro.previewImage({
                  current: src,
                  urls: upState.pics,
                })
              }
            ></tz-image>
          ))}
          {upState.pics.length < 3 && (
            <div class="upload" onClick={handleChooseImage}>
              <div id="cross"></div>
            </div>
          )}
        </div>
      </div>
    ),
  };
};
