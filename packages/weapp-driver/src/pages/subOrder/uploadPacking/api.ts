import { api } from "@tz-mall/shared";

// 上传图片
export const uploadPic = (data) => {
  const option = {
    url: "/basicdata/base/attachment/newUpload",
    ...data,
  };
  return api.upload(option);
};
