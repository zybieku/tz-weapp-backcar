import { ICell } from "@/components/tzUI/TzCellForm/cellForm";
import type { IApplyDriverState } from "../type";
import { computed, reactive, ref } from "vue";

import { useUpload } from "./useUpload";
import { EnumBizType } from "../../constant";
import { geCurrDate } from "@tz-mall/shared";

export const useCertificate = (state: IApplyDriverState) => {
  //上传图片
  const { render: renderUpload, uploadImgs } = useUpload(state);
  //申请人证件
  const applicantCertHook = useApplicantCert(state, renderUpload);
  //行驶证
  const drivingCertHook = useDrivingCert(state, renderUpload);
  //驾驶证
  const driversCertHook = useDriversCert(state, renderUpload);
  //道路运输证
  const roadTransCertHook = useRoadTransCert(state, renderUpload);

  return {
    uploadImgs,
    validate: () => {
      return (
        applicantCertHook.validate() &&
        drivingCertHook.validate() &&
        driversCertHook.validate() &&
        roadTransCertHook.validate()
      );
    },
    render: () => (
      <>
        {applicantCertHook.render()}
        {drivingCertHook.render()}
        {driversCertHook.render()}
        {roadTransCertHook.render()}
      </>
    ),
  };
};

/**
 * 申请人信息
 */
export const useApplicantCert = (state: IApplyDriverState, renderUpload) => {
  const formRef = ref();

  const applicantCells: ICell[] = [
    {
      label: "证件类型",
      field: "certificateType",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () =>
          state.certTypeArrs.length ? (
            <tz-picker
              v-model={state.form.certificateType}
              mode="selector"
              rangeKey="name"
              valueKey="code"
              disabled={state.isRead}
              range={state.certTypeArrs}
              onChange={(_: MouseEvent, item) => {
                state.form.certificateName = item.name;
              }}
            ></tz-picker>
          ) : null,
      },
    },
    {
      label: "证件号码",
      field: "certificateNo",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入证件号码",
          clearable: true,
          disabled: state.isRead,
          rules: (val) => {
            const value = val?.match(/[a-zA-Z0-9\(\)]{0,20}/g)?.[0] || "";
            return value.toUpperCase();
          },
        },
      },
    },
    {
      label: "有效期",
      field: "certificateValidity",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => (
          <tz-picker
            v-model={state.form.certificateValidity}
            mode="date"
            disabled={state.isRead}
            start={geCurrDate()}
          >
            <text>{state.form.certificateValidity || "点击选择日期"}</text>
          </tz-picker>
        ),
      },
    },
    {
      label: "身份证",
      field: "3",
      render: () => (
        <div class="cert-card">
          {renderUpload({
            bizType: EnumBizType.DRIVER_ID_A,
            desc: "上传人像面",
          })}
          {renderUpload({
            bizType: EnumBizType.DRIVER_ID_B,
            desc: "上传国徽面",
          })}
        </div>
      ),
    },
  ];
  return {
    validate: () => {
      const { validate, errMsg } = formRef.value.validate();
      if (!validate) {
        $TzNotify.danger(errMsg);
        return false;
      }
      return true;
    },
    render: () => (
      <div class="section">
        <div class="title">申请人信息</div>
        <tz-cell-form
          ref={formRef}
          cells={applicantCells}
          v-model={state.form}
        ></tz-cell-form>
      </div>
    ),
  };
};

/**
 * 行驶证信息
 */
export const useDrivingCert = (state: IApplyDriverState, renderUpload) => {
  const formRef = ref();

  //行驶证
  const drivingCells: ICell[] = [
    {
      label: "发证日期",
      field: "travelDate",
      attrs: {
        required: true,
        border: true,
        disabled: state.isRead,
      },
      component: {
        render: () => (
          <tz-picker
            v-model={state.form.travelDate}
            mode="date"
            disabled={state.isRead}
            end={geCurrDate()}
          >
            <text>{state.form.travelDate || "点击选择日期"}</text>
          </tz-picker>
        ),
      },
    },
    {
      label: "行驶证",
      field: "2",
      render: () => (
        <div class="cert-card">
          {renderUpload({
            bizType: EnumBizType.DRIVER_TRAVEL,
            desc: "行驶证主页",
          })}
        </div>
      ),
    },
  ];
  return {
    validate: () => {
      const { validate, errMsg } = formRef.value.validate();
      if (!validate) {
        $TzNotify.danger(errMsg);
        return false;
      }
      return true;
    },
    render: () => (
      <div class="section">
        <div class="title">行驶证信息</div>
        <tz-cell-form
          ref={formRef}
          cells={drivingCells}
          v-model={state.form}
        ></tz-cell-form>
      </div>
    ),
  };
};

/**
 * 驾驶证信息
 */
export const useDriversCert = (state: IApplyDriverState, renderUpload) => {
  const formRef = ref();

  //驾驶证
  const driversCells: ICell[] = [
    {
      label: "证件号码",
      field: "drivingNo",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入证件号码",
          clearable: true,
          disabled: state.isRead,
          rules: (val) => {
            const value = val?.match(/[a-zA-Z0-9\(\)]{0,20}/g)?.[0] || "";
            return value.toUpperCase();
          },
        },
      },
    },
    {
      label: "有效期",
      field: "drivingLicence",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => (
          <tz-picker
            v-model={state.form.drivingLicence}
            mode="date"
            disabled={state.isRead}
            start={geCurrDate()}
          >
            <text>{state.form.drivingLicence || "点击选择日期"}</text>
          </tz-picker>
        ),
      },
    },
    {
      label: "驾驶证",
      field: "2",
      render: () => (
        <div class="cert-card">
          {renderUpload({
            bizType: EnumBizType.DRIVER_DRIVING,
            desc: "驾驶证主页",
          })}
        </div>
      ),
    },
  ];
  return {
    validate: () => {
      const { validate, errMsg } = formRef.value.validate();
      if (!validate) {
        $TzNotify.danger(errMsg);
        return false;
      }
      return true;
    },
    render: () => (
      <div class="section">
        <div class="title">驾驶证信息</div>
        <tz-cell-form
          ref={formRef}
          cells={driversCells}
          v-model={state.form}
        ></tz-cell-form>
      </div>
    ),
  };
};

/**
 * 道路运输证
 */
export const useRoadTransCert = (state: IApplyDriverState, renderUpload) => {
  const formRef = ref();

  const roadTransCells: ICell[] = [
    {
      label: "运输证号",
      field: "roadTransportNo",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入证件号码",
          clearable: true,
          disabled: state.isRead,
          rules: (val) => {
            const value = val?.match(/[a-zA-Z0-9\(\)]{0,50}/g)?.[0] || "";
            return value.toUpperCase();
          },
        },
      },
    },
    {
      label: "有效期",
      field: "roadTransport",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => (
          <tz-picker
            v-model={state.form.roadTransport}
            mode="date"
            disabled={state.isRead}
            start={geCurrDate()}
          >
            <text>{state.form.roadTransport || "点击选择日期"}</text>
          </tz-picker>
        ),
      },
    },
    {
      label: "道路运输证",
      field: "2",
      render: () => (
        <div class="cert-card">
          {renderUpload({
            bizType: EnumBizType.DRIVER_ROAD_TRANSPORT,
            desc: "道路运输证",
          })}
        </div>
      ),
    },
  ];
  return {
    validate: () => {
      const { validate, errMsg } = formRef.value.validate();
      if (!validate) {
        $TzNotify.danger(errMsg);
        return false;
      }
      return true;
    },
    render: () => (
      <div class="section">
        <div class="title">道路运输证</div>
        <tz-cell-form
          ref={formRef}
          cells={roadTransCells}
          v-model={state.form}
        ></tz-cell-form>
      </div>
    ),
  };
};
