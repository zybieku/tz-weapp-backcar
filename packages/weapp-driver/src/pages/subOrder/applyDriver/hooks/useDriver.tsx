import type { IApplyDriverState } from "../type";
import { ref } from "vue";
import { useFormReadonly, type ICell } from "@tz-mall/weapp-ui";

export const useDriver = (state: IApplyDriverState) => {
  //电子关锁单选
  const radioList = [
    {
      value: true,
      text: "有",
    },
    {
      value: false,
      text: "无",
    },
  ];
  const formRef = ref();
  const driverCells: ICell[] = [
    {
      label: "司机姓名",
      field: "driverName",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入司机姓名",
          maxlength: 10,
          clearable: true,
        },
      },
    },
    {
      label: "司机卡号",
      field: "driverCardNumber",
      attrs: {
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入司机卡号",
          maxlength: 50,
          clearable: true,
          rules: (val) => {
            const value = val?.match(/[a-zA-Z0-9]{0,18}/g)?.[0] || "";
            return value.toUpperCase();
          },
        },
      },
    },
    {
      label: "大陆电话",
      field: "phone",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入大陆电话",
          maxlength: 11,
          type: "number",
          clearable: true,
        },
      },
    },
    {
      label: "香港电话",
      field: "hkPhone",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入香港电话",
          maxlength: 13,
          type: "number",
          clearable: true,
        },
      },
    },
    {
      label: "电子关锁",
      field: "electronicLock",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => (
          <tz-radio-group
            options={radioList}
            disabled={state.isRead}
            v-model={state.form.electronicLock}
          ></tz-radio-group>
        ),
      },
    },
  ];

  useFormReadonly(driverCells, state.isRead);

  return {
    validate: () => {
      const { validate, errMsg } = formRef.value.validate();
      if (!validate) {
        $TzNotify.danger(errMsg);
        return false;
      }
      return true;
    },
    render: () => (
      <div class="section">
        <div class="title">司机信息</div>
        <tz-cell-form
          ref={formRef}
          cells={driverCells}
          v-model={state.form}
        ></tz-cell-form>
      </div>
    ),
  };
};
