import { apiUploadReplacePic } from "@/api/basicApi";
import {
  imgDrivingCard,
  imgIdCard0,
  imgIdCard1,
  imgTransportCard,
  imgTravelCard,
} from "@/assets";
import Taro from "@tarojs/taro";
import { reactive, watch } from "vue";
import { EnumBizType } from "../../constant";
import type { IApplyDriverState } from "../type";

interface IPic {
  filePath: string;
  bizType: string;
  fileId: string | undefined;
}

/**
 * 上传图片
 */
export const useUpload = (state: IApplyDriverState) => {
  const upState = reactive({
    pics: [] as IPic[],
    bizTypeMap: {
      [EnumBizType.DRIVER_ID_A]: { defaultUrl: imgIdCard0, url: "" },
      [EnumBizType.DRIVER_ID_B]: { defaultUrl: imgIdCard1, url: "" },
      [EnumBizType.DRIVER_TRAVEL]: { defaultUrl: imgTravelCard, url: "" },
      [EnumBizType.DRIVER_DRIVING]: { defaultUrl: imgDrivingCard, url: "" },
      [EnumBizType.DRIVER_ROAD_TRANSPORT]: {
        defaultUrl: imgTransportCard,
        url: "",
      },
    },
  });

  watch(
    () => state.fileListMap,
    () => {
      Object.keys(state.fileListMap).forEach((bizType) => {
        const file = state.fileListMap[bizType][0];
        const { id, url } = file || {};
        Object.assign(upState.bizTypeMap[bizType], { id, url });
      });
    }
  );

  const handleChooseImage = async (bizType: string) => {
    if (state.isRead) return;
    const { tempFilePaths } = await Taro.chooseImage({
      count: 1,
      sizeType: ["compressed"],
      sourceType: ["camera"],
    }).catch((err) => {
      console.log(err);
      return { tempFilePaths: "" };
    });
    if (tempFilePaths) {
      upState.bizTypeMap[bizType].url = tempFilePaths[0];
      const fileId = state.fileListMap[bizType]?.[0]?.id || "";
      const index = upState.pics.findIndex((item) => item.bizType === bizType);
      if (index === -1) {
        upState.pics.push({ filePath: tempFilePaths[0], bizType, fileId });
      } else {
        upState.pics[index].filePath = tempFilePaths[0];
      }
    }
  };

  const handlePreviewImage = (src) => {
    Taro.previewImage({
      current: src,
      urls: upState.pics.map((item) => item.filePath),
    });
  };

  //批量上传图片
  const uploadImgs = async () => {
    if (!upState.pics.length) return [];

    return await Promise.all(
      upState.pics.map((pic) =>
        apiUploadReplacePic({
          filePath: pic.filePath,
          formData: {
            bizId: state.form.id || "",
            bizType: pic.bizType,
            fileId: pic.fileId,
          },
        })
      )
    )
      .then((arr) => arr.map((res) => res.data.id))
      .catch((err) => {
        console.log(err);
        $TzNotify.danger("上传图片错误,请重试");
        return [] as string[];
      });
  };

  return {
    uploadImgs,
    render: (option: { bizType: string; desc: string }) => {
      const item = upState.bizTypeMap[option.bizType];
      return (
        <div class="cert" onClick={() => handleChooseImage(option.bizType)}>
          <tz-image
            class="cert-img"
            mode="aspectFit"
            src={item.url || item.defaultUrl}
          ></tz-image>
          <div class="desc">{option.desc}</div>
        </div>
      );
    },
  };
};
