import { useFormReadonly, type ICell } from "@tz-mall/weapp-ui";
import { ref } from "vue";
import type { IApplyDriverState } from "../type";

export const useCarInfo = (state: IApplyDriverState) => {
  const formRef = ref();
  const carInfoCells: ICell[] = [
    {
      label: "车型",
      field: "truckTypeName",
      attrs: {
        border: true,
        required: true,
      },
      component: {
        render: () => {
          return state.truckTypeArrs.length ? (
            <tz-picker
              v-model={state.form.truckTypeName}
              mode="selector"
              rangeKey="code"
              valueKey="code"
              disabled={state.isRead}
              range={state.truckTypeArrs}
            ></tz-picker>
          ) : null;
        },
      },
    },
    {
      label: "大陆车牌",
      field: "plateNumber",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => (
          <tz-input
            v-model={state.form.plateNumber}
            placeholder="请输入大陆车牌"
            clearable
            disabled={state.isRead}
            rules={(val) => {
              let value = val?.match(/[\u4e00-\u9fa5\w]{0,32}/g)?.[0];
              // 把繁体的粤改成简体
              if (value && value[0] === "粵") {
                value = value.replace("粵", "粤");
              }
              return value.toUpperCase();
            }}
          />
        ),
      },
    },
    {
      label: "香港车牌",
      field: "hkPlateNumber",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => (
          <tz-input
            v-model={state.form.hkPlateNumber}
            placeholder="请输入香港车牌"
            clearable
            disabled={state.isRead}
            rules={(val) => {
              const value = val?.match(/[a-zA-Z0-9]{0,50}/g)?.[0] || "";
              return value.toUpperCase();
            }}
          />
        ),
      },
    },
    {
      label: "车辆海关编码",
      field: "customsNo",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入车辆海关编码",
          clearable: true,
          rules: (val) => {
            const value = val?.match(/[a-zA-Z0-9]{0,25}/g)?.[0] || "";
            return value.toUpperCase();
          },
        },
      },
    },
    {
      label: "备案重量(空车)",
      field: "weight",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => (
          <>
            <tz-input
              type="number"
              maxlength={5}
              disabled={state.isRead}
              v-model={state.form.weight}
              placeholder="请输入备案重量(空车)"
            ></tz-input>
            KG
          </>
        ),
      },
    },
    {
      label: "牌头公司",
      field: "brandCompany",
      attrs: {
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入牌头公司",
          maxlength: 200,
          clearable: true,
        },
      },
    },
    {
      label: "车厢容量",
      field: "boatload",
      attrs: {
        border: true,
      },
      component: {
        render: () => (
          <>
            <tz-input
              type="digit"
              v-model={state.form.boatload}
              placeholder="请输入车厢容量"
              disabled={state.isRead}
              rules={(_val) => {
                let val = "";
                if (_val) {
                  _val = _val.toString();
                  val = _val.match(/^\d{1,3}/g)?.[0] || 0;
                  _val.match(/\.\d{0,2}/)?.[0] &&
                    (val += _val.match(/\.\d{0,2}/)?.[0]);
                }
                return val;
              }}
            ></tz-input>
            CBM
          </>
        ),
      },
    },
    {
      label: "载货参数(米)",
      field: "",
      attrs: {
        border: true,
      },
      component: {
        render: () => (
          <div class="truck-cell-value">
            <tz-input
              v-model={state.form.truckLength}
              disabled={state.isRead}
              type="digit"
              rules={(_val) => {
                let value = "";
                if (_val) {
                  _val = _val.toString();
                  value = _val.match(/^\d{1,2}/g)?.[0] || 0;
                  _val.match(/\.\d{0,2}/)?.[0] &&
                    (value += _val.match(/\.\d{0,2}/)?.[0]);
                }
                return value;
              }}
            ></tz-input>
            <span class="after">长</span>

            <tz-input
              v-model={state.form.truckWide}
              disabled={state.isRead}
              type="digit"
              rules={(_val) => {
                let value = "";
                if (_val) {
                  _val = _val.toString();
                  value = _val.match(/^\d{1,2}/g)?.[0] || 0;
                  _val.match(/\.\d{0,2}/)?.[0] &&
                    (value += _val.match(/\.\d{0,2}/)?.[0]);
                }
                return value;
              }}
            ></tz-input>
            <span class="after">宽</span>

            <tz-input
              v-model={state.form.truckHigh}
              type="digit"
              disabled={state.isRead}
              rules={(_val) => {
                let value = "";
                if (_val) {
                  _val = _val.toString();
                  value = _val.match(/^\d{1,2}/g)?.[0] || 0;
                  _val.match(/\.\d{0,2}/)?.[0] &&
                    (value += _val.match(/\.\d{0,2}/)?.[0]);
                }
                return value;
              }}
            ></tz-input>
            <span class="after">高</span>
          </div>
        ),
      },
    },
  ];

  useFormReadonly(carInfoCells, state.isRead);

  return {
    validate: () => {
      const { validate, errMsg } = formRef.value.validate();
      if (!validate) {
        $TzNotify.danger(errMsg);
        return false;
      }
      return true;
    },
    render: () => (
      <div class="section">
        <div class="title">车辆信息</div>
        <tz-cell-form
          ref={formRef}
          cells={carInfoCells}
          v-model={state.form}
        ></tz-cell-form>
      </div>
    ),
  };
};
