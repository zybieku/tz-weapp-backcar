import { api } from "@tz-mall/shared";
import type { IApplyDetailResp } from "./type";

/**
 * 保存司机申请资格
 * @param data
 */
export const apiDriverSave = (data) => {
  const option = {
    url: `/uc/driver/driverApplication/save`,
    data,
  };
  return api.post(option);
};

/**
 * 修改司机申请资格
 */
export const apiDriverUpdate = (data) => {
  const option = {
    url: `/uc/driver/driverApplication/update`,
    data,
  };
  return api.post(option);
};

/**
 * 获取当前用户的司机申请资格状态
 */
export const apiGetApplicationStatus = () => {
  const option = {
    url: `/uc/driver/driverApplication/getApplicationStatus`,
  };
  return api.post(option);
};

/**
 * 获取当前用户的司机申请资格详情
 */
export const apiGetDetail = () => {
  const option = {
    url: `/uc/driver/driverApplication/getDriverApplicationInfo`,
  };
  return api.post<IApplyDetailResp>(option);
};
