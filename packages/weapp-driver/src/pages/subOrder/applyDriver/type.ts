/**
 * 司机审核详情
 */
export interface IApplyDriverDetail {
  id?: string;
  truckTypeCode?: string;
  truckTypeName: string;
  plateNumber: string;
  hkPlateNumber: string;
  customsNo: string;
  weight: number;
  brandCompany: string;
  boatload: number;
  truckLength: number;
  truckWide: number;
  truckHigh: number;
  driverName: string;
  driverCardNumber: string;
  phone: string;
  hkPhone: string;
  electronicLock: boolean;
  /**
   * 证书类型
   */
  certificateType: string;
  /**
   * 证书类型名称
   */
  certificateName: string;
  certificateNo: string;
  certificateValidity: string;
  travelDate: string;
  drivingNo: string;
  drivingLicence: string;
  roadTransportNo: string;
  roadTransport: string;
  auditorStatus: number;
  fileList?: string[];
  driverCode: string;
  auditorInfo?: string;
  auditorStatusName: string;
}

export interface IApplyDetailResp extends IBaseResp {
  data: IApplyDriverDetail;
}

export interface IApplyDriverState {
  /**
   * 审核状态
   */
  auditorStatus: number;
  /**
   * 是否只读
   */
  isRead: boolean;
  showLoading: boolean;
  form: IApplyDriverDetail;
  truckTypeArrs: unknown[];
  /**
   * 证件类型
   */
  certTypeArrs: unknown[];
  fileListMap: Record<string, Partial<Record<string, string>[]>>;
}

export interface IUploadResp extends IBaseResp {
  data: IFile;
}
