interface ICell {
  title: string;
  render?: Function;
  prop: string;
}

import { dateFormat } from "@tz-mall/shared";
import { ILocusDetalState } from "../type";
export const useLine = (state: ILocusDetalState) => {
  const cells: ICell[] = [
    {
      title: "车型",
      prop: "truckType",
      render: () => {
        return (
          <>
            {state.formData.truckType}【
            {state.formData.truckLength +
              "m*" +
              state.formData.truckWidth +
              "m*" +
              state.formData.truckHeight +
              "m/"}
            {state.formData.truckBoatload}KG/
            {state.formData.truckVolume}CBM】
          </>
        );
      },
    },
    {
      title: "价格有效时间",
      prop: "truckType",
      render: () => {
        return (
          <>
            {dateFormat(state.formData.lineValidTimeBegin || "", "MM-DD HH:mm")}
            ~{dateFormat(state.formData.lineValidTimeEnd || "", "MM-DD HH:mm")}
          </>
        );
      },
    },
  ];

  return () => (
    <div class="line">
      <div class="title">订单编号：{state.formData.sysOrderNo}</div>
      <div class="address">
        <div class="icon">
          <i class="circle"></i>
          <i class="wire"></i>
          <div class="arrow">
            <tz-icon name="d-arrow-right"></tz-icon>
          </div>
          <i class="wire"></i>
          <i class="circle"></i>
        </div>
        <div class="area">
          <div>{state.formData.consignerRegionName?.replace(/,/g, "/")}</div>
          <div>{state.formData.consigneeRegionName?.replace(/,/g, "/")}</div>
        </div>
      </div>
      {cells.map((cell) => (
        <tz-cell title={cell.title}>
          <div class="conent">
            {cell?.render?.() || state.formData[cell.prop]}
          </div>
        </tz-cell>
      ))}
    </div>
  );
};
