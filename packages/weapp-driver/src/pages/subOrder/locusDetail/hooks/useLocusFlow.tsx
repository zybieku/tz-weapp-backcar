import { EnumTrackCode } from "../../constant";
import type { ILocusDetalState, ILocus } from "../type";

export const useLocusFlow = (state: ILocusDetalState, navigateTo) => {
  const hasAttach = (trackCode: number) => {
    return [EnumTrackCode.SCZHD, EnumTrackCode.SCQSD].includes(trackCode);
  };

  const handleHasAttach = (item: ILocus) => {
    if (hasAttach(item.trackCode)) {
      navigateTo({
        path: "/pages/subOrder/locusAttaImg/index",
        query: {
          id: item.bizId,
          trackType:
            item.trackCode === EnumTrackCode.SCQSD
              ? "sign_for_documents"
              : "shipping_order",
        },
      });
    }
  };
  return () => (
    <div class="track-line">
      {state.locusArr.map((item, index) => (
        <div class="item" key={index}>
          <div class="dot"></div>
          <div class="label">
            <div
              onClick={() => {
                handleHasAttach(item);
              }}
              class={hasAttach(item.trackCode) ? "hasAttach" : ""}
            >
              {item.trackName}
              {hasAttach(item.trackCode) && (
                <tz-button type="primary" class="track-item-btn" size="mini">
                  查看附件
                </tz-button>
              )}
            </div>
          </div>
          <div class="time">{item.createTime}</div>
        </div>
      ))}
    </div>
  );
};
