export interface ILocus {
  bizId: string; //: "1078018167526887424",
  bizNameName: string; //: "待接单",
  bizStatusCode: number; //: 109,
  createTime: string; //: "2023-82-22 18:19:23",
  createrId: string; //:"6fa824dc-a262-4d96-ad67-2039fdcd6529",
  createrName: string; //:“刘加宝",
  id: number; //: 47,
  operateCode: number; //: 19,
  operateName: string; //:“下单预约",
  trackCode: number; //: 19,
  trackName: string; //:"下单,
  trackSort: number; //: 10,
}

interface FeeAmount {
  id: string;
  costName: string;
  feeAmount: number;
}

interface FeeDetail {
  feeAmountList: FeeAmount[];
  preferentialFeeAmountList: any[];
  totalFeeAmount: number;
  paidFeeAmount: number;
  unpaidAmount: number;
}

export interface IOrderDetail {
  id: string;
  sysOrderNo: string;
  lineOfferId: number;
  lineId: string;
  lineName: string;
  truckType: string;
  truckVolume: number;
  truckHeight: number;
  truckWidth: number;
  truckLength: number;

  /**
   * 载重
   */
  truckBoatload: number;
  consignerRegion: string;
  consignerRegionName: string;
  consignerAddress: string;
  consignerName: string;
  consignerMobile: string;
  unloadTimeBegin: string;
  unloadTimeEnd: string;
  consigneeRegion: string;
  consigneeRegionName: string;
  consigneeAddress: string;
  consigneeName: string;
  consigneeMobile: string;
  loadingTimeBegin: string;
  loadingTimeEnd: string;
  goodsName?: any;
  goodsNumber?: any;
  goodsWeight?: any;
  goodsVolume?: any;
  goodsValue?: any;
  packingSpec?: any;
  driverAccountNo: string;
  driver: string;
  licenseNumber: string;
  orderValue: number;
  lineValidTimeBegin: string;
  lineValidTimeEnd: string;
  roughWeight?: any;
  tdecConsignerName?: any;
  tdecConsignerAddress?: any;
  tdecConsigneeName?: any;
  tdecConsigneeAddress?: any;
  tradeType?: any;
  tradeIdentity?: any;
  tradeMerchant?: any;
  tradeAddress?: any;
  remark?: any;
  enterpriseVerifyStatus: boolean;
  payStatus: number;
  status: number;
  collectFlag: boolean;
  collectRemark?: any;
  tdecRise?: any;
  drawback: boolean;
  tray: string;
  clearedCustoms: string;
  userCode: string;
  placeTime: string;
  createTime: string;
  createrFirmId: string;
  createrFirmName: string;
  orderGoodsArr: any[];
  feeId?: any;
  feeAmount?: any;
  entrustDeclarer: boolean;
  feeDetail: FeeDetail;
}

export interface ILocusArrResp extends IBaseResp {
  data: ILocus[];
}

export interface IOrderDetailResp extends IBaseResp {
  data: IOrderDetail;
}

export interface ILocusDetalState {
  id: string;
  formData: IOrderDetail;
  locusArr: ILocus[];
}
