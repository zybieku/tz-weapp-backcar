import { api } from "@tz-mall/shared";
import type { ILocusArrResp, IOrderDetailResp } from "./type";

//获取详情
export const getDetail = (id: string) => {
  const option = {
    url: `/transport/order/transportOrder/getById/${id}`,
  };
  return api.post<IOrderDetailResp>(option);
};

//获取详情
export const getLocusDetail = (data) => {
  const option = {
    url: `/transport/order/tracked/listOrderTracked`,
    data: { id: data },
  };
  return api.post<ILocusArrResp>(option);
};
