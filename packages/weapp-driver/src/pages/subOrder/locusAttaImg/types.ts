export interface Attachement {
  bizId: string;
  fileName: string;
  id: string;
  remoteFileName: string;
}

export interface AttachResp extends IBaseResp {
  data: Attachement[];
}

export interface LocusAttaState {
  attachementArr: Attachement[];
}
