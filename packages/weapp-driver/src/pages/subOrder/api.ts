import { api } from "@tz-mall/shared";

// 更新节点状态
export const updateNodeStatus = (data) => {
  const option = {
    url: "/transport/order/tracked/addOrderTracked",
    data,
  };
  return api.post(option);
};
