import Taro from "@tarojs/taro";
import { reactive } from "vue";
import { uploadPic } from "../uploadPacking/api";

export const useUpload = (state) => {
  const upState = reactive({
    pics: [],
  });

  const handleChooseImage = async () => {
    const { tempFilePaths } = await Taro.chooseImage({
      sizeType: ["compressed"],
      sourceType: ["camera"],
    }).catch((err) => {
      console.log(err);
      return {};
    });
    if (tempFilePaths) {
      upState.pics.push(tempFilePaths[0]);
    }
  };

  //批量上传图片
  const uploadImgs = async () => {
    if (!upState.pics.length) return [];
    return await Promise.all(
      upState.pics.map((filePath) =>
        uploadPic({
          filePath,
          formData: { bizId: state.formData.id, bizType: "sign_for_documents" },
        })
      )
    )
      .then((arr) => arr.map((res) => res.data))
      .catch(() => {
        $TzNotify.danger("上传图片错误,请重试");
        return [];
      });
  };

  return {
    uploadImgs,
    render: () => (
      <tz-view class="item">
        <tz-view class="label required">签收单(最多上传3个)</tz-view>
        <tz-view class="tags">
          {upState.pics.map((src) => (
            <tz-image
              class="upload"
              src={src}
              mode="scaleToFill"
              onClick={() =>
                Taro.previewImage({
                  current: src,
                  urls: upState.pics,
                })
              }
            ></tz-image>
          ))}
          {upState.pics.length < 3 && (
            <tz-view class="upload" onClick={handleChooseImage}>
              <tz-view id="cross"></tz-view>
            </tz-view>
          )}
        </tz-view>
      </tz-view>
    ),
  };
};
