import { api } from "@tz-mall/shared";
import { ICostType, ICurrencytype, IFeeDetail } from "./types";

export const apiAddFee = (data: IFeeDetail) => {
  const option = {
    url: "/transport/order/fee/addWxOrderFee",
    data,
  };
  return api.post(option);
};

export const apiUpdateFee = (data: IFeeDetail) => {
  const option = {
    url: "/transport/order/fee/updateWxOrderFee",
    data,
  };
  return api.post(option);
};

/**
 * 获取费用名称基础数据
 *
 * receiptFlag  boolean  应收标识
 * paymentFlag  boolean  应付标识
 * enableFlag   boolean  启停标识  true-启用， false-停用
 */
interface IGetCostType {
  receiptFlag?: boolean;
  paymentFlag?: boolean;
  enableFlag?: boolean;
  driverEnterFlag?: boolean;
}
export const apiGetCostCode = (data: IGetCostType) => {
  const option = {
    url: "/basicdata/base/costType/listCostCodeInfo",
    data,
  };
  return api.post<{ data: ICostType[] }>(option);
};

/**
 * 获取费币种基础数据
 */
export const apiGetCurrencyByOrderId = (data: { id: string }) => {
  const option = {
    url: "/transport/order/currency/getCurrencyByOrderId",
    data,
  };
  return api.post<{ data: ICurrencytype[] }>(option);
};

/**
 * 根据费用id查询司机端（小程序）的费目信息
 */
export const apiGetWxOrderFeeById = (data) => {
  const option = {
    url: "/transport/order/fee/getWxOrderFeeById",
    data,
  };
  return api.post(option);
};

/**
 *  根据业务类型/车型等获取增值服务售价
 */

export const apiGetOrderFeeQuoteVas = (data) => {
  const option = {
    url: "/transport/quote/vasSellPrice/listOrderFeeQuoteVas",
    data,
  };
  return api.post(option);
};
