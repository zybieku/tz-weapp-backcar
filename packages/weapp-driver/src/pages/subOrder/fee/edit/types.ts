export interface IBasicOrderInfo {
  id: string;
  sysOrderNo: string;
  status: number;
  consigneeRegionName: string;
  consigneeAddress: string;
  consignerRegionName: string;
  consignerAddress: string;
  bizType: string;
  truckType: string;
}
export interface IFeeDetail {
  id?: string;
  orderId: string;
  costCode: string;
  costName: string;
  originalCurrency: string;
  originalAmount: string;
  auditExplain: string;
  status: number;
  statusName: string;
  attachmentIdList?: string[];
  feeUnitCode?: string;
  feeUnitName?: string;
  unitPrice?: string;
  number?: number;
}

export interface IFeeResp extends IBaseResp {
  data: IFeeDetail;
}

export interface IFeeDetailState {
  id: string;
  isRead: boolean;
  feeDeatil: {
    basicOrder: IBasicOrderInfo;
    fee: IFeeDetail;
  };
  showLoading: boolean;
  isUnit: boolean; //是否是计费数量
  sellList: IOrderFeeQuote;
  orderFeeQuoteList: IOrderFeeQuote[];
}

export interface ICostType {
  costCode: string;
  costName: string;
}

export interface ICurrencytype {
  currencyName: string;
  currency: string;
  label?: string;
}

export interface IOrderFeeQuote {
  bizType: string;
  feeCode: string;
  feeName: string;
  currency: string;
  feeUnitCode: string;
  feeUnitName: string;
  pricingModeCode: string;
  pricingModeName: string;
  truckType: string;
  unitPrice: string;
  lowestPrice: string;
  highestPrice: string;
}
