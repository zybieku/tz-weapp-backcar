import { apiListMultipleType, apiUploadReplacePic } from "@/api/basicApi";
import Taro from "@tarojs/taro";
import { reactive } from "vue";
import { EnumFeeBizType } from "../../../constant";
import type { IFeeDetailState } from "../types";

interface IPic {
  filePath?: string;
  url?: string;
  bizType?: string;
  fileId: string | undefined;
}

/**
 * 上传图片
 */
export const useUpload = (state: IFeeDetailState) => {
  const upState = reactive({
    pics: [] as IPic[],
  });

  /**
   * 获取已经上传的附件
   */
  const getBizAttachList = () => {
    //获取图片附件
    apiListMultipleType({
      bizTypes: Object.values(EnumFeeBizType),
      bizId: state.id!,
    }).then((res) => {
      upState.pics = res[EnumFeeBizType.TRANSPORT_ORDER_FEE].map((item) => {
        return {
          url: item.url,
          fileId: item.id,
        };
      });
    });
  };

  const handleChooseImage = async (index: number) => {
    if (state.isRead) {
      handlePreviewImage(upState.pics[index].filePath);
      return;
    }
    const { tempFilePaths } = await Taro.chooseImage({
      count: 1,
      sizeType: ["compressed"],
      sourceType: ["camera"],
    }).catch((err) => {
      console.log(err);
      return { tempFilePaths: "" };
    });
    if (tempFilePaths) {
      //替换图片
      if (index >= 0) {
        upState.pics[index].filePath = tempFilePaths[0];
        return;
      }
      upState.pics.push({ filePath: tempFilePaths[0], fileId: "" });
    }
  };

  const handlePreviewImage = (src) => {
    Taro.previewImage({
      current: src,
      urls: upState.pics.map((item) => item.filePath || item.url || ""),
    });
  };

  //批量上传图片
  const uploadImgs = async () => {
    const pics = upState.pics?.filter((item) => !!item.filePath) || [];
    if (!pics.length) return [];
    return await Promise.all(
      pics.map((pic) =>
        apiUploadReplacePic({
          filePath: pic.filePath,
          formData: {
            bizId: state.id || "",
            bizType: EnumFeeBizType.TRANSPORT_ORDER_FEE,
            fileId: pic.fileId,
          },
        })
      )
    ).then((arr) => arr.map((res) => res.data.id));
  };

  return {
    uploadImgs,
    getBizAttachList,
    render: () => {
      return (
        <>
          <tz-cell title="附件" required={!state.isUnit}></tz-cell>
          <div class="wrapper-img-list">
            {upState.pics.map((pic, index) => (
              <tz-image
                class="upload"
                src={pic.filePath || pic.url}
                key={index}
                mode="scaleToFill"
                onClick={(e) => {
                  e.stopPropagation();
                  handleChooseImage(index);
                }}
              ></tz-image>
            ))}
            {!state.isRead && upState.pics.length < 3 && (
              <div
                class="upload"
                onClick={(e: MouseEvent) => {
                  e.stopPropagation();
                  handleChooseImage(-1);
                }}
              >
                <div id="cross"></div>
              </div>
            )}
          </div>
        </>
      );
    },
  };
};
