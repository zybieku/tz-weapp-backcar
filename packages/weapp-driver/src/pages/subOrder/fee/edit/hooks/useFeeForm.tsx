import type { ICell } from "@tz-mall/weapp-ui";
import { reactive, ref } from "vue";
import { apiGetCostCode, apiGetCurrencyByOrderId } from "../api";
import type { ICostType, ICurrencytype, IFeeDetailState } from "../types";

export const useFeeForm = (state: IFeeDetailState) => {
  const formRef = ref();
  const fState = reactive({
    currencyList: [] as ICurrencytype[],
    costList: [] as ICostType[],
  });

  const getBasicData = () => {
    apiGetCostCode({
      receiptFlag: true,
      enableFlag: true,
      driverEnterFlag: true,
    }).then((res) => {
      //费用名称
      fState.costList = [{ costCode: "", costName: "--请选择--" }, ...res.data];
    });
    apiGetCurrencyByOrderId({ id: state.feeDeatil.basicOrder.id }).then(
      (res) => {
        //币种
        fState.currencyList = [
          { currency: "", currencyName: "--请选择--", label: "--请选择--" },
          ...res.data.map((item) => {
            const { currency, currencyName } = item;
            return {
              currency,
              currencyName,
              label: currency + "-" + currencyName,
            };
          }),
        ];
      }
    );
  };

  getBasicData();

  const getIsUnit = (costName: string) => {
    const list = state.orderFeeQuoteList.filter(
      (item) => item.feeName === costName
    );
    if (list.length > 0) {
      state.isUnit = true;
      state.sellList = list[0];
    } else {
      state.isUnit = false;
    }
  };

  const cells: ICell[] = [
    {
      label: "费用名称",
      field: "costCode",
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => {
          if (state.isRead) {
            return <div>{state.feeDeatil.fee.costName}</div>;
          }
          return fState.costList?.length ? (
            <tz-picker
              v-model={state.feeDeatil.fee.costCode}
              mode="selector"
              rangeKey="costName"
              valueKey="costCode"
              disabled={state.isRead}
              range={fState.costList}
              onChange={(_, item) => {
                state.feeDeatil.fee.costName = item.costName;
                getIsUnit(item.costName);
              }}
            ></tz-picker>
          ) : null;
        },
      },
    },
    {
      label: "计费数量",
      field: "number",
      show: () => state.isUnit,
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () => {
          return state.isRead ? (
            <span>{state.feeDeatil.fee.number}</span>
          ) : (
            <>
              <tz-input-number
                v-model={state.feeDeatil.fee.number}
              ></tz-input-number>
            </>
          );
        },
      },
    },
    {
      label: "计费单位",
      field: "feeUnitName",
      show: () => state.isUnit,
      attrs: {
        require: true,
        border: true,
      },
      component: {
        render: () => {
          return <span>{state.orderFeeQuoteList[0]?.feeUnitName}</span>;
        },
      },
    },
    {
      label: "费用金额",
      field: "originalAmount",
      show: () => !state.isUnit,
      attrs: {
        required: true,
        border: true,
      },
      component: {
        name: "tz-input",
        attrs: {
          placeholder: "请输入费用金额",
          clearable: true,
          type: "digit",
          disabled: state.isRead,
          rules: (_val) => {
            let value = "";
            if (_val) {
              _val = _val.toString();
              value = Number(_val.match(/^\d{1,6}/g)?.[0] || 0) + "";
              _val.match(/\.\d{0,2}/)?.[0] &&
                (value += _val.match(/\.\d{0,2}/)?.[0]);
            }
            return value;
          },
        },
      },
    },
    {
      label: "费用币种",
      field: "originalCurrency",
      show: () => !state.isUnit,
      attrs: {
        required: true,
        border: true,
      },
      component: {
        render: () =>
          fState.currencyList?.length ? (
            <tz-picker
              v-model={state.feeDeatil.fee.originalCurrency}
              mode="selector"
              rangeKey="label"
              valueKey="currency"
              disabled={state.isRead}
              range={fState.currencyList}
            ></tz-picker>
          ) : null,
      },
    },
  ];
  return {
    validate: () => {
      debugger;
      if (state.isUnit) {
        const regex = /^[1-9]\d{0,2}$/;
        if (!state.feeDeatil.fee.number) {
          state.feeDeatil.fee.number = 1;
        }

        if (!regex.test(Number(state.feeDeatil.fee.number) + "")) {
          $TzNotify.danger("计费数量必须为小于三位的正整数!");
          return false;
        }
      }

      const { validate, errMsg } = formRef.value.validate();
      if (!validate) {
        $TzNotify.danger(errMsg);
        return false;
      }
      return true;
    },
    render: () => (
      <div class="section">
        <tz-cell-form
          ref={formRef}
          cells={cells}
          v-model={state.feeDeatil.fee}
        ></tz-cell-form>
      </div>
    ),
  };
};
