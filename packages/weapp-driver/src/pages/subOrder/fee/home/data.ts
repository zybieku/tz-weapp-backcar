import { EnumFeeStatus } from "../constant";

/**
 * 运输状态
 * `
 * 200: "已接单",
 * 400: "已完成",
 * `
 */
export const statusJson = {
  200: "已接单",
  400: "已完成",
};

/**
 * fee项目状态
 */
export const feeStatusJson = {
  [EnumFeeStatus.WAIT_SUBMIT]: {
    name: "待提交",
    className: "fee-status-primary",
  },
  [EnumFeeStatus.WAIT_AUDIT]: {
    name: "待审核",
    className: "fee-status-primary",
  },
  [EnumFeeStatus.REFUSE]: { name: "已拒绝", className: "fee-status-danger" },
  [EnumFeeStatus.REVIEW]: { name: "已审核", className: "fee-status-success" },
  [EnumFeeStatus.CANCEL]: { name: "已取消", className: "fee-status-info" },
};
