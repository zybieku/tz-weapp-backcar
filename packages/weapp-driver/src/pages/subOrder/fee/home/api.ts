import { api } from "@tz-mall/shared";
import type { IFeeResp } from "./types";

export const apiGetFeeDetail = (data) => {
  const option = {
    url: "/transport/order/fee/listWxOrderFeeByOrderId",
    data,
  };
  return api.post<IFeeResp>(option);
};
