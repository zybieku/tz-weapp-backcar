export interface IFeeDetail {
  id: string;
  orderId: string;
  /**
   * 费用名称code
   */
  costCode: string;
  costName: string;
  /**
   * 费用金额
   */
  originalCurrency: string;
  /**
   * 费用金额单位
   */
  originalAmount: string;
  auditExplain: string;

  /**
   * 状态
   */
  feeStatus: number;
  feeStatusName: string;
}

export interface IFeeResp extends IBaseResp {
  data: IFeeDetail[];
}

export interface IBasicOrderInfo {
  id: string;
  sysOrderNo: string;
  status: number;
  consigneeRegionName: string;
  consigneeAddress: string;
  consignerRegionName: string;
  consignerAddress: string;
}

export interface IFeeDetailState {
  id: string;
  feeDeatil: {
    basicOrder: IBasicOrderInfo;
    list: IFeeDetail[];
    total: string;
  };
  showLoading: boolean;
}
