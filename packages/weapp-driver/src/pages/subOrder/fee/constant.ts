export const EnumFeeStatus = {
  /**
   * 10: "待提交",
   */
  WAIT_SUBMIT: 10,
  /**
   *  20: "待审核"
   */
  WAIT_AUDIT: 20,
  /**
   *  30: "已拒绝",
   */
  REFUSE: 30,
  /**
   * 40: "已审核",
   */
  REVIEW: 40,
  /**
   *  50: "已取消",
   */
  CANCEL: 50,
};
