import { EnumTrackCode } from "@/pages/subOrder/constant";

/**
 * 运输状态
 * `
 * 200: "已接单",
 * 400: "已完成",
 * `
 */
export const statusJson = {
  200: "已接单",
  400: "已完成",
};

export const uploadUrlJson = {
  /**
   * 上传装货单
   */

  [EnumTrackCode.SCZHD]:
    "/pages/subOrder/uploadPacking/index",
  /**
   * 上传签收单
   */
  [EnumTrackCode.SCQSD]:
    "/pages/subOrder/uploadReceipt/index",
} as const;
