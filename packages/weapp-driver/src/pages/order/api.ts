import { api } from "@tz-mall/shared";
import type { IAuditor, IOrderLsitResp } from "./types";

export const getOrderListApi = (data) => {
  const option = {
    url: "/transport/order/transportOrder/list",
    data,
  };
  return api.post<IOrderLsitResp>(option);
};

// 更新节点状态
export const updateNodeStatus = (data) => {
  const option = {
    url: "/transport/order/tracked/addOrderTracked",
    data,
  };
  return api.post(option);
};

/**
 * 获取当前用户的司机申请资格状态
 */
export const apiGetApplicationStatus = () => {
  const option = {
    url: `/uc/driver/driverApplication/getApplicationStatus`,
  };
  return api.post<{ data: IAuditor }>(option);
};
