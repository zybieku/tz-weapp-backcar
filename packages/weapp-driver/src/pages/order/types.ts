export interface IOrder {
  id: string;
  sysOrderNo: string;
  lineName: string;
  consigneeRegionName: string;
  consigneeAddress: string;
  consignerRegionName: string;
  consignerAddress: string;
  unloadTimeBegin: string;
  unloadTimeEnd: string;
  consignerName: string;
  consignerMobile: string;
  consigneeName: string;
  consigneeMobile: string;
  loadingTimeBegin: string;
  loadingTimeEnd: string;
  createrName: string;
  goodsName?: string;
  orderValue: number;
  createrFirmId: string;
  createrFirmName: string;
  userCode: string;
  enterpriseVerifyStatus: boolean;
  createTime: string;
  placeTime: string;
  status: number;
  payStatus: number;
  truckType: string;
  driverAccountNo: string;
  driver: string;
  licenseNumber: string;
  operateCode: number;
  operateName: string;
  bizType: string;
}

export interface IOrderLsitResp extends IBaseResp {
  data: {
    records: IOrder[];
    total: number;
    size: number;
    current: number;
    searchCount: boolean;
    pages: number;
  };
}

export interface IAuditor {
  status: number;
  auditorInfo: string;
}
export interface IOrderLsitState {
  orderList: IOrder[];
  queryParams: {
    size: number;
    current: number;
    receiving: boolean;
    /**
     * 200, 300, 400
     * 已接单,运输中，已完成
     */
    statusArr: number[];
    sysOrderNo: string;
  };
  total: number;
  showLoading: boolean;
  auditor: IAuditor;
}
