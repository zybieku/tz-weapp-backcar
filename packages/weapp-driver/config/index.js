let conf = require("@tz-mall/conf");

let config = {
  defineConstants: {
    //api版本号,用于后台区分灰度和正式环境
    ENV_API_VERSION: '"v1.1.0"',
    ENV_HOME_URL: '"/pages/order/index"',
    LOCATION_APIKEY: JSON.stringify("VZ7BZ-6I5KH-ZL4D5-WFK5Z-XE73F-CEBBI"),
  },
};

module.exports = function (merge) {
  return conf.merge(config, merge);
};
