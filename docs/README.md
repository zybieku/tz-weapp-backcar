# 介绍

这是粤港车taro小程序

# 快速开始
```shell
# 安装pnpm (已安装可以忽略)
npm install -g pnpm
```

```shell
# 安装依赖库
pnpm install
```

```shell 
#微信小程序
pnpm dev:weapp 启动开发
pnpm build:weapp 打包测试  
pnpm prod:weapp 打包生产  

#h5
pnpm dev:h5 启动h5服务
pnpm build:h5 打包测试  
pnpm prod:h5 打包生产 
```

# 目录结构

```js
├─home 首页
│      api.js
│      index.config.js
│      index.vue
│      README.md
│      useActivDialog.js
│      useHongkong.js
│      useMainland.js
│      
├─login 登录页
│      api.js
│      index.config.js
│      index.vue
│      login.config.js
│      login.vue
│      README.md
├─order 订单页
│      api.js
│      index.config.js
│      index.vue
│      README.md 
│      
├─mine 我的
│      api.js
│      index.config.js
│      index.vue
│      README.md  
│              
└─subUser 用户管理相关
    │      
    ├─findPwd 忘记密码
    │      api.js
    │      index.config.js
    │      index.vue
    │      README.md       
    │      
    ├─recharge 充值
    │      api.js
    │      index.config.js
    │      index.vue
    │      README.md
    │      useBalance.js
    │      
    ├─register 注册
    │      api.js
    │      index.config.js
    │      index.vue
    │      README.md
    │    
    │      
    └─webPage
            api.js
            index.config.js
            index.vue
```