## 回头车小程序,发布流程

### 发体验版步骤 
1.打包
```shell
pnpm build:cus 
# 或者driver
pnpm build:driver 
```
在微信开发者工具上预览打包后的小程序，确认无误后点击右上角的上传按钮，上传到体验版  

![alt 属性文本](1658381914616.jpg)

备注上传版本号与修改信息

![alt 属性文本](1658382148010.jpg)

2.登录小程序管理后台

> https://mp.weixin.qq.com/

进入版本管理

![alt 属性文本](1658382500388.png)

找到你上传的版本，选为体验版  

![alt 属性文本](1658382757918.jpg)

之后将体验版二维码发给测试即可




### 目录结构

```
├─home 首页
│      api.js
│      index.config.js
│      index.vue
│      README.md
│      useActivDialog.js
│      useHongkong.js
│      useMainland.js
│      
├─login 登录页
│      api.js
│      index.config.js
│      index.vue
│      login.config.js
│      login.vue
│      README.md
│      
├─mine 我的
│      api.js
│      index.config.js
│      index.vue
│      README.md
│      
```
