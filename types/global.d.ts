declare module "*.png";
declare module "*.gif";
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.svg";
declare module "*.css";
declare module "*.less";
declare module "*.scss";
declare module "*.sass";
declare module "*.styl";

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

// @ts-ignore
declare const process: {
  env: {
    TARO_ENV:
      | "weapp"
      | "swan"
      | "alipay"
      | "h5"
      | "rn"
      | "tt"
      | "quickapp"
      | "qq";
    [key: string]: any;
  };
};

declare module "@tarojs/components" {
  export * from "@tarojs/components/types/index.vue3";
}

declare const ENV_BASE_API: string;
declare const ENV_API_VERSION: string;
declare const ENV_HOME_URL: string;
/**
 * 腾讯地图key
 */
declare const LOCATION_APIKEY: string;
declare const $TzNotify: {
  danger: (msg: string) => void;
  success: (msg: string) => void;
};
