declare global {
  interface ITokenInfo {
    accessToken: string;
    expiresIn: string;
    refreshToken: string;
    now: number;
  }

  interface IUserInfo {
    id: string;
    username?: string;
    name: string;
    userCode: string;
    phone: string;
    password?: string;
    mainAccount: boolean;
    userType: number;
    firmId: string;
    firmName?: string;
    verifyStatus: number;
    userOrigin: number;
    status: number;
    deptId?: number;
    firmVerified: boolean;
    firmVerifyType?: boolean;
    customerInfo?: any;
    supplierInfo?: any;
    account: string;
    nickname?: string;
  }

  interface INavOption {
    delta?: number;
    event?: {
      type: string;
      data?: Record<string, any> | string;
    } & Record<string, any>;
    path: string;
    query?: Record<string, string>;
    /** 接口调用结束的回调函数（调用成功、失败都会执行） */
    complete?: (res: TaroGeneral.CallbackResult) => void;
    /** 接口调用失败的回调函数 */
    fail?: (res: TaroGeneral.CallbackResult) => void;
    /** 接口调用成功的回调函数 */
    success?: (res: TaroGeneral.CallbackResult) => void;
  }

  interface IBaseResp {
    code: number;
    msg: string;
    success: boolean;
    notSuccess: boolean;
  }

  interface IFile {
    id: string;
    fileName: string;
    remoteFileName: string;
    fileType: string;
    fileSize: number;
    bizType: string;
  }
}

export {};
